SELECT SUM(CASE
               WHEN r.os = 'delivered' THEN 1
               WHEN r.os = 'cancelled'
                    AND r.cri NOT IN (25, 26, 27, 42)
                    AND r.ifp = TRUE THEN 1
               ELSE 0
           END) order_count,
       SUM(CASE
               WHEN r.os = 'delivered' THEN r.sb
               WHEN r.os = 'cancelled'
                    AND r.cri NOT IN (25, 26, 27, 42)
                    AND r.ifp = TRUE THEN r.sb
               ELSE 0
           END) revenue,
       id,
       is_veg,
       min(name) AS name,
       min(cat) AS category,
       min(sub_cat) AS sub_category,
       price
FROM
  (SELECT DISTINCT (voi.order_id) oi,
                   vos.order_status os,
                   vos.cancel_reason_id cri,
                   vos.is_food_prepared ifp,
                   cart.item_id id,
                   cart.name name,
                   cart.sub_total sb,
                   cart.category cat,
                   cart.sub_category sub_cat,
                   items.is_veg,
                   items.price
   FROM oms_orderstatus vos,
        oms_order voi,
        oms_cartitem cart,
        items
   WHERE vos.id = voi.status_id
     AND cart.cart_id = voi.cart_id
     AND cart.item_id = items.id
     AND voi.restaurant_details_id IN
       (SELECT id
        FROM oms_restaurantdetails
        WHERE restaurant_id=${rest_id})
     AND vos.ordered_time >= '${start_date}'
     AND vos.ordered_time <= '${end_date}') r
GROUP BY id
ORDER BY revenue DESC;