SELECT count(DISTINCT(ord.order_id)) AS total_orders,
       count(DISTINCT(CASE
                          WHEN TIME_TO_SEC(TIMEDIFF(ord_status.placed_time, ord_status.with_partner_time)) < 360 THEN ord.order_id
                          ELSE NULL
                      END)) AS no_of_orders,
       count(DISTINCT(CASE
                          WHEN ord.cancel_disposition_id IN (25, 26, 27,42) THEN ord.order_id
                          ELSE NULL
                      END)) AS cancelled_orders,
       Count(DISTINCT(CASE
                          WHEN edited_flag = 1
                               AND (ord.payment_txn_status = 'success'
                                    OR ord.payment_txn_status = '')
                               AND cancelled_time IS NULL THEN ord.order_id
                          ELSE NULL
                      END)) AS edited_orders,
       avg(TIME_TO_SEC(TIMEDIFF(ord_status.placed_time, ord_status.with_partner_time))) AS avg_confirmation_time
FROM oms_order ord
INNER JOIN oms_orderstatus ord_status ON ord_status.id = ord.status_id
LEFT JOIN
  (SELECT DISTINCT order_id,
                   1 AS edited_flag
   FROM oms_itemeditlogs
   WHERE lower(remove_reason) NOT LIKE '%customer%') iel ON iel.order_id = ord.id
WHERE ord.restaurant_details_id IN
    (SELECT id
     FROM oms_restaurantdetails
     WHERE restaurant_id=${rest_id})
  AND ord.ordered_time >= '${start_date}'
  AND ord.ordered_time <= '${end_date}';