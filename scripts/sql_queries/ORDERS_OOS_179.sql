
SELECT count(DISTINCT(CASE
                          WHEN ord.cancel_disposition_id IN (25, 26, 27,42)
                               AND iel.edited_flag = 1 THEN ord.order_id
                          ELSE NULL
                      END)) AS alter_not_accepted_cancelled,
       count(DISTINCT(CASE
                          WHEN ord_status.order_status = 'delivered'
                               AND iel.edited_flag = 1 THEN ord.order_id
                          ELSE NULL
                      END)) AS alter_provided_delivered,
       count(DISTINCT(CASE
                          WHEN TIME_TO_SEC(TIMEDIFF(ord_status.placed_time, ord_status.with_partner_time)) <= 360 THEN ord.order_id
                      END)) AS confirmed_in_6min,
       count(DISTINCT(ord.order_id)) AS total_orders,
       sum(CASE
               WHEN ord_status.order_status = 'delivered' THEN 1
               WHEN ord_status.order_status = 'cancelled'
                    AND ord.cancel_disposition_id NOT IN (25, 26, 27, 42)
                    AND ord_status.is_food_prepared = 1 THEN 1
               ELSE 0
           END) AS delivered_orders,
       avg(CASE
               WHEN TIME_TO_SEC(TIMEDIFF(ord_status.placed_time, ord_status.with_partner_time)) > 0 THEN TIME_TO_SEC(TIMEDIFF(ord_status.placed_time, ord_status.with_partner_time))
               ELSE NULL
           END) AS avg_confirmation_time
FROM oms_order ord
INNER JOIN oms_orderstatus ord_status ON ord.status_id = ord_status.id
LEFT JOIN
  (SELECT order_id,
          1 AS edited_flag
   FROM oms_itemeditlogs
   WHERE lower(remove_reason) NOT LIKE '%customer%') iel ON iel.order_id = ord.id
WHERE ord.restaurant_details_id IN
    (SELECT id
     FROM oms_restaurantdetails
     WHERE restaurant_id=${rest_id})
  AND ord.ordered_time >= '${start_date}'
  AND ord.ordered_time <= '${end_date}';