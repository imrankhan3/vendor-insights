SELECT count(DISTINCT(ord.order_id)) AS order_count,
       ord.cancel_disposition_id AS reason_id,
       sum(ord.bill) AS losses,
       u.total_orders AS total_orders
FROM oms_orderstatus order_status
INNER JOIN oms_order ord ON order_status.id = ord.status_id
CROSS JOIN
  (SELECT count(DISTINCT(ord.order_id)) AS total_orders
   FROM oms_order ord
   INNER JOIN oms_orderstatus ord_status ON ord.status_id=ord_status.id
   WHERE ord.restaurant_details_id IN
       (SELECT id
        FROM oms_restaurantdetails
        WHERE restaurant_id=${rest_id})
     AND ord.ordered_time >= '${start_date}'
     AND ord.ordered_time <= '${end_date}') u
WHERE order_status.order_status = 'cancelled'
  AND ord.cancel_disposition_id IN (25,
                                    26,
                                    27,
                                    42)
  AND ord.restaurant_details_id IN
    (SELECT id
     FROM oms_restaurantdetails
     WHERE restaurant_id=${rest_id})
  AND ord.ordered_time >= '${start_date}'
  AND ord.ordered_time <= '${end_date}'
GROUP BY reason_id;