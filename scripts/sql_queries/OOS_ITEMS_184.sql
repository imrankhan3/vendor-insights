SELECT count(item_id) AS order_count,
       min(oms_cartitem.name) AS name,
       min(category) AS category,
       min(sub_category) AS sub_category,
       item_id AS id,
       items.is_veg,
       items.price
FROM oms_cartitem
INNER JOIN items ON oms_cartitem.item_id = items.id
WHERE cart_id IN
    (SELECT cart_id
     FROM oms_order
     WHERE restaurant_details_id IN
         (SELECT id
          FROM oms_restaurantdetails
          WHERE restaurant_id=${rest_id})
       AND ordered_time >= '${start_date}'
       AND ordered_time <= '${end_date}')
  AND is_oos=1
GROUP BY item_id
ORDER BY order_count DESC;