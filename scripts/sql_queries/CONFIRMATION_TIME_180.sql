
SELECT IFNULL(avg(c.confirmation_time), 0) AS avg_confirmation_time,
       IFNULL(min(c.confirmation_time), 0) AS min_confirmation_time,
       IFNULL(max(c.confirmation_time), 0) AS max_confirmation_time,
       IFNULL(sum(CASE
                      WHEN c.confirmation_time <= 180 THEN 1
                      ELSE 0
                  END), 0) AS zero_three_min,
       IFNULL(sum(CASE
                      WHEN c.confirmation_time > 180
                           AND c.confirmation_time <= 360 THEN 1
                      ELSE 0
                  END), 0) AS three_six_min,
       IFNULL(sum(CASE
                      WHEN c.confirmation_time > 360
                           AND c.confirmation_time <= 540 THEN 1
                      ELSE 0
                  END), 0) AS six_nine_min,
       IFNULL(sum(CASE
                      WHEN c.with_de_time IS NOT NULL THEN 1
                      ELSE 0
                  END), 0) AS confirmed_by_de,
       IFNULL(sum(CASE
                      WHEN c.placed_time IS NULL THEN 1
                      ELSE 0
                  END), 0) AS not_accepted,
       count(c.order_id) AS no_of_orders
FROM
  (SELECT DISTINCT (ord.order_id) order_id,
                   CASE
                       WHEN TIME_TO_SEC(TIMEDIFF(ord_status.placed_time, ord_status.with_partner_time)) > 0 THEN TIME_TO_SEC(TIMEDIFF(ord_status.placed_time, ord_status.with_partner_time))
                       ELSE NULL
                   END AS confirmation_time,
                   ord_status.with_de_time AS with_de_time,
                   ord_status.placed_time AS placed_time
   FROM oms_order ord
   INNER JOIN oms_orderstatus ord_status ON ord.status_id=ord_status.id
   WHERE ord.restaurant_details_id IN
       (SELECT id
        FROM oms_restaurantdetails
        WHERE restaurant_id=${rest_id})
     AND ord.ordered_time >= '${start_date}'
     AND ord.ordered_time <= '${end_date}') c;