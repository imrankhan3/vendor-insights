SELECT count(DISTINCT (a.order_id)) total_orders,
       SUM(CASE
               WHEN a.restaurant_rating IN (1,2,3,4,5) THEN 1
               ELSE 0
           END) rated_orders,
       SUM(CASE
               WHEN a.restaurant_rating IN (1,2,3,4,5) THEN restaurant_rating
               ELSE 0
           END) sum_rating,
       SUM(CASE
               WHEN a.restaurant_rating = 1 THEN 1
               ELSE 0
           END) one_star,
       SUM(CASE
               WHEN a.restaurant_rating = 2 THEN 1
               ELSE 0
           END) two_star,
       SUM(CASE
               WHEN a.restaurant_rating = 3 THEN 1
               ELSE 0
           END) three_star,
       SUM(CASE
               WHEN a.restaurant_rating = 4 THEN 1
               ELSE 0
           END) four_star,
       SUM(CASE
               WHEN a.restaurant_rating = 5 THEN 1
               ELSE 0
           END) five_star,
       SUM(CASE
               WHEN a.issues LIKE '%FOOD_RELATED%' THEN 1
               ELSE 0
           END) r_one_count,
       SUM(CASE
               WHEN a.issues LIKE '%PACKAGING%' THEN 1
               ELSE 0
           END) r_two_count,
       SUM(CASE
               WHEN a.issues LIKE '%ITEM_MISSING%' THEN 1
               ELSE 0
           END) r_three_count,
       SUM(CASE
               WHEN a.issues LIKE '%food_quality%' THEN 1
               ELSE 0
           END) r_four_count,
       SUM(CASE
               WHEN a.issues LIKE '%food_quantity%' THEN 1
               ELSE 0
           END) r_five_count,
       SUM(CASE
               WHEN a.issues LIKE '%order_packaging%' THEN 1
               ELSE 0
           END) r_six_count,
       SUM(CASE
               WHEN a.issues LIKE '%missing_item%' THEN 1
               ELSE 0
           END) r_seven_count
FROM
  (SELECT DISTINCT oi.order_id,
                   r.restaurant_rating,
                   r.issues,
                   oi.ordered_time
   FROM oms_order oi
   LEFT JOIN oms_orderstatus os ON os.id = oi.status_id
   LEFT JOIN ratings r ON oi.order_id = r.order_id
   WHERE oi.restaurant_details_id IN
       (SELECT id
        FROM oms_restaurantdetails
        WHERE restaurant_id=${rest_id})
     AND os.status = 'delivered'
     AND oi.ordered_time >= '${start_date}'
     AND oi.ordered_time <= '${end_date}') AS a;