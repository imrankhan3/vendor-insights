SELECT sum(CASE
               WHEN ord_status.order_status = 'delivered' THEN 1
               WHEN ord_status.order_status = 'cancelled'
                    AND ord.cancel_disposition_id IN (25, 26, 27,42) THEN 1
               WHEN ord_status.order_status = 'cancelled'
                    AND ord.cancel_disposition_id NOT IN (25, 26, 27, 42)
                    AND ord_status.is_food_prepared = 1 THEN 1
               ELSE 0
           END) AS no_of_orders,
       sum(CASE
               WHEN ord_status.order_status = 'delivered' THEN 1
               WHEN ord_status.order_status = 'cancelled'
                    AND ord.cancel_disposition_id NOT IN (25, 26, 27, 42)
                    AND ord_status.is_food_prepared = 1 THEN 1
               ELSE 0
           END) AS no_of_delivered_orders,
       sum(CASE
               WHEN ord_status.order_status = 'cancelled'
                    AND ord.cancel_disposition_id IN (25, 26, 27,42) THEN 1
               ELSE 0
           END) AS no_of_cancelled_orders,
       sum(CASE
               WHEN ord_status.order_status = 'delivered' THEN ord.total
               WHEN ord_status.order_status = 'cancelled'
                    AND ord.cancel_disposition_id NOT IN (25, 26, 27, 42)
                    AND ord_status.is_food_prepared = 1 THEN ord.bill
               ELSE 0
           END) AS revenue,
       sum(CASE
               WHEN ord_status.order_status = 'cancelled'
                    AND ord.cancel_disposition_id IN (25, 26, 27,42) THEN ord.bill
               ELSE 0
           END) AS losses,
       EXTRACT(HOUR
               FROM convert_tz(ord.ordered_time, 'UTC', 'Asia/Calcutta')) AS hour
FROM oms_order ord
INNER JOIN oms_orderstatus ord_status ON ord.status_id = ord_status.id
WHERE ord.restaurant_details_id IN
    (SELECT id
     FROM oms_restaurantdetails
     WHERE restaurant_id IN (${rest_id}))
  AND ord.ordered_time >= '${start_date}'
  AND ord.ordered_time <= '${end_date}'
GROUP BY hour;