SELECT cart_item.item_id id,
       cart_item.name AS name,
       count(DISTINCT(ord.order_id)) AS order_count,
       items.is_veg,
       items.price AS price,
       sum(cart_item.sub_total) AS revenue
FROM oms_order ord
INNER JOIN oms_cartitem cart_item ON ord.cart_id = cart_item.cart_id
INNER JOIN items ON cart_item.item_id = items.id
WHERE ord.restaurant_details_id IN
    (SELECT id
     FROM oms_restaurantdetails
     WHERE restaurant_id=${rest_id})
  AND cart_item.category = '${category}'
  AND cart_item.sub_category = '${sub_category}'
  AND ord.ordered_time >= '${start_date}'
  AND ord.ordered_time <= '${end_date}'
GROUP BY id,
         name
ORDER BY revenue DESC