SELECT CAST(convert_tz(max(ord.ordered_time), 'UTC', 'Asia/Calcutta') AS CHAR) AS last_updated_time
FROM oms_order ord
INNER JOIN oms_orderstatus ord_status ON ord_status.id = ord.status_id
WHERE restaurant_details_id IN
    (SELECT id
     FROM oms_restaurantdetails
     WHERE restaurant_id=${rest_id})
  AND ord_status.status = 'delivered';