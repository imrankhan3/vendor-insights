SELECT count(ord.order_id) AS total_orders,
       count(CASE
                 WHEN TIME_TO_SEC(TIMEDIFF(ord_status.placed_time, ord_status.with_partner_time)) < 360 THEN ord.order_id
                 ELSE NULL
             END) AS no_of_orders,
       count(CASE
                 WHEN ord.cancel_disposition_id IN (25, 26, 27,42) THEN ord.order_id
                 ELSE NULL
             END) AS cancelled_orders,
       Count(CASE
                 WHEN edited_flag = 1
                      AND (ord.payment_txn_status = 'success'
                           OR ord.payment_txn_status = '')
                      AND cancelled_time IS NULL THEN ord.order_id
                 ELSE NULL
             END) AS edited_orders,
       sum(CASE
               WHEN ord_status.status = 'delivered' THEN ord.bill
               WHEN ord_status.status = 'cancelled'
                    AND ord.cancel_disposition_id NOT IN (25, 26, 27, 42)
                    AND ord_status.is_food_prepared = 1 THEN ord.bill
               ELSE 0
           END) AS revenue
FROM oms_order ord
INNER JOIN oms_orderstatus ord_status ON ord_status.id = ord.status_id
LEFT JOIN
  (SELECT DISTINCT order_id,
                   1 AS edited_flag
   FROM oms_itemeditlogs
   WHERE lower(remove_reason) NOT LIKE '%customer%') iel ON iel.order_id = ord.id
WHERE ord.restaurant_details_id IN
    (SELECT id
     FROM oms_restaurantdetails
     WHERE restaurant_id=${rest_id})
  AND ord.ordered_time >= '${start_date}'
  AND ord.ordered_time <= '${end_date}';