SELECT avg(c.confirmation_time) AS avg_confirmation_time,
       min(c.confirmation_time) AS min_confirmation_time,
       max(c.confirmation_time) AS max_confirmation_time,
       sum(CASE
               WHEN c.confirmation_time <= 180 THEN 1
               ELSE 0
           END) AS zero_three_min,
       sum(CASE
               WHEN c.confirmation_time > 180
                    AND c.confirmation_time <= 360 THEN 1
               ELSE 0
           END) AS three_six_min,
       sum(CASE
               WHEN c.confirmation_time > 360
                    AND c.confirmation_time <= 540 THEN 1
               ELSE 0
           END) AS six_nine_min,
       sum(CASE
               WHEN c.with_de_time IS NOT NULL THEN 1
               ELSE 0
           END) AS confirmed_by_de,
       sum(CASE
               WHEN c.placed_time IS NULL THEN 1
               ELSE 0
           END) AS not_accepted,
       count(c.order_id) AS no_of_orders,
       EXTRACT(HOUR
               FROM convert_tz(c.ordered_time, 'UTC', 'Asia/Calcutta')) AS hour
FROM
  (SELECT DISTINCT (ord.order_id) order_id,
                   CASE
                       WHEN TIME_TO_SEC(TIMEDIFF(ord_status.placed_time, ord_status.with_partner_time)) > 0 THEN TIME_TO_SEC(TIMEDIFF(ord_status.placed_time, ord_status.with_partner_time))
                       ELSE NULL
                   END AS confirmation_time,
                   ord_status.with_de_time AS with_de_time,
                   ord_status.placed_time AS placed_time,
                   ord.ordered_time AS ordered_time
   FROM oms_order ord
   INNER JOIN oms_orderstatus ord_status ON ord.status_id=ord_status.id
   WHERE ord.restaurant_details_id IN
       (SELECT id
        FROM oms_restaurantdetails
        WHERE restaurant_id=${rest_id})
     AND ord.ordered_time >= '${start_date}'
     AND ord.ordered_time <= '${end_date}') c
GROUP BY hour;