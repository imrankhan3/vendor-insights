package com.swiggy.vendor_insights.logging;

import java.util.Arrays;

import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

@Log4j2
@Aspect
@Component
public class LoggingHandler {

    @Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
    public void restController(){
    }

    @Pointcut("execution(* *.*(..))")
    protected void allMethod() {
    }

    @Before("(restController() || within(com.swiggy.vendor_insights.services.commonQueryService.FetchDataService) || within(com.swiggy.vendor_insights.services.restaurantService.FetchRestaurantDetails) ) && allMethod() && args(..,requestObj)")
    public void logBefore(JoinPoint joinPoint, @RequestBody RequestObj requestObj) {

        log.info("Entering in Method :  " + joinPoint.getSignature() +
                 " with arguments " + Arrays.toString(joinPoint.getArgs()));
    }
}