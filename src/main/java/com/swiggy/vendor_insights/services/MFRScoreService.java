package com.swiggy.vendor_insights.services;

import com.swiggy.vendor_insights.cache.MfrScoreCache;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.exceptions.exceptions.BadRequestException;
import com.swiggy.vendor_insights.exceptions.exceptions.MfrException;
import com.swiggy.vendor_insights.helpers.transformationlayer.MFRScoreTransformation;
import com.swiggy.vendor_insights.pojos.api.MFRScoreData;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.utils.CommonUtils;
import com.swiggy.vendor_insights.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static com.swiggy.vendor_insights.constants.MfrConstants.MONTH_ON_MONTH;
import static com.swiggy.vendor_insights.constants.MfrConstants.WEEK_ON_WEEK;
import static java.time.temporal.TemporalAdjusters.firstDayOfMonth;
import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;

@Slf4j
@Service
public class MFRScoreService {

    @Autowired
    private MfrScoreCache mfrScoreCache;

    @Autowired
    private MFRScoreTransformation transformation;

    public List<MFRScoreData> getMfrScore(List<String> restaurantIds, String fromDate, String toDate, String modelName) throws MfrException, BadRequestException {

        List<MFRScoreData> mfrScores = new ArrayList<>();

        for (String restId: restaurantIds) {
            Long restaurantId = CommonUtils.parseRestaurantId(restId);
            mfrScores.add(MFRScoreData.builder()
                    .restaurantId(restaurantId)
                    .mfrScore( (StringUtils.isEmpty(fromDate) || StringUtils.isEmpty(toDate))
                            ? getModelsMfrScore(modelName, restaurantId)
                            : getCustomMfrScore(restaurantId, DateUtils.convertToLocalDateTime(fromDate),
                            DateUtils.convertToLocalDateTime(toDate)))
                    .build());
        }

        return mfrScores;
    }


    private MFRScoreData.MfrScore getCustomMfrScore(Long restId, LocalDateTime startDate, LocalDateTime endDate) {
        MFRScoreData.MfrScore mfrScore = new MFRScoreData.MfrScore();
        Double customMfr = calculateMfrScore(restId, startDate, endDate);
        mfrScore.setCustomMfr(customMfr);
        return mfrScore;
    }

    private MFRScoreData.MfrScore getModelsMfrScore(String modelName, Long restaurantId) {
        MFRScoreData.MfrScore mfrScore = new MFRScoreData.MfrScore();
        Double wowScore = mfrScoreCache.getMFRScore(restaurantId, WEEK_ON_WEEK).orElseGet(() -> getWOWMfrScore(restaurantId));
        Double momScore = mfrScoreCache.getMFRScore(restaurantId, MONTH_ON_MONTH).orElseGet(() -> getMOMMfrScore(restaurantId));

        if(StringUtils.isEmpty(modelName)) {
            mfrScore.setWow(wowScore);
            mfrScore.setMom(momScore);
        } else if(WEEK_ON_WEEK.equals(modelName)) {
            mfrScore.setWow(wowScore);
        } else if(MONTH_ON_MONTH.equals(modelName)){
            mfrScore.setMom(momScore);
        } else{
            throw new BadRequestException("invalid model name");
        }
        return mfrScore;
    }


    private Double getMOMMfrScore(Long restId) {
        Double momScore;
        LocalDateTime startDate, endDate;
        log.info("Mom not found in cache");

        startDate = LocalDateTime.now().minusMonths(1);
        startDate = startDate.with(firstDayOfMonth()).with(LocalTime.MIN);
        endDate = startDate.with(lastDayOfMonth()).with(LocalTime.MAX);

        log.info("This is the starting date for mom: " + startDate);

        momScore = calculateMfrScore(restId, startDate, endDate);

        Long ttl = ChronoUnit.SECONDS.between(LocalDateTime.now(), LocalDateTime.now().with(lastDayOfMonth()).with(LocalTime.MAX));

        mfrScoreCache.setMFRScore(restId, momScore, MONTH_ON_MONTH, ttl);
        return momScore;
    }


    private Double getWOWMfrScore(Long restaurantId) {
        Double wowScore;
        LocalDateTime endDate, startDate;
        log.info("Wow not found in cache");

        startDate = LocalDateTime.now().minusWeeks(1);
        startDate = startDate.minusDays(startDate.getDayOfWeek().getValue() - 1).with(LocalTime.MIN);
        endDate = startDate.plusDays(7);
        log.info("This is the starting date for wow: " + startDate);

        wowScore = calculateMfrScore(restaurantId, startDate, endDate);

        endDate = endDate.plusDays(7);
        Long ttl = ChronoUnit.SECONDS.between(LocalDateTime.now(), endDate);

        mfrScoreCache.setMFRScore(restaurantId, wowScore, WEEK_ON_WEEK, ttl);
        return wowScore;
    }

    public Double calculateMfrScore(Long restaurantId, LocalDateTime startDate, LocalDateTime endDate) {

        List<Template> keys = new ArrayList<>();

        log.info("Restaurant Id : {}, Start Date : {}, End Date : {}" , restaurantId
                , DateUtils.convertToUTC(startDate), DateUtils.convertToUTC(endDate));

        List<Long> restaurantIds = new ArrayList<>();
        restaurantIds.add(restaurantId);
        RequestObj requestObj = CommonUtils.buildRequestObj(restaurantIds, startDate, endDate);
        keys.add(Template.MFR_SCORE_KEY);
        List<Long> queryResult = transformation.transform(keys, requestObj);
        Long totalOrders = queryResult.get(0), accurateOrders = queryResult.get(1);

        log.info("Restaurant Id : {}, Total orders : {}, Accurate orders : {}" , restaurantId, totalOrders, accurateOrders);

        return totalOrders == 0L ? 0.0 : ((double) accurateOrders / totalOrders) * 100;
    }

}
