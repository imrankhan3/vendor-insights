package com.swiggy.vendor_insights.services.commonQueryService;


import com.swiggy.commons.response.Response;
import com.swiggy.vendor_insights.pojos.requestobj.QueryRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by sandeepjindal on 5/15/17.
 */
public interface CommonQueryClient {

    @POST(value = "/v1/query/")
    Call<Response> getCQLData(@Body QueryRequest queryRequest);
}
