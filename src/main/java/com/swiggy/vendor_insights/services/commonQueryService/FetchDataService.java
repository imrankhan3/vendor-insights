package com.swiggy.vendor_insights.services.commonQueryService;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.commons.response.Response;
import com.swiggy.vendor_insights.exceptions.exceptions.ThirdPartyServiceFailureException;
import com.swiggy.vendor_insights.pojos.bo.BaseBo;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.requestobj.QueryRequest;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.services.EndPointInterface;
import com.swiggy.vendor_insights.utils.RetrofitUtils;
import com.swiggy.vendor_insights.utils.Settings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.swiggy.vendor_insights.utils.Settings.DAYS;
import static com.swiggy.vendor_insights.utils.Settings.REST_ID;
import static com.swiggy.vendor_insights.utils.Settings.SLOTS;
import static com.swiggy.vendor_insights.utils.Settings.ORDER_IDS;

/**
 * Created by sandeepjindal1 on 5/23/17.
 */
@Service
@Slf4j
public class FetchDataService implements EndPointInterface {

    @Autowired
    private CommonQueryClient commonQueryClient;

    @Autowired
    RedisTemplate redisTemplate;

    public QueryResult getDataByTemplateIds(QueryRequest queryRequest) {
        Response dataServiceResponse = RetrofitUtils.execute(commonQueryClient.getCQLData(queryRequest), new Response<>());
        if(dataServiceResponse.getStatusCode() == 0){
            throw new ThirdPartyServiceFailureException(dataServiceResponse.getStatusMessage());
        }
        return new ObjectMapper().convertValue(dataServiceResponse.getData(), QueryResult.class);
    }

    @Override
    public BaseBo execute(Optional<Long> templateId, Object requestObj) {

        if(!templateId.isPresent()){

            //TODO:: exceptions need to handled

            return new BaseBo();
        }

        return getDataByTemplateIds(getQueryRequest(templateId.get(), (RequestObj)requestObj));

    }

    // TODO: get the set before hand and check for the fields afterwards

    private QueryRequest getQueryRequest(Long templateId, RequestObj requestObj) {
        Map<String, Object> placeholders = new HashMap<>();
        try {
            for (PropertyDescriptor field : Introspector.getBeanInfo(RequestObj.class).getPropertyDescriptors()) {
                Object value = field.getReadMethod().invoke(requestObj);
                if (field.getReadMethod() != null && null != value && redisTemplate.opsForSet().isMember(Settings.KEY_LIST,field.getName())) {
                    if(field.getName().equals(REST_ID) || ORDER_IDS.equals(field.getName()) || Settings.CAMPAIGN_ID.equals(field.getName())){
                        placeholders.put(field.getName(), ((List)value).stream().map(Object::toString)
                                .collect(Collectors.joining(",")));
                    } else {
                        placeholders.put(field.getName(), value);
                    }

                } else if (field.getName().equals(DAYS) && null != value) {
                    placeholders.put(field.getName(), setDays(requestObj));
                } else if (field.getName().equals(SLOTS) && null != value) {
                    placeholders.put(field.getName(), setSlots(requestObj));
                }
            }

        }catch(Exception e){
            log.info(String.valueOf(e));
        }

        return QueryRequest.builder()
                .placeHolders(placeholders)
                .templateId(templateId).build();
    }

    private String setDays(RequestObj requestObj) {
        if (requestObj.getDays().size() == 0) {
            return "";
        }
        String days = requestObj.getDays().stream().map(Object::toString)
                .collect(Collectors.joining(","));
        return " and dayofweek(ord.ordered_time) in (" + days + ") ";
    }

    private String setSlots(RequestObj requestObj) {
        StringBuilder sb = new StringBuilder();
        if (requestObj.getSlots() != null && requestObj.getSlots().size() > 0) {
            for (RequestObj.Slot slot : requestObj.getSlots()) {
                String start = slot.getStart();
                String end = slot.getEnd();
                sb.append(" and TIME(ord.ordered_time) BETWEEN \'" + start + "\' AND \'" + end + "\' ");
            }
        }
        return sb.toString();
    }

}
