package com.swiggy.vendor_insights.services.SellerTieringService;

import com.swiggy.vendor_insights.exceptions.exceptions.RestaurantNotFoundException;
import com.swiggy.vendor_insights.exceptions.exceptions.ThirdPartyServiceFailureException;
import com.swiggy.vendor_insights.pojos.bo.BaseBo;
import com.swiggy.vendor_insights.pojos.bo.RestaurantServiceResponse;
import com.swiggy.vendor_insights.pojos.bo.SellerTieringResult;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.requestobj.SellerTieringRequest;
import com.swiggy.vendor_insights.services.EndPointInterface;
import com.swiggy.vendor_insights.services.restaurantService.RestaurantServiceClient;
import com.swiggy.vendor_insights.utils.RetrofitUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * Created by sandeepjindal on 5/17/17.
 */
@Component
public class FetchSellerTierData {


    @Autowired
    private SellerTieringClient sellerTieringClient;


    public SellerTieringResult getSellerTierData(SellerTieringRequest sellerTieringRequest)  {
        SellerTieringResult sellerTieringResult = RetrofitUtils.execute(sellerTieringClient.getSellerTieringData(sellerTieringRequest), new SellerTieringResult());
        if(sellerTieringResult.getStatus() == 0){
            throw new ThirdPartyServiceFailureException(sellerTieringResult.getMessage());
        }
        return sellerTieringResult;
    }

    public BaseBo execute(SellerTieringRequest sellerTieringRequest) {

       return getSellerTierData(sellerTieringRequest);
    }
}
