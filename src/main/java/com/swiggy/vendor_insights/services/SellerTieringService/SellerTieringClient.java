package com.swiggy.vendor_insights.services.SellerTieringService;

import com.swiggy.vendor_insights.pojos.bo.SellerTieringResult;
import com.swiggy.vendor_insights.pojos.requestobj.SellerTieringRequest;
import com.swiggy.vendor_insights.pojos.responseobj.SellerTieringResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

import java.util.List;


public interface SellerTieringClient {

    /**
     *
     * @param sellerTieringRequest
     * @return
     */

    @POST(value = "/seller-tier/v1/status")
    Call<SellerTieringResult> getSellerTieringData(@Body SellerTieringRequest sellerTieringRequest);

}
