package com.swiggy.vendor_insights.services;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.transformationlayer.MFRMetricsTransformation;
import com.swiggy.vendor_insights.helpers.transformationlayer.MFRStatusTransformation;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.utils.CommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;



@Slf4j
@Component
public class MFRStatusReportService {

    @Autowired
    private MFRStatusTransformation transformation;

    @Autowired
    private MFRMetricsTransformation mfrMetricsTransformation;

    public Map<Long, List<Object>> getMfrStatus(List<Long> orderIds) {
        List<Template> keys = new ArrayList<>();
        RequestObj requestObj = RequestObj.builder()
                .order_ids(orderIds).build();
        keys.add(Template.MFR_STATUS_KEY);
        return transformation.transform(keys, requestObj);
    }
    
    public Map<String, Object> getMfrMetrics(Long restaurantId,  LocalDateTime startDate,  LocalDateTime endDate) {
        List<Template> keys = new ArrayList<>();
        List<Long> restaurantIds = new ArrayList<>();
        restaurantIds.add(restaurantId);
        RequestObj requestObj = CommonUtils.buildRequestObj(restaurantIds, startDate, endDate);
        log.info("RequestObj for mfr metrics is: {}", requestObj);
        keys.add(Template.MFR_METRICS_KEY);
        return mfrMetricsTransformation.transform(keys, requestObj);
    }
}
