package com.swiggy.vendor_insights.services.restaurantService;

import com.swiggy.vendor_insights.exceptions.exceptions.RestaurantNotFoundException;
import com.swiggy.vendor_insights.pojos.bo.BaseBo;
import com.swiggy.vendor_insights.pojos.bo.RestaurantServiceResponse;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.services.EndPointInterface;
import com.swiggy.vendor_insights.utils.RetrofitUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * Created by sandeepjindal on 5/17/17.
 */
@Component
public class FetchRestaurantDetails implements EndPointInterface {


    @Autowired
    private RestaurantServiceClient restaurantServiceClient;


    public RestaurantServiceResponse getRestaurantByFilters(List<Long> restIds)  {
        RestaurantServiceResponse restaurantServiceResponse = RetrofitUtils.execute(restaurantServiceClient.getRestaurantsByFilters(restIds), new RestaurantServiceResponse());
        validateResponse(restaurantServiceResponse);
        return restaurantServiceResponse;
    }

    private void validateResponse(RestaurantServiceResponse restaurantServiceResponse) {
        if (null == restaurantServiceResponse || restaurantServiceResponse.getStatus() != 1) {
            new RestaurantNotFoundException("no restaurant found for filters" + restaurantServiceResponse);
        }
    }

    @Override
    public BaseBo execute(Optional<Long> templateId, Object object) {

        List<Long> list = ((RequestObj)object).getRest_id();
       return getRestaurantByFilters(list);
    }
}
