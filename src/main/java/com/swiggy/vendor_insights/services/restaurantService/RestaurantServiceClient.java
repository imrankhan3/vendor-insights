package com.swiggy.vendor_insights.services.restaurantService;

import com.swiggy.vendor_insights.pojos.bo.RestaurantServiceResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

import java.util.List;

/**
 * Created by sandeepjindal on 3/21/17.
 */
public interface RestaurantServiceClient {

    /**
     *
     * @param restIds
     * @return
     */

    @POST(value = "/restaurant/find/by")
    Call<RestaurantServiceResponse> getRestaurantsByFilters(@Body List<Long> restIds);
}
