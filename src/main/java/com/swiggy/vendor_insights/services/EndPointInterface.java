package com.swiggy.vendor_insights.services;

import com.swiggy.vendor_insights.pojos.bo.BaseBo;

import java.util.Optional;

/**
 * Created by sandeepjindal1 on 5/23/17.
 */
public interface EndPointInterface {

    BaseBo execute(Optional<Long> templateId, Object object);

}
