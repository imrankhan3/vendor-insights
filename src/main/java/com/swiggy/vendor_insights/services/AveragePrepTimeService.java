package com.swiggy.vendor_insights.services;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.transformationlayer.PrepTimeTransformation;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.AveragePrepTimeResponse;
import com.swiggy.vendor_insights.utils.CommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class AveragePrepTimeService {

    @Autowired
    private PrepTimeTransformation prepTimeTransformation;

    public AveragePrepTimeResponse getAveragePrepTime (String restaurantId, LocalDateTime startDate, LocalDateTime endDate) {
        List<Template> keys = new ArrayList<>();
        List<Long> restaurantIds = new ArrayList<>();
        restaurantIds.add(CommonUtils.parseRestaurantId(restaurantId));

        RequestObj requestObj = CommonUtils.buildRequestObj(restaurantIds, startDate, endDate);
        keys.add(Template.PREP_TIME_HOURLY);
        keys.add(Template.AVERAGE_PREP_TIME);
        return prepTimeTransformation.transform(keys, requestObj);
    }
}
