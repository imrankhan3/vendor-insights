package com.swiggy.vendor_insights;

import com.swiggy.vendor_insights.services.SellerTieringService.SellerTieringClient;
import com.swiggy.vendor_insights.services.commonQueryService.CommonQueryClient;
import com.swiggy.vendor_insights.services.restaurantService.RestaurantServiceClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.util.concurrent.TimeUnit;


/**
 * Created by sandeepjindal on 5/15/17.
 */
@Configuration
public class VendorInsightsApplicationConfig {

    @Value("${swiggy.cqi.url}")
    private String COMMON_QUERY_INTERFACE;

    @Value("${swiggy.restaurant.service.url}")
    private String SWIGGY_RESTAURANT_SERVICE_CLIENT;

    @Value("${connect_timeout_ms}")
    private Integer CONNECT_TIMEOUT_MS;

    @Value("${read_timeout_ms}")
    private Integer READ_TIMEOUT_MS;

    @Value("${write_timeout_ms}")
    private Integer WRITE_TIMEOUT_MS;

    @Bean
    public okhttp3.OkHttpClient getHttpClient(){
        return new okhttp3.OkHttpClient.Builder()
                .addNetworkInterceptor(chain -> {
                    okhttp3.Request request = chain
                            .request()
                            .newBuilder()
                            .addHeader("Content-Type", "application/json")
                            .build();
                    return chain.proceed(request);
                })
                .connectTimeout(CONNECT_TIMEOUT_MS, TimeUnit.MILLISECONDS) // 2 second
                .readTimeout(READ_TIMEOUT_MS, TimeUnit.MILLISECONDS) // 10 seconds
                .writeTimeout(WRITE_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                .build();
    }

    @Bean
    public RestaurantServiceClient restaurantServiceClient(){
        return new Retrofit.Builder()
                .baseUrl(SWIGGY_RESTAURANT_SERVICE_CLIENT)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(getHttpClient())
                .build()
                .create(com.swiggy.vendor_insights.services.restaurantService.RestaurantServiceClient.class);
    }

    @Bean
    public CommonQueryClient commonQueryClient(){
        return new Retrofit.Builder()
                .baseUrl(COMMON_QUERY_INTERFACE)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(getHttpClient())
                .build()
                .create(CommonQueryClient.class);
    }

    @Bean
    public SellerTieringClient sellerTieringClient(){
        return new Retrofit.Builder()
                .baseUrl(SWIGGY_RESTAURANT_SERVICE_CLIENT)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(getHttpClient())
                .build()
                .create(SellerTieringClient.class);
    }

}
