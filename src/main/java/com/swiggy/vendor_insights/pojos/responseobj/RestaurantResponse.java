package com.swiggy.vendor_insights.pojos.responseobj;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.commons.response.Response;
import lombok.*;

/**
 * Created by sandeepjindal on 5/16/17.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString
public class RestaurantResponse extends Response{

    @JsonProperty("rest_name")
    private String restName;

    @JsonProperty("rest_id")
    private Integer restId;

    @JsonProperty("rest_area")
    private String restArea;

    @JsonProperty("enabled")
    private Boolean enabled;

    @JsonProperty("category")
    private String category;

    @JsonProperty("sa")
    private Boolean sa;

    @JsonProperty("average_rating")
    private Double averageRating;

    @JsonProperty("this_week_rating")
    private String thisWeekRating;

}
