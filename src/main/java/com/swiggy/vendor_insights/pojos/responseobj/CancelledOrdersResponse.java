package com.swiggy.vendor_insights.pojos.responseobj;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * Created by sandeepjindal1 on 5/29/17.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString
public class CancelledOrdersResponse extends InsightsBaseResponse{

    @JsonProperty("order_count")
    private Integer orderCount;

    @JsonProperty("reason")
    private String reason;

    @JsonProperty("initiated_by")
    private String initiatedBy;

    @JsonProperty("total_losses")
    private Double totalLosses;

    @JsonProperty("losses")
    private Double losses;

    @JsonProperty("cancellation_percent")
    private Double cancellationPercent;

    @JsonProperty("reason_id")
    private Integer reasonId;

}
