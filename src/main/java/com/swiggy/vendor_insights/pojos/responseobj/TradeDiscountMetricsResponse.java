package com.swiggy.vendor_insights.pojos.responseobj;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TradeDiscountMetricsResponse extends InsightsBaseResponse {

    private Integer restaurantId;
    private Double revenue;
    private Double spend;
    private Integer discountedOrders;
    private Integer totalOrders;
    private Integer newCustomers;
}
