package com.swiggy.vendor_insights.pojos.responseobj.rating;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * Created by sandeepjindal1 on 5/29/17.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString
public class RatingReasonDistributionResponse {

    @JsonProperty("count")
    private Integer count;

    @JsonProperty("reason")
    private String reason;

    @JsonProperty("share")
    private Double share;

}
