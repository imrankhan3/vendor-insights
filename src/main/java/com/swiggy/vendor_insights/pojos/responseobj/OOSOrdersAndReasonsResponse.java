package com.swiggy.vendor_insights.pojos.responseobj;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;


/**
 * Created by sandeepjindal on 5/16/17.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString
public class OOSOrdersAndReasonsResponse extends InsightsBaseResponse {

    @JsonProperty("alter_provided_delivered")
    private Integer alterProvidedDelivered;

    @JsonProperty("alter_not_accepted_cancelled")
    private Integer alterNotAcceptedCancelled;

    @JsonProperty("total_orders")
    private Integer totalOrders;

    @JsonProperty("delivered_orders")
    private Integer deliveredOrders;
}
