package com.swiggy.vendor_insights.pojos.responseobj;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class GrowthPackResponse extends InsightsBaseResponse {

    private Integer totalOrders;
    private Integer newCustomers;
    private Integer oldCustomers;
    private Integer restaurantId;
    private Double GMV;
    private Double totalInvestment;
    private List<Integer> campaignId;
    private String status;
}
