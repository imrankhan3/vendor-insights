package com.swiggy.vendor_insights.pojos.responseobj.rating;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import lombok.*;

import java.util.List;

/**
 * Created by sandeepjindal1 on 5/29/17.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString
public class RatingsResponse extends InsightsBaseResponse {

    @JsonProperty("model_based_rating")
    private Double modelBasedRating;

    @JsonProperty("rated_orders")
    private Integer ratedOrders;

    @JsonProperty("reasons")
    private List<RatingReasonDistributionResponse> reasons;

    @JsonProperty("ratings")
    private List<RatingDistributionResponse> ratings;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RatingsResponse that = (RatingsResponse) o;

        if (!modelBasedRating.equals(that.modelBasedRating)) return false;
        if (!ratedOrders.equals(that.ratedOrders)) return false;
        if (!reasons.equals(that.reasons)) return false;
        return ratings.equals(that.ratings);
    }

    @Override
    public int hashCode() {
        int result = modelBasedRating.hashCode();
        result = 31 * result + ratedOrders.hashCode();
        result = 31 * result + reasons.hashCode();
        result = 31 * result + ratings.hashCode();
        return result;
    }
}
