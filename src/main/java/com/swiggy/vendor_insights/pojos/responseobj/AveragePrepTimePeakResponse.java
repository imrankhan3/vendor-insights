package com.swiggy.vendor_insights.pojos.responseobj;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString
public class AveragePrepTimePeakResponse {

    @JsonProperty("peak")
    private String peak;

    @JsonProperty("averagePrepTime")
    private Double averagePrepTime;

    @JsonProperty("totalOrders")
    private Integer totalOrders;

    @JsonProperty("distribution")
    private List<AveragePrepTimeDistribution> averagePrepTimeDistributions;

}
