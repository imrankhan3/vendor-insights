package com.swiggy.vendor_insights.pojos.responseobj;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * Created by shahbaz.khalid on 5/24/17.
 */

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class RestaurantMetricsResponse extends InsightsBaseResponse {

    @JsonProperty("acceptance_rate")
    private double acceptanceRate;
    @JsonProperty("oos_rate")
    private double oosRate;
    private double rating;
    private double total;
}
