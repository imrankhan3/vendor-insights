package com.swiggy.vendor_insights.pojos.responseobj;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.vendor_insights.pojos.bo.SellerTieringResult;
import lombok.*;

import java.util.Map;


/**
 * Created by sandeepjindal on 5/16/17.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString
public class SellerTieringResponse extends InsightsBaseResponse {

    @JsonProperty("revenue")
    private Double revenue;

    @JsonProperty("cancellation_rate")
    private Double cancellationRate;

    @JsonProperty("acceptance_rate")
    private Double acceptenceRate;

    @JsonProperty("edit_rate")
    private Double editRate;

    @JsonProperty("rating")
    private Double rating;

    @JsonProperty("seller_tier_data")
    private Map<String, Object> sellerTieringData;

    @JsonProperty("message")
    private String  message;

    @JsonProperty("is_message_positive")
    private Boolean isMessagePositive;


}
