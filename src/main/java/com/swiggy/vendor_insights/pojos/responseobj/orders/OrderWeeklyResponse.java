package com.swiggy.vendor_insights.pojos.responseobj.orders;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import lombok.*;

/**
 * Created by sandeepjindal1 on 5/30/17.
 */

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString
public class OrderWeeklyResponse extends InsightsBaseResponse {

    @JsonProperty("no_of_orders")
    private Integer numberOfOrders;

    @JsonProperty("no_of_cancelled_orders")
    private Integer numberOfCancelledOrders;

    @JsonProperty("no_of_delivered_orders")
    private Integer numberOfDeliveredOrders;

    @JsonProperty("revenue")
    private Double revenue;

    @JsonProperty("losses")
    private Double losses;

    @JsonProperty("start_date")
    private String startDate;

    @JsonProperty("end_date")
    private String endDate;

    @JsonProperty("week")
    private Integer week;
}
