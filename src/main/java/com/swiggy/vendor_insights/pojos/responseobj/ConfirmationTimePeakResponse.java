package com.swiggy.vendor_insights.pojos.responseobj;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.vendor_insights.enums.Peaks;
import lombok.*;

import java.util.List;

/**
 * Created by sandeepjindal on 5/16/17.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString
public class ConfirmationTimePeakResponse extends InsightsBaseResponse {

    @JsonProperty("avg_confirmation_time")
    private Double avgConfirmationTime;

    @JsonProperty("min_confirmation_time")
    private Double minConfirmationTime;

    @JsonProperty("max_confirmation_time")
    private Double maxConfirmationTime;

    @JsonProperty("order_confirmation_percentage")
    private Double orderConfirmationPercentage;

    @JsonProperty("zero_three_min")
    private Integer zeroThreeMin;

    @JsonProperty("three_six_min")
    private Integer threeSixMin;

    @JsonProperty("six_nine_min")
    private Integer sixNineMin;

    @JsonProperty("nine_plus")
    private Integer ninePlus;

    @JsonProperty("confirmed_by_de")
    private Integer confirmedByDe;

    @JsonProperty("not_accepted")
    private Integer notAccepted;

    @JsonProperty("no_of_orders")
    private Integer noOfOrders;

    @JsonProperty("peak")
    private Peaks peak;

    @JsonProperty("distribution")
    private List<ConfirmationTimeHourlyResponse> confirmationHourlyResponse;

    @JsonProperty("accepted_orders")
    private Integer acceptedOrders;
}
