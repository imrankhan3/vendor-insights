package com.swiggy.vendor_insights.pojos.responseobj;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * Created by sandeepjindal on 5/16/17.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString
public class OpsMetricsResponse extends InsightsBaseResponse{

    @JsonProperty("confirmation_time")
    private Double confirmationTime;

    @JsonProperty("accept_rate")
    private Double acceptRate;

    @JsonProperty("edit_rate")
    private Double editRate;

    @JsonProperty("cancel_rate")
    private Double cancelRate;

}
