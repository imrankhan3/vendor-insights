package com.swiggy.vendor_insights.pojos.responseobj;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.ItemResponse;
import com.swiggy.vendor_insights.pojos.responseobj.MenuResponse;
import lombok.*;

import java.util.List;

/**
 * Created by sandeepjindal on 5/16/17.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString
public class OrderMetricResponse extends InsightsBaseResponse {

    @JsonProperty("top_delivered_orders")
    private List<ItemResponse> topDeliveredOrders;

    @JsonProperty("top_oos_orders")
    private List<ItemResponse> topOosOrders;
}
