package com.swiggy.vendor_insights.pojos.requestobj;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class GrowthPackObject {


    @JsonProperty("past")
    GrowthPackCampaign past;

    @JsonProperty("current")
    GrowthPackCampaign current;

}
