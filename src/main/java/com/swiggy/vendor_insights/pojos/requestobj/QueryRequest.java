package com.swiggy.vendor_insights.pojos.requestobj;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class QueryRequest {
    @JsonProperty("template_id")
    @NotNull
    private Long templateId;

    @NotNull
    @JsonProperty("placeholders")
    Map<String, Object> placeHolders;
}
