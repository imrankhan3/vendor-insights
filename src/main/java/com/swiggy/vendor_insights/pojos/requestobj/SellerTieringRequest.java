package com.swiggy.vendor_insights.pojos.requestobj;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.NotNull;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class SellerTieringRequest {

    @JsonProperty("restId")
    @NotNull
    private Long restId;

    @JsonProperty("acceptanceRate")
    @NotNull
    private Double acceptanceRate;

    @JsonProperty("cancellationRate")
    @NotNull
    private Double cancellationRate;

    @JsonProperty("editRate")
    @NotNull
    private Double editRate;

    @JsonProperty("totalSales")
    @NotNull
    private Double totalSales;

    @JsonProperty("rating")
    @NotNull
    private Double rating;

}
