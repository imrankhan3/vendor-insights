package com.swiggy.vendor_insights.pojos.requestobj;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


import java.util.List;
import java.util.Map;

@Data
public class GrowthPackHistoryRequest {

    @JsonProperty("growth_pack_metrics_history")
    Map<String, List<GrowthPackCampaign>> growthPackHistoryMap;
}
