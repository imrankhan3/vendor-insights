package com.swiggy.vendor_insights.pojos.requestobj;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

/**
 * Created by sandeepjindal on 5/16/17.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class RequestObj{

    @JsonProperty("user_id")
    private String user_id;

    private String start_date;

    private String end_date;

    private List<Long> rest_id;

    private Integer city_id;

    private String case_identifier;

    private String group_by;

    private Integer category_id;

    private Integer subcategory_id;

    private String category;

    private String sub_category;

    private String key;

    private String context;

    private String offerId;

    private Long order_id;

    private List<Long> order_ids;

    private List<Integer> days;

    private List<Slot> slots;

    private List<Integer> campaign_id;

    private String status;

    private List<GrowthPackCampaign> listOfGrowthPackCampaign;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Slot {
        private String start;
        private String end;
    }

}
