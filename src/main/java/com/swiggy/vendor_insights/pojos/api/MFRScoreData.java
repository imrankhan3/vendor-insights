package com.swiggy.vendor_insights.pojos.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.NoArgsConstructor;
import lombok.Data;
import lombok.Builder;


import javax.validation.constraints.NotNull;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MFRScoreData {

    @NotNull
    @NonNull
    @JsonProperty("restaurant_id")
    private Long restaurantId;

    @JsonProperty("mfr")
    private MfrScore mfrScore;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class MfrScore {

        @JsonProperty("wow")
        private Double wow;

        @JsonProperty("mom")
        private Double mom;

        @JsonProperty("custom")
        private Double customMfr;
    }
}


