package com.swiggy.vendor_insights.pojos.api;

public class ErrorResponse extends BaseResponse {

    public ErrorResponse(Object data) {
        super(0, "Error!", data);
    }

    public ErrorResponse(String status, Object data) {
        super(0, status, data);
    }

    public ErrorResponse(String status) {
        super(0, status, null);
    }
}
