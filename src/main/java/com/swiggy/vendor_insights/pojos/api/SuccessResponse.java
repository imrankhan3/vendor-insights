package com.swiggy.vendor_insights.pojos.api;

public class SuccessResponse extends BaseResponse {

    public SuccessResponse(Object data) {
        super(1, "Success!", data);
    }

    public SuccessResponse(String status, Object data) {
        super(1, status, data);
    }

    public SuccessResponse(){
        super();
    }

}
