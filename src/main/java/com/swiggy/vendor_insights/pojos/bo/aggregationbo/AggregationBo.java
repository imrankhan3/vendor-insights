package com.swiggy.vendor_insights.pojos.bo.aggregationbo;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.pojos.bo.BaseBo;
import lombok.*;

import java.util.Map;

/**
 * Created by sandeepjindal on 5/17/17.
 */
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AggregationBo {

    private Map<Template, BaseBo> data;
}
