package com.swiggy.vendor_insights.pojos.bo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
@ToString
public class TierInfo {

    private String tier;

    private Boolean acceptanceRate;

    private Boolean editRate;

    private Boolean cancellationRate;

    private Boolean totalSales;

    private Boolean rating;

}
