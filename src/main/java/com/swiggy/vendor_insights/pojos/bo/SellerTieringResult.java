package com.swiggy.vendor_insights.pojos.bo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.swiggy.vendor_insights.pojos.bo.BaseBo;
import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
@ToString
public class SellerTieringResult extends BaseBo{
    private Integer status;
    private String message;
    private String code;
}
