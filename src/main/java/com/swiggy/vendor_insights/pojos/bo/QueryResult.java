package com.swiggy.vendor_insights.pojos.bo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;
import java.util.Map;

/**
 * Created by sandeepjindal on 4/25/17.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QueryResult extends BaseBo{

    @JsonProperty("meta_info")
    Map<String, Object> metaInfo;

    @JsonProperty("data")
    List<Map<String,Object>> result;
}
