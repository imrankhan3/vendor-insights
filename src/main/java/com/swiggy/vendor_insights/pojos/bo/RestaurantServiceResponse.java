package com.swiggy.vendor_insights.pojos.bo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

/**
 * Created by sandeepjindal on 3/21/17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
@ToString
public class RestaurantServiceResponse extends BaseBo{
    private Long status;
    private String code;
    private String message;
}
