package com.swiggy.vendor_insights.enums;

/**
 * Created by sandeepjindal1 on 6/27/17.
 */
public enum ResponseType {
    ITEM(0),
    MENU(1),
    ITEM_WITH_OOS(2);

    private Integer value;
    ResponseType(int i) {
        this.value=i;
    }

    public static Integer getValue(ResponseType responseType){
        return responseType.value;
    }
}

