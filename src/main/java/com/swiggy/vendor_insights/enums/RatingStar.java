package com.swiggy.vendor_insights.enums;

/**
 * Created by sandeepjindal1 on 5/30/17.
 */
public enum RatingStar {
    ONE_STAR("one_star"),
    TWO_STAR("two_star"),
    THREE_STAR("three_star"),
    FOUR_STAR("four_star"),
    FIVE_STAR("five_star");

    private String value;

    RatingStar(String value){
        this.value = value;
    }

    public static String getValue(RatingStar ratingStar){
        return ratingStar.value;
    }

    public static RatingStar getRatingStar(String value){

        for(RatingStar ratingStar : RatingStar.values()){
            if(ratingStar.value.equals(value)){
                return ratingStar;
            }
        }
        return null;
    }

    public static boolean contains(String key) {
        for(RatingStar ratingStar : RatingStar.values()){
            if(ratingStar.value.equals(key)){
                return true;
            }
        }
        return false;
    }
}
