package com.swiggy.vendor_insights.enums;

/**
 * Created by sandeepjindal1 on 6/6/17.
 */
public enum Peaks {

    BREAKFAST, LUNCH, DINNER, SNACKS, LATE_NIGHT
}
