package com.swiggy.vendor_insights.enums;

/**
 * Created by sandeepjindal on 5/16/17.
 */
public enum Template {
        ORDERS,
        RESTAURANTS,
        RATINGS,
        REVENUE,
        TRADEDISCOUNT,
        METRICS,
        CITY,
        CONFIRMATION_TIME,
        ORDERS_HOURLY,
        ITEMS,
        CATEGORY,
        SUBCATEGORY,
        ITEMSCATEGORYBASIS,
        CANCELLATION,
        ORDERS_OOS,
        ORDERS_MONTHLY,
        OPS_METRIC,
        RAW_ORDER,
        ORDERS_WEEKLY,
        SRS,
        OOS_ITEMS,
        CONFIRMATION_TIME_HOURLY,
        LAST_UPDATED_TIME,
        RATING_FOR_ORDER,
        SELLER_TIER_API,
        SELLER_TIER_STATS,
        TD_METRICS,
        MFR_SCORE_KEY,
        MFR_STATUS_KEY,
        MFR_METRICS_KEY,
        PREP_TIME_HOURLY,
        AVERAGE_PREP_TIME,
        GROWTH_PACK_METRICS,
        GROWTH_PACK_HISTORY_METRICS
}
