package com.swiggy.vendor_insights.helpers.aggregationlayer;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;

import java.util.List;

/**
 * Created by sandeepjindal1 on 5/23/17.
 */

public interface AggregationInterface {

    AggregationBo aggregate(List<Template> keys, RequestObj requestObj);

}
