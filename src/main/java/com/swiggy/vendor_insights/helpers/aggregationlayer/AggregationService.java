package com.swiggy.vendor_insights.helpers.aggregationlayer;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.pojos.bo.BaseBo;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.services.commonQueryService.FetchDataService;
import com.swiggy.vendor_insights.services.restaurantService.FetchRestaurantDetails;
import com.swiggy.vendor_insights.utils.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by sandeepjindal1 on 5/18/17.
 */
@Service
public class AggregationService implements AggregationInterface {

    @Autowired
    FetchRestaurantDetails fetchRestaurantDetails;

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    FetchDataService fetchDataService;
    

    @Override
    public AggregationBo aggregate(List<Template> keys, RequestObj requestObj){
        Map<Template, BaseBo> baseBoMap = new HashMap<>();

        for (Template key : keys) {
            BaseBo bo = null;
            if(key.equals(Template.SRS)){
                bo = fetchRestaurantDetails.execute(Optional.empty(), requestObj);
            } else {
                 bo = fetchDataService.execute(Optional.of(Long.parseLong((String) redisTemplate.opsForHash().get(Settings.TEMPLATE_MAP, key.toString()))), requestObj);
            }
            baseBoMap.put(key, bo);
        }

        return AggregationBo
                .builder()
                .data(baseBoMap)
                .build();

    }
}
