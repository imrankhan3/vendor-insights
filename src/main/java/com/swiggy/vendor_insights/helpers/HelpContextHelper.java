package com.swiggy.vendor_insights.helpers;

import com.swiggy.vendor_insights.utils.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sandeepjindal1 on 6/15/17.
 */
@Component
public class HelpContextHelper {

    @Autowired
    RedisTemplate redisTemplate;

    private Map<String, Map<String, Object>> contextMap;

    public String getMessage(String context, String key){
        try {
            return contextMap.get(context).get(key).toString();
        }catch (NullPointerException e){
            return null;
        }
    }

    @PostConstruct
    public void refresh(){
        contextMap = new HashMap<>();
        contextMap.put(Settings.MESSAGE, redisTemplate.opsForHash().entries(Settings.MESSAGE));
        contextMap.put(Settings.CONFIG, redisTemplate.opsForHash().entries(Settings.CONFIG));
        contextMap.put(Settings.HELP, redisTemplate.opsForHash().entries(Settings.HELP));
    }
}
