package com.swiggy.vendor_insights.helpers;

import com.swiggy.vendor_insights.pojos.requestobj.*;
import com.swiggy.vendor_insights.utils.Settings;
import lombok.NonNull;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Component
public class GrowthPackHelper {

    public RequestObj getGrowthPackMetrics(@NonNull GrowthPackRequest growthPackListObject, @NonNull String status) {

        List<Integer> campaignIds = new ArrayList<>();
        List<Long> restaurantIds = new ArrayList<>();

        if (Settings.PAST_STATUS.equals(status)) {
            for (Map.Entry<String, GrowthPackObject> entry : growthPackListObject.getGrowthPackObjects().entrySet()) {
                if (entry.getValue() != null && entry.getValue().getPast() != null
                        && CollectionUtils.isNotEmpty(entry.getValue().getPast().getCampaignIds())) {
                    restaurantIds.add(Long.valueOf(entry.getKey()));
                    campaignIds.addAll(entry.getValue().getPast().getCampaignIds());
                }
            }
            return (RequestObj.builder().rest_id(restaurantIds).campaign_id(campaignIds).status(status).build());

        } else if (Settings.CURRENT_STATUS.equals(status)) {
            for (Map.Entry<String, GrowthPackObject> entry : growthPackListObject.getGrowthPackObjects().entrySet()) {
                if (entry.getValue() != null && entry.getValue().getCurrent() != null
                        && CollectionUtils.isNotEmpty(entry.getValue().getCurrent().getCampaignIds())) {
                    restaurantIds.add(Long.valueOf(entry.getKey()));
                    campaignIds.addAll(entry.getValue().getCurrent().getCampaignIds());
                }
            }
            return (RequestObj.builder().rest_id(restaurantIds).campaign_id(campaignIds).status(status).build());
        }
        return RequestObj.builder().build();
    }


    public RequestObj getGrowthPackMetricsHistory(long restaurantId, @NonNull List<GrowthPackCampaign> growthPackCampaigns) {

        List<Integer> campaignIds = new ArrayList<>();

        for (GrowthPackCampaign growthPackCampaign : growthPackCampaigns) {
            if (growthPackCampaign != null && CollectionUtils.isNotEmpty(growthPackCampaign.getCampaignIds()))
                campaignIds.addAll(growthPackCampaign.getCampaignIds());
        }

        return RequestObj.builder().rest_id(Collections.singletonList(restaurantId)).campaign_id(campaignIds).listOfGrowthPackCampaign(growthPackCampaigns).build();
    }
}
