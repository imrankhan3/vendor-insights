package com.swiggy.vendor_insights.helpers;


import com.swiggy.vendor_insights.enums.Peaks;

public class PeakHelper {

    /**
     * peek wise time distribution
     */
    private static final Integer BREAKFAST_ST = 5;
    private static final Integer BREAKFAST_ET = 10;
    private static final Integer LUNCH_ST = 11;
    private static final Integer LUNCH_ET = 14;
    private static final Integer SNACKS_ST = 15;
    private static final Integer SNACKS_ET = 18;
    private static final Integer DINNER_ST = 19;
    private static final Integer DINNER_ET = 22;
    private static final Integer LATE_NIGHT_ST = 23; // >23 or  < 4
    private static final Integer LATE_NIGHT_ET = 4;

    public static Peaks getPeak(Integer h) {
        if(h >= BREAKFAST_ST && h <= BREAKFAST_ET){
            return Peaks.BREAKFAST;
        }
        else if(h >= LUNCH_ST && h <= LUNCH_ET){
            return Peaks.LUNCH;
        }
        else if(h >= SNACKS_ST && h <= SNACKS_ET){
            return Peaks.SNACKS;
        }
        else if(h >= DINNER_ST && h <= DINNER_ET){
            return Peaks.DINNER;
        }
        else if(h >= LATE_NIGHT_ST || h <= LATE_NIGHT_ET){
            return Peaks.LATE_NIGHT;
        }
        return null;
    }
}
