package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.orders.OrderWeeklyResponse;
import com.swiggy.vendor_insights.utils.Settings;
import com.swiggy.vendor_insights.utils.Tuple;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by sandeepjindal1 on 5/29/17.
 */
@Component
@Slf4j
public class OrderWeeklyTransformation implements TransformationService<List<InsightsBaseResponse>> {

    @Autowired
    private AggregationService aggregationService;


    public List<InsightsBaseResponse> transform(List<Template> keys, RequestObj requestObj) {

        AggregationBo orderAggregationBo = aggregationService.aggregate(keys, requestObj);

        return convertToTransformedObject(orderAggregationBo);

    }

    public static List<InsightsBaseResponse> convertToTransformedObject(AggregationBo orderAggregationBo) {

        QueryResult queryResult = null;

//        RestaurantServiceResponse restaurantServiceResponse = null;

        if (ObjectUtils.isEmpty(orderAggregationBo)) {
            return new ArrayList<>();
        }

        for (Template key : orderAggregationBo.getData().keySet()) {
            if (key.equals(Template.ORDERS)) {
                queryResult = (QueryResult) orderAggregationBo.getData().get(Template.ORDERS);
                break;
            }
        }

        if (ObjectUtils.isEmpty(queryResult)) {
            return new ArrayList<>();
        }

        List<Map<String, Object>> data = queryResult.getResult();

        List<Map<String, Object>> collect = data.stream()
                .collect(Collectors.groupingBy(k -> convertDateIntoWeek(k.get(Settings.DATE)), Collectors.toList()))
                .entrySet()
                .stream()
                .map(stringListEntry -> new Tuple<>(stringListEntry.getKey(), stringListEntry.getValue().stream().reduce((a, b) -> {
                    a.put(Settings.NO_OF_ORDERS, (Integer) a.get(Settings.NO_OF_ORDERS) + (Integer) b.get(Settings.NO_OF_ORDERS));
                    a.put(Settings.REVENUE, (Double) a.get(Settings.REVENUE) + (Double) b.get(Settings.REVENUE));
                    a.put(Settings.NO_OF_DELIVERED_ORDERS, (Integer) a.get(Settings.NO_OF_DELIVERED_ORDERS) + (Integer) b.get(Settings.NO_OF_DELIVERED_ORDERS));
                    a.put(Settings.NO_OF_CANCELLED_ORDERS, (Integer) a.get(Settings.NO_OF_CANCELLED_ORDERS) + (Integer) b.get(Settings.NO_OF_CANCELLED_ORDERS));
                    a.put(Settings.LOSSES, (Double) a.get(Settings.LOSSES) + (Double) b.get(Settings.LOSSES));
                    a.putIfAbsent(Settings.END_DATE, (String) a.get(Settings.DATE));
                    a.replace(Settings.END_DATE, (String) b.get(Settings.DATE));
                    return a;
                })))
                .filter(k -> k.getRight().isPresent())
                .map(k -> new Tuple<>(k.getLeft(), k.getRight().get()))
                .map(k -> {
                    k.getRight().put(Settings.WEEK, k.getLeft());
                    return k.getRight();
                })
                .collect(Collectors.toList());
        return buildOrderWeeklyResponse(collect);


/**
 * [{},{}] => {week1->[{},{}],week2->[{},{}]}
 *
 **/

    }

    private static List<InsightsBaseResponse> buildOrderWeeklyResponse(List<Map<String, Object>> collect) {
        List<InsightsBaseResponse> list = new ArrayList<>();

        for (Map<String, Object> map : collect) {
            OrderWeeklyResponse orderWeeklyResponse = new OrderWeeklyResponse();
            orderWeeklyResponse.setNumberOfOrders(Integer.valueOf((Integer) map.get(Settings.NO_OF_ORDERS)));
            orderWeeklyResponse.setRevenue(Double.valueOf((Double) map.get(Settings.REVENUE)));
            orderWeeklyResponse.setLosses(Double.valueOf((Double) map.get(Settings.LOSSES)));
            orderWeeklyResponse.setNumberOfCancelledOrders((Integer) map.get(Settings.NO_OF_CANCELLED_ORDERS));
            orderWeeklyResponse.setNumberOfDeliveredOrders((Integer) map.get(Settings.NO_OF_DELIVERED_ORDERS));

            Calendar calendar = Calendar.getInstance();
            calendar.clear();
            calendar.set(Calendar.WEEK_OF_YEAR, Integer.valueOf((String) map.get(Settings.WEEK)));
            calendar.set(Calendar.YEAR, 2017);
            Date date = calendar.getTime();
            date.getDate();
            orderWeeklyResponse.setStartDate((String) map.get(Settings.DATE));
            if (!(map.containsKey(Settings.END_DATE))) {
                orderWeeklyResponse.setEndDate((String) map.get(Settings.DATE));
            } else {
                orderWeeklyResponse.setEndDate((String) map.get(Settings.END_DATE));
            }
            orderWeeklyResponse.setWeek(Integer.valueOf((String) map.get(Settings.WEEK)));
            list.add(orderWeeklyResponse);
        }
        return list;
    }

    private static String convertDateIntoWeek(Object left) {
        String weekIdentifier = null;
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setFirstDayOfWeek(Calendar.MONDAY);
            calendar.setTime(new SimpleDateFormat("yyyy-MM-dd").parse((String) left));
            weekIdentifier = String.valueOf(calendar.get(Calendar.WEEK_OF_YEAR));
            return weekIdentifier;
        } catch (ParseException e) {
            log.error(e.getMessage(), e);
        }
        return weekIdentifier;
    }

}
