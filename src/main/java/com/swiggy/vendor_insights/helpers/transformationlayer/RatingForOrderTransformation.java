package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.RatingForOrderResponse;
import com.swiggy.vendor_insights.utils.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringEscapeUtils;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


@Component
public class RatingForOrderTransformation implements TransformationService<InsightsBaseResponse> {

    @Autowired
    private AggregationService aggregationService;


    public InsightsBaseResponse transform(List<Template> keys, RequestObj requestObj) {

        AggregationBo orderAggregationBo = aggregationService.aggregate(keys, requestObj);
        return convertToTransformedObject(orderAggregationBo);
    }

    public static InsightsBaseResponse convertToTransformedObject(AggregationBo opsAggregationBo) {
        QueryResult queryResult = null;

        if (ObjectUtils.isEmpty(opsAggregationBo)) {
            return null;
        }

        for (Template key : opsAggregationBo.getData().keySet()) {
            if (key.equals(Template.RATING_FOR_ORDER)) {
                queryResult = (QueryResult) opsAggregationBo.getData().get(Template.RATING_FOR_ORDER);
                break;
            }
        }

        if (ObjectUtils.isEmpty(queryResult)) {
            return null;
        }

        if(ObjectUtils.isEmpty(queryResult.getResult())){
            return null;
        }
        Map<String, Object> entry = queryResult.getResult().get(0);
        return RatingForOrderResponse.builder()
                .comments(getComments(entry))
                .rating(getRating(entry))
                .issues(getIssues(entry))
                .orderId(getOrderId(entry))
                .build();
    }

    private static String getComments(Map<String, Object> result){

        return (String) result.get(Settings.COMMENTS);
    }

    private static Integer getRating(Map<String, Object> result){

        return (Integer) result.get(Settings.RATING);
    }

    private static Long getOrderId(Map<String, Object> result){
        Object orderId = result.get(Settings.ORDER_ID);
        return ObjectUtils.isEmpty(orderId) ? null : Long.parseLong(orderId.toString());
    }

    private static List<String> getIssues(Map<String, Object> result){
        return getIssuesAsList( (String) result.get(Settings.ISSUES));
    }

    private static List<String> getIssuesAsList(String issuesStr) {
       List<String> issues;
       try {
           issuesStr = StringEscapeUtils.unescapeJava(issuesStr);
           if (!("[\"\"]").equals(issuesStr)) {
               issuesStr = issuesStr.replaceAll("\"\"", "\"");
           }
           issues = jsonDecodeAsList(issuesStr, String.class);
       } catch (IOException e) {
           issues = new ArrayList<>();
       }
       issues.removeAll(Arrays.asList("", null));
       return issues;
    }

    public static <T> T jsonDecodeAsList(String s, Class<?> clazz) throws IOException {
        if (s == null || "".equals(s.trim())) return null;

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(s, objectMapper.getTypeFactory().constructCollectionType(List.class, clazz));
    }
}
