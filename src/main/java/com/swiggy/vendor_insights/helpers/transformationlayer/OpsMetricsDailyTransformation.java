package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.OpsMetricsResponse;
import com.swiggy.vendor_insights.utils.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by sandeepjindal1 on 5/31/17.
 */
@Component
public class OpsMetricsDailyTransformation implements TransformationService<List<InsightsBaseResponse>> {

    @Autowired
    private AggregationService aggregationService;


    public List<InsightsBaseResponse> transform(List<Template> keys, RequestObj requestObj) {

        AggregationBo orderAggregationBo = aggregationService.aggregate(keys, requestObj);
        return convertToTransformedObject(orderAggregationBo);
    }

    public static List<InsightsBaseResponse> convertToTransformedObject(AggregationBo opsAggregationBo) {
        QueryResult queryResult = null;
//        RestaurantServiceResponse restaurantServiceResponse = null;

        if (ObjectUtils.isEmpty(opsAggregationBo)) {
            return new ArrayList<>();
        }

        for (Template key : opsAggregationBo.getData().keySet()) {
            if (key.equals(Template.OPS_METRIC)) {
                queryResult = (QueryResult) opsAggregationBo.getData().get(Template.OPS_METRIC);
                break;
            }
        }

        if (ObjectUtils.isEmpty(queryResult)) {
            return new ArrayList<>();
        }

        List<InsightsBaseResponse> data = new ArrayList<>();
        for (Map<String, Object> opsTuple : queryResult.getResult()) {
            data.add(OpsMetricsResponse.builder()
                    .acceptRate(getAcceptRate(opsTuple))
                    .cancelRate(getCancelRate(opsTuple))
                    .confirmationTime(getConfirmationTime(opsTuple))
                    .editRate(getEditRate(opsTuple))
                    .build());
        }
        return data;
    }

    private static Double getAcceptRate(Map<String, Object> result) {

        Integer deliveredOrders  = (Integer) result.get(Settings.NO_OF_ORDERS);

        Integer totalOrders = (Integer) result.get(Settings.TOTAL_ORDERS);

       return (double) Math.round((Double.valueOf(deliveredOrders)/Double.valueOf(totalOrders))*100*100)/100.0;

    }

    private static Double getCancelRate(Map<String, Object> result) {

        Integer cancelledOrders = (Integer) result.get(Settings.CANCELLED_ORDERS);

        Integer totalOrders = (Integer) result.get(Settings.TOTAL_ORDERS);

        return (double) Math.round((Double.valueOf(cancelledOrders)/Double.valueOf(totalOrders))*100*100)/100.0;
    }

    private static Double getConfirmationTime(Map<String, Object> result) {

        if(result.get(Settings.AVG_CONFIRMATION_TIME) == null){
            return (double) 0;
        }else {
            return ((double) Math.round((((Double) result.get(Settings.AVG_CONFIRMATION_TIME)) * 100) / 60)) / 100;
        }
    }

    private static Double getEditRate(Map<String, Object> result) {

        Integer editedOrders  = (Integer) result.get(Settings.EDITED_ORDERS);

        Integer totalOrders = (Integer) result.get(Settings.TOTAL_ORDERS);

        return (double) Math.round((Double.valueOf(editedOrders)/Double.valueOf(totalOrders))*100*100)/100.0;

    }

}
