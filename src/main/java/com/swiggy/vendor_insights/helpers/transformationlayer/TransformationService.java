package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by shahbaz.khalid on 5/24/17.
 */

@Service
public interface TransformationService<T> {

    T transform(List<Template> keys, RequestObj req);

}
