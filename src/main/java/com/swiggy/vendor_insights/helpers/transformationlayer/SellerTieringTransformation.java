package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.SellerTierHelper;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.RestaurantServiceResponse;
import com.swiggy.vendor_insights.pojos.bo.SellerTieringResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.requestobj.SellerTieringRequest;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.SellerTieringResponse;
import com.swiggy.vendor_insights.services.SellerTieringService.FetchSellerTierData;
import com.swiggy.vendor_insights.utils.Settings;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.swiggy.vendor_insights.utils.Settings.MODEL_BASED_RATING;

/**
 * Created by sandeepjindal1 on 5/23/17.
 */
@Component
public class SellerTieringTransformation implements TransformationService<InsightsBaseResponse> {

    @Autowired
    private AggregationService aggregationService;

    @Autowired
    private FetchSellerTierData fetchSellerTierData;

    @Autowired
    private SellerTierHelper sellerTierHelper;


    public InsightsBaseResponse transform(List<Template> keys, RequestObj requestObj) {

        AggregationBo orderAggregationBo = aggregationService.aggregate(keys, requestObj);
        return convertToTransformedObject(orderAggregationBo, requestObj);
    }

    public InsightsBaseResponse convertToTransformedObject(AggregationBo sellerTierAggregationBo, RequestObj requestObj) {
        QueryResult queryResult = null;
        RestaurantServiceResponse restaurantServiceResponse = null;
        SellerTieringResult sellerTieringResult = null;

        if (ObjectUtils.isEmpty(sellerTierAggregationBo)) {
            return null;
        }

        for (Template key : sellerTierAggregationBo.getData().keySet()) {
            if (key.equals(Template.SELLER_TIER_STATS)) {
                queryResult = (QueryResult) sellerTierAggregationBo.getData().get(Template.SELLER_TIER_STATS);
            }
            if (key.equals(Template.SRS)) {
                restaurantServiceResponse = (RestaurantServiceResponse) sellerTierAggregationBo.getData().get(Template.SRS);
            }
        }

        if (ObjectUtils.isEmpty(queryResult) || restaurantServiceResponse == null) {
            return null;
        }

        List<Map<String,Object>> srsResult = (List<Map<String, Object>>) restaurantServiceResponse.getData();

        if (ObjectUtils.isEmpty(queryResult.getResult())){
            return null;
        }
        Map<String, Object> result = queryResult.getResult().get(0);

        SellerTieringRequest sellerTieringRequest = SellerTieringRequest.builder()
                                                        .cancellationRate(getCancelRate(result))
                                                        .editRate(getEditRate(result))
                                                        .acceptanceRate(getAcceptRate(result))
                                                        .rating(getModelBasedRating(srsResult))
                                                        .totalSales(getRevenue(result))
                                                        .restId(requestObj.getRest_id().get(0))
                                                        .build();
        sellerTieringResult = fetchSellerTierData.getSellerTierData(sellerTieringRequest);

        Pair<String, Boolean> pair = sellerTierHelper.getMessage((Map<String, Object>) sellerTieringResult.getData());
        String message = pair.getKey();
        Boolean isMessagePositive = pair.getValue();

        return SellerTieringResponse.builder()
                        .cancellationRate(getCancelRate(result))
                        .editRate(getEditRate(result))
                        .acceptenceRate(getAcceptRate(result))
                        .rating(getModelBasedRating(srsResult))
                        .revenue(getRevenue(result))
                        .message(message)
                        .isMessagePositive(isMessagePositive)
                        .sellerTieringData((Map<String, Object>) sellerTieringResult.getData())
                        .build();
    }

    private static Double getModelBasedRating(List<Map<String, Object>> srsResult) {
        if(CollectionUtils.isEmpty(srsResult)){
            return null;
        } else {
            return (Double) srsResult.get(0).get(MODEL_BASED_RATING);
        }
    }

    private static Double getAcceptRate(Map<String, Object> result) {

        Integer deliveredOrders  = (Integer) result.get(Settings.NO_OF_ORDERS);

        Integer totalOrders = (Integer) result.get(Settings.TOTAL_ORDERS);

        return (double) Math.round((Double.valueOf(deliveredOrders)/Double.valueOf(totalOrders))*100*100)/100.0;

    }

    private static Double getCancelRate(Map<String, Object> result) {

        Integer cancelledOrders = (Integer) result.get(Settings.CANCELLED_ORDERS);

        Integer totalOrders = (Integer) result.get(Settings.TOTAL_ORDERS);

        return (double) Math.round((Double.valueOf(cancelledOrders)/Double.valueOf(totalOrders))*100*100)/100.0;
    }

    private static Double getEditRate(Map<String, Object> result) {

        Integer editedOrders  = (Integer) result.get(Settings.EDITED_ORDERS);

        Integer totalOrders = (Integer) result.get(Settings.TOTAL_ORDERS);

        return (double) Math.round((Double.valueOf(editedOrders)/Double.valueOf(totalOrders))*100*100)/100.0;

    }

    public static Double getRevenue(Map<String, Object> result){
        return (Double) result.get(Settings.REVENUE);
    }
}
