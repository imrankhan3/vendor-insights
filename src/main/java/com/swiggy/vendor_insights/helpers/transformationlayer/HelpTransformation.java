package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.HelpContextHelper;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.HelpContextResponse;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.orders.OrderResponse;
import com.swiggy.vendor_insights.utils.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sandeepjindal1 on 5/23/17.
 */
@Component
public class HelpTransformation implements TransformationService<InsightsBaseResponse> {

    @Autowired
    private HelpContextHelper helpContextHelper;


    public InsightsBaseResponse transform(List<Template> keys, RequestObj requestObj) {
        String key = requestObj.getKey();
        String context = requestObj.getContext();


        String messageTemplate = helpContextHelper.getMessage(context, key);

        if (messageTemplate == null){
            return new HelpContextResponse();
        }

        Map<String, Object> params = new HashMap<>();

        for (String paramsKey: params.keySet()) {
            messageTemplate.replace(paramsKey, params.get(paramsKey).toString());
        }
        return HelpContextResponse.builder().message(messageTemplate).build();

    }
}
