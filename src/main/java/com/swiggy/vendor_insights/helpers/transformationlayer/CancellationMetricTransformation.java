package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.CancelledOrdersResponse;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.utils.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by sandeepjindal1 on 5/23/17.
 */
@Component
public class CancellationMetricTransformation implements TransformationService<List<InsightsBaseResponse>> {

    @Autowired
    private AggregationService aggregationService;


    public List<InsightsBaseResponse> transform(List<Template> keys, RequestObj requestObj) {

        AggregationBo orderAggregationBo = aggregationService.aggregate(keys, requestObj);
        return convertToTransformedObject(orderAggregationBo);
    }

    public static List<InsightsBaseResponse> convertToTransformedObject(AggregationBo orderAggregationBo) {
        QueryResult queryResult = null;
//        RestaurantServiceResponse restaurantServiceResponse = null;

        if (ObjectUtils.isEmpty(orderAggregationBo)) {
            return new ArrayList<>();
        }

        for (Template key : orderAggregationBo.getData().keySet()) {
            if (key.equals(Template.CANCELLATION)) {
                queryResult = (QueryResult) orderAggregationBo.getData().get(Template.CANCELLATION);
                break;
            }
        }

        if (ObjectUtils.isEmpty(queryResult)) {
            return new ArrayList<>();
        }

        List<CancelledOrdersResponse> data = new ArrayList<>();
        Double cancellationPercent = getCancellationPercent(queryResult.getResult());
        Double totalLosses = getTotalLosses(queryResult.getResult());
        for (Map<String, Object> cancellationTuple : queryResult.getResult()) {
            data.add(CancelledOrdersResponse.builder()
                    .orderCount(getOrderCount(cancellationTuple))
                    .initiatedBy(getInitiatedBy(cancellationTuple))
                    .losses(getLosses(cancellationTuple))
                    .totalLosses(totalLosses)
                    .cancellationPercent(cancellationPercent)
                    .reasonId(getReasonId(cancellationTuple))
                    .build());
        }
        return addReason(data);
    }

    private static List<InsightsBaseResponse> addReason(List<CancelledOrdersResponse> data){
        List<InsightsBaseResponse> baseResponseList = data.stream().collect(Collectors.groupingBy(k -> Settings.CANCELLATION_DISPOSITION_MAP.get(k.getReasonId())))
                .entrySet().stream()
                .map(cancellationCollectionEntry -> cancellationCollectionEntry.getValue().stream().map(cancellationEntry -> {
                    cancellationEntry.setReason(cancellationCollectionEntry.getKey());
                    return cancellationEntry;
                }).collect(Collectors.toList()))
                .map(cancellationListEntry -> cancellationListEntry
                        .stream().reduce((a, b) -> {
                                return CancelledOrdersResponse.builder()
                                        .cancellationPercent(a.getCancellationPercent())
                                        .reason(a.getReason())
                                        .orderCount(a.getOrderCount() + b.getOrderCount())
                                        .losses(a.getLosses() + b.getLosses())
                                        .totalLosses(a.getTotalLosses())
                                        .build();
                        })).map(k -> k.get()).collect(Collectors.toList());
        return baseResponseList;
    }

    private static Integer getOrderCount(Map<String, Object> result) {

        return (Integer) result.get(Settings.ORDER_COUNT);
    }

    private static Integer getReasonId(Map<String, Object> result) {

        return (Integer) result.get(Settings.REASON_ID);
    }

    private static String getInitiatedBy(Map<String, Object> result) {

        return (String) result.get(Settings.INITIATED_BY);
    }

    private static Double getLosses(Map<String, Object> result){

        return (Double) result.get(Settings.LOSSES);
    }

    private static Double getTotalLosses(List<Map<String, Object>> result){

        return result.stream().mapToDouble((r) -> (Double) r.get(Settings.LOSSES)).sum();
    }

    private static Integer getTotalCancelledOrders(List<Map<String, Object>> result){

        return result.stream().mapToInt((r) -> (Integer) r.get(Settings.ORDER_COUNT)).sum();
    }

    private static Double getCancellationPercent(List<Map<String, Object>> results){

        if (results.isEmpty()) {
            return (double) 0;
        }else{

            Integer totalCancelledOrders = getTotalCancelledOrders(results);

            Integer totalOrders = (Integer) results.get(0).get(Settings.TOTAL_ORDERS);

            return (double) Math.round((Double.valueOf(totalCancelledOrders) / Double.valueOf(totalOrders)) * 100 * 100) / 100.0;
        }
    }

}
