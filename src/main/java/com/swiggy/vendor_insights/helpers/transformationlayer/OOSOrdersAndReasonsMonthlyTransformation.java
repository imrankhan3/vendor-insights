package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.OOSOrdersAndReasonsResponse;
import com.swiggy.vendor_insights.utils.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by sandeepjindal1 on 5/31/17.
 */
@Component
public class OOSOrdersAndReasonsMonthlyTransformation implements TransformationService<List<InsightsBaseResponse>> {

    @Autowired
    private AggregationService aggregationService;


    public List<InsightsBaseResponse> transform(List<Template> keys, RequestObj requestObj) {

        AggregationBo orderAggregationBo = aggregationService.aggregate(keys, requestObj);
        return convertToTransformedObject(orderAggregationBo);
    }

    public static List<InsightsBaseResponse> convertToTransformedObject(AggregationBo oosAggregationBo) {
        QueryResult queryResult = null;
//        RestaurantServiceResponse restaurantServiceResponse = null;

        if (ObjectUtils.isEmpty(oosAggregationBo)) {
            return new ArrayList<>();
        }

        for (Template key : oosAggregationBo.getData().keySet()) {
            if (key.equals(Template.ORDERS_OOS)) {
                queryResult = (QueryResult) oosAggregationBo.getData().get(Template.ORDERS_OOS);
                break;
            }
        }

        if (ObjectUtils.isEmpty(queryResult)) {
            return new ArrayList<>();
        }

        List<InsightsBaseResponse> data = new ArrayList<>();
        for (Map<String, Object> oosTuple : queryResult.getResult()) {
            data.add(OOSOrdersAndReasonsResponse.builder()
                    .alterNotAcceptedCancelled(getAlterNotAcceptedCancelled(oosTuple))
                    .alterProvidedDelivered(getAlterProvidedDelivered(oosTuple))
                    .deliveredOrders(getDeliveredOrders(oosTuple))
                    .totalOrders(getTotalOrders(oosTuple))
                    .build());
        }
        return data;
    }

    private static Integer getAlterProvidedDelivered(Map<String, Object> result) {

        return (Integer) result.get(Settings.ALTER_PROVIDED_DELIVERED);
    }

    private static Integer getAlterNotAcceptedCancelled(Map<String, Object> result) {

        return (Integer) result.get(Settings.ALTER_NOT_ACCEPTED_CANCELLED);
    }


    private static Integer getDeliveredOrders(Map<String, Object> result) {

        return (Integer) result.get(Settings.DELIVERED_ORDERS);
    }

    private static Integer getTotalOrders(Map<String, Object> result) {

        return (Integer) result.get(Settings.TOTAL_ORDERS);
    }

}
