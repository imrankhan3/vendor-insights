package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.RestaurantMetricsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by shahbaz.khalid on 5/24/17.
 */

@Service
public class MetricTransformationService implements TransformationService<List<InsightsBaseResponse>> {

    @Autowired
    private AggregationService aggregationService;

    public List<InsightsBaseResponse> transform (List<Template> keys, RequestObj req) {
        AggregationBo response;

        QueryResult queryResult = null;
        response = aggregationService.aggregate(keys, req);
        for (Template key : response.getData().keySet()) {
            if (key.equals(Template.METRICS)) {
                queryResult = (QueryResult) response.getData().get(Template.METRICS);
            }
        }
        if (ObjectUtils.isEmpty(response)) {
            return new ArrayList<>();
        }

        List<InsightsBaseResponse> data = new ArrayList<>();
        for(Map<String, Object> orderTuple: queryResult.getResult()){
            data.add((InsightsBaseResponse) RestaurantMetricsResponse.builder()
                    .acceptanceRate(getAcceptanceRate(orderTuple))
                    .oosRate(getOosRate(orderTuple))
                    .rating(getRating(orderTuple))
                    .total(getTotal(orderTuple))
                    .build());
        }
        return data;
    }

    private Double getAcceptanceRate(Map<String, Object> result) {
        return (Double) result.get("acceptance_rate");
    }

    private Double getOosRate(Map<String, Object> result) {
        return (Double) result.get("oos_rate");
    }

    private static Double getRating(Map<String, Object> result) {
        return (Double) result.get("rating");
    }

    private static Double getTotal(Map<String, Object> result) {
        return (Double) result.get("total");
    }


}
