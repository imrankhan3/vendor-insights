package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.LastUpdatedTimeResponse;
import com.swiggy.vendor_insights.pojos.responseobj.OpsMetricsResponse;
import com.swiggy.vendor_insights.utils.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Component
public class LastUpdatedTimeTransformation implements TransformationService<InsightsBaseResponse> {

    @Autowired
    private AggregationService aggregationService;


    public InsightsBaseResponse transform(List<Template> keys, RequestObj requestObj) {

        AggregationBo orderAggregationBo = aggregationService.aggregate(keys, requestObj);
        return convertToTransformedObject(orderAggregationBo);
    }

    public static InsightsBaseResponse convertToTransformedObject(AggregationBo opsAggregationBo) {
        QueryResult queryResult = null;

        if (ObjectUtils.isEmpty(opsAggregationBo)) {
            return null;
        }

        for (Template key : opsAggregationBo.getData().keySet()) {
            if (key.equals(Template.LAST_UPDATED_TIME)) {
                queryResult = (QueryResult) opsAggregationBo.getData().get(Template.LAST_UPDATED_TIME);
                break;
            }
        }

        if (ObjectUtils.isEmpty(queryResult)) {
            return null;
        }

        return LastUpdatedTimeResponse.builder()
                .lastUpdatedDate(getLastUpdatedTime(queryResult.getResult().get(0)))
                .build();
    }

    private static String getLastUpdatedTime(Map<String, Object> result){

        return (String) result.get(Settings.LAST_UPDATED_TIME);
    }
}
