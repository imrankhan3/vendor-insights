package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.OpsMetricsResponse;
import com.swiggy.vendor_insights.pojos.responseobj.TradeDiscountMetricsResponse;
import com.swiggy.vendor_insights.utils.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class TradeDiscountMetricsTransformation implements TransformationService<List<InsightsBaseResponse>> {

    @Autowired
    private AggregationService aggregationService;

    @Override
    public List<InsightsBaseResponse> transform(List<Template> keys, RequestObj requestObj) {
        AggregationBo orderAggregationBo = aggregationService.aggregate(keys, requestObj);
        return convertToTransformedObject(orderAggregationBo);
    }

    private List<InsightsBaseResponse> convertToTransformedObject(AggregationBo tradeDiscountAggregationBo) {
        QueryResult queryResult = null;

        if (ObjectUtils.isEmpty(tradeDiscountAggregationBo)) {
            return new ArrayList<>();
        }

        for (Template key : tradeDiscountAggregationBo.getData().keySet()) {
            if (key.equals(Template.TD_METRICS)) {
                queryResult = (QueryResult) tradeDiscountAggregationBo.getData().get(Template.TD_METRICS);
                break;
            }
        }

        if (ObjectUtils.isEmpty(queryResult)) {
            return new ArrayList<>();
        }
        List<InsightsBaseResponse> result = new ArrayList<>();
        for (Map<String, Object> opsTuple : queryResult.getResult()) {
            result.add(TradeDiscountMetricsResponse.builder()
                .totalOrders(getTotalOrders(opsTuple))
                .discountedOrders(getDiscountedOrders(opsTuple))
                .newCustomers(getNewCustomers(opsTuple))
                .restaurantId(getRestaurantId(opsTuple))
                .revenue(getRevenue(opsTuple))
                .spend(getSpend(opsTuple))
                .build());
        }
        return result;
    }

    private Integer getTotalOrders(Map<String, Object> result) {
        return (Integer) result.get(Settings.TOTAL_ORDERS);
    }

    private Integer getDiscountedOrders(Map<String, Object> result) {
        return (Integer) result.get(Settings.DISCOUNTED_ORDERS);
    }

    private Integer getNewCustomers(Map<String, Object> result) {
        return (Integer) result.get(Settings.NEW_CUSTOMERS);
    }

    private Integer getRestaurantId(Map<String, Object> result) {
        return (Integer) result.get(Settings.REST_ID);
    }

    private Double getRevenue(Map<String, Object> result) {
        return (Double) result.get(Settings.REVENUE);
    }

    private Double getSpend(Map<String, Object> result) {
        return (Double) result.get(Settings.SPEND);
    }


}
