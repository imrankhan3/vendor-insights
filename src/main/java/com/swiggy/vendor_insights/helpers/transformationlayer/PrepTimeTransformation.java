package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.Peaks;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.PeakHelper;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.AveragePrepTimeDistribution;
import com.swiggy.vendor_insights.pojos.responseobj.AveragePrepTimePeakResponse;
import com.swiggy.vendor_insights.pojos.responseobj.AveragePrepTimeResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.swiggy.vendor_insights.utils.Settings.AVERAGE_PREP_TIME;
import static com.swiggy.vendor_insights.utils.Settings.HOUR;
import static com.swiggy.vendor_insights.utils.Settings.ORDER_COUNT;

@Log4j2
@Component
public class PrepTimeTransformation {

    @Autowired
    private AggregationService aggregationService;

    public AveragePrepTimeResponse transform(List<Template> keys, RequestObj requestObj) {
        AggregationBo orderAggregationBo = aggregationService.aggregate(keys, requestObj);
        return convertToTransformedObject(orderAggregationBo);
    }

    private static AveragePrepTimeResponse convertToTransformedObject(AggregationBo orderAggregationBo) {

        Optional<AggregationBo> orderAggregationBoOptional = Optional.ofNullable(orderAggregationBo);
        Optional<List<Map<String, Object>>> hourlyPrepTimeQueryResultDataMapList = getQueryMap(orderAggregationBoOptional, Template.PREP_TIME_HOURLY);
        Optional<List<Map<String, Object>>> monthlyPrepTimeQueryResultDataMapList = getQueryMap(orderAggregationBoOptional, Template.AVERAGE_PREP_TIME);

        Map<Peaks, List<AveragePrepTimeDistribution>> peaksAveragePrepTimeDistributionMap = new HashMap<>();
        Map <Peaks, Double> peakPrepTimeMap = new HashMap<>();
        Map <Peaks, Integer> peakOrdersMap = new HashMap<>();

        List<AveragePrepTimePeakResponse> averagePrepTimePeakResponseList = new ArrayList<>();
        for (Map<String, Object> hourlyPrepTimeQueryResultDataMap : hourlyPrepTimeQueryResultDataMapList.get()) {
            Integer hour = (Integer) hourlyPrepTimeQueryResultDataMap.get(HOUR);
            Double averagePrepTime = hourlyPrepTimeQueryResultDataMap.get(AVERAGE_PREP_TIME) == null
                    ? 0 : (Double) hourlyPrepTimeQueryResultDataMap.get(AVERAGE_PREP_TIME);
            Integer orderCount = hourlyPrepTimeQueryResultDataMap.get(ORDER_COUNT) == null
                    ? 0 : (Integer) hourlyPrepTimeQueryResultDataMap.get(ORDER_COUNT);

            Peaks peak = PeakHelper.getPeak(hour);

            AveragePrepTimeDistribution averagePrepTimeDistribution = AveragePrepTimeDistribution
                    .builder()
                    .averagePrepTime(averagePrepTime)
                    .hour(hour)
                    .build();
            List<AveragePrepTimeDistribution> averagePrepTimeDistributions = new ArrayList<>();
            if (peaksAveragePrepTimeDistributionMap.get(peak) != null) {
                averagePrepTimeDistributions = peaksAveragePrepTimeDistributionMap.get(peak);
            }
            averagePrepTimeDistributions.add(averagePrepTimeDistribution);
            peaksAveragePrepTimeDistributionMap.put(peak, averagePrepTimeDistributions);
            peakPrepTimeMap.merge(peak, averagePrepTime * orderCount,(x, y) -> x + y);
            peakOrdersMap.merge(peak, orderCount, (x,y) -> x + y);
        }

        for (Peaks peak : Peaks.values()) {
            AveragePrepTimePeakResponse averagePrepTimePeakResponse = AveragePrepTimePeakResponse
                        .builder()
                        .averagePrepTime((peakOrdersMap.get(peak) == null || peakOrdersMap.get(peak) == 0)
                                ? 0.0 : peakPrepTimeMap.get(peak) / peakOrdersMap.get(peak))
                        .totalOrders(peakOrdersMap.get(peak) == null ? 0 : peakOrdersMap.get(peak))
                        .peak(peak.name())
                        .averagePrepTimeDistributions(peaksAveragePrepTimeDistributionMap.get(peak))
                        .build();
            averagePrepTimePeakResponseList.add(averagePrepTimePeakResponse);
        }

        return AveragePrepTimeResponse.builder()
                .averagePrepTime(monthlyPrepTimeQueryResultDataMapList.get().get(0).get(AVERAGE_PREP_TIME) == null
                        ? 0 : (Double) monthlyPrepTimeQueryResultDataMapList.get().get(0).get(AVERAGE_PREP_TIME))
                .totalOrders(monthlyPrepTimeQueryResultDataMapList.get().get(0).get(ORDER_COUNT) == null
                        ? 0 : (Integer) monthlyPrepTimeQueryResultDataMapList.get().get(0).get(ORDER_COUNT))
                .averagePrepTimePeakResponse(averagePrepTimePeakResponseList)
                .build();
    }

    private static Optional<List<Map<String, Object>>> getQueryMap(Optional<AggregationBo> orderAggregationBoOptional, Enum template) {
         return  orderAggregationBoOptional
                .flatMap(orderAggregation -> Optional.ofNullable(orderAggregation.getData()))
                .flatMap(orderAggregationBoMap -> Optional.ofNullable((QueryResult) orderAggregationBoMap.get(template)))
                .flatMap(queryResult -> Optional.ofNullable(queryResult.getResult()));

    }
}
