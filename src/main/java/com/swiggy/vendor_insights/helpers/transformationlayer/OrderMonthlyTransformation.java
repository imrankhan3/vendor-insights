package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.orders.OrderMonthlyResponse;
import com.swiggy.vendor_insights.utils.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by sandeepjindal1 on 5/31/17.
 */
@Component
public class OrderMonthlyTransformation implements TransformationService<List<InsightsBaseResponse>> {

    @Autowired
    private AggregationService aggregationService;


    public List<InsightsBaseResponse> transform(List<Template> keys, RequestObj requestObj) {

        AggregationBo orderAggregationBo = aggregationService.aggregate(keys, requestObj);
        return convertToTransformedObject(orderAggregationBo);
    }

    public List<InsightsBaseResponse> convertToTransformedObject(AggregationBo orderAggregationBo) {
        QueryResult queryResult = null;
//        RestaurantServiceResponse restaurantServiceResponse = null;

        if (ObjectUtils.isEmpty(orderAggregationBo)) {
            return new ArrayList<>();
        }

        for (Template key : orderAggregationBo.getData().keySet()) {
            if (key.equals(Template.ORDERS_MONTHLY)) {
                queryResult = (QueryResult) orderAggregationBo.getData().get(Template.ORDERS_MONTHLY);
                break;
            }
        }

        if (ObjectUtils.isEmpty(queryResult)) {
            return new ArrayList<>();
        }

        List<InsightsBaseResponse> data = new ArrayList<>();
        for (Map<String, Object> orderTuple : queryResult.getResult()) {
            data.add(OrderMonthlyResponse.builder()
                    .month(getMonth(orderTuple))
                    .year(getYear(orderTuple))
                    .revenue(getRevenue(orderTuple))
                    .numberOfOrders(getNumberOfOrders(orderTuple))
                    .numberOfCancelledOrders(getCancelledOrders(orderTuple))
                    .numberOfDeliveredOrders(getDeliveredOrders(orderTuple))
                    .losses(getLosses(orderTuple))
                    .build());
        }
        return data;
    }

    private static Integer getNumberOfOrders(Map<String, Object> result) {

        return (Integer) result.get(Settings.NO_OF_ORDERS);
    }

    private static Double getRevenue(Map<String, Object> result) {

        return (Double) result.get(Settings.REVENUE);
    }

    private static Integer getMonth(Map<String, Object> result) {

        return (Integer) result.get(Settings.MONTH);
    }

    private static Integer getYear(Map<String, Object> result) {

        return (Integer) result.get(Settings.YEAR);
    }

    private static Integer getCancelledOrders(Map<String, Object> result) {

        return (Integer) result.get(Settings.NO_OF_CANCELLED_ORDERS);
    }

    private static Integer getDeliveredOrders(Map<String, Object> result) {

        return (Integer) result.get(Settings.NO_OF_DELIVERED_ORDERS);
    }

    private static Double getLosses(Map<String, Object> result) {

        return (Double) result.get(Settings.LOSSES);
    }

}
