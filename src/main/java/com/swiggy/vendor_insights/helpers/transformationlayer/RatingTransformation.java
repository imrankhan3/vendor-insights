package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.RatingStar;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.RestaurantServiceResponse;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.rating.RatingDistributionResponse;
import com.swiggy.vendor_insights.pojos.responseobj.rating.RatingReasonDistributionResponse;
import com.swiggy.vendor_insights.pojos.responseobj.rating.RatingsResponse;
import com.swiggy.vendor_insights.utils.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.swiggy.vendor_insights.utils.Settings.MODEL_BASED_RATING;
import static com.swiggy.vendor_insights.utils.Settings.RATED_ORDERS;


/**
 * Created by sandeepjindal1 on 6/05/17.
 */
@Component
public class RatingTransformation implements TransformationService<InsightsBaseResponse> {

    @Autowired
    private AggregationService aggregationService;

    @Autowired
    private RedisTemplate redisTemplate;

    public InsightsBaseResponse transform(List<Template> keys, RequestObj requestObj) {

        AggregationBo ratingsAggregationBo = aggregationService.aggregate(keys, requestObj);
        return convertToTransformedObject(ratingsAggregationBo);
    }

    public InsightsBaseResponse convertToTransformedObject(AggregationBo ratingsAggregationBo) {
        QueryResult queryResult = null;

        RestaurantServiceResponse restaurantServiceResponse = null;

        if (ObjectUtils.isEmpty(ratingsAggregationBo)) {
            return null;
        }

        for (Template key : ratingsAggregationBo.getData().keySet()) {
            if (key.equals(Template.RATINGS)) {
                queryResult = (QueryResult) ratingsAggregationBo.getData().get(Template.RATINGS);
            }
            if (key.equals(Template.SRS)) {
                restaurantServiceResponse = (RestaurantServiceResponse) ratingsAggregationBo.getData().get(Template.SRS);
            }
        }

        if (ObjectUtils.isEmpty(queryResult)) {
            return null;
        }

        InsightsBaseResponse data = new InsightsBaseResponse();
        List<Map<String, Object>> result = queryResult.getResult();
        List<Map<String,Object>> srsResult = null;
        if(restaurantServiceResponse != null){
            srsResult = (List<Map<String, Object>>) restaurantServiceResponse.getData();
        }
        data = (RatingsResponse.builder()
                .ratedOrders(getRatedOrders(result))
                .reasons(getReasons(result))
                .ratings(getRatings(result))
                .modelBasedRating(getModelBasedRating(srsResult))
                .build());
        return data;
    }

    private Double getModelBasedRating(List<Map<String, Object>> srsResult) {
        if(CollectionUtils.isEmpty(srsResult)){
            return null;
        } else {
            return (Double) srsResult.get(0).get(MODEL_BASED_RATING);
        }
    }

    private Integer getRatedOrders(List<Map<String, Object>> result) {
        Integer ratedOrderCount = 0;
        for (Map<String, Object> map : result) {
            ratedOrderCount += (Integer) map.get(RATED_ORDERS);
        }
        return ratedOrderCount;
    }

    private List<RatingReasonDistributionResponse> getReasons(List<Map<String, Object>> result) {

        Map<String, Integer> reasonCountMap = new HashMap<>();
        Map<String, String> cachedReasonMap = (Map<String, String>) redisTemplate.opsForHash().entries(Settings.RATING_REASONS);

        Integer globalCount = 0;

        for (Map<String, Object> resultMap : result) {
            for (String key : resultMap.keySet()) {
                if (cachedReasonMap.containsKey(key)) {
                    Integer value = (Integer) resultMap.get(key);
                    String reason = cachedReasonMap.get(key);
                    if (reasonCountMap.containsKey(reason)) {

                        reasonCountMap.put(reason, reasonCountMap.get(reason) + value);
                        globalCount = globalCount + value;
                    } else {
                        reasonCountMap.put(reason, value);
                        globalCount = globalCount + value;
                    }
                }
            }
        }
        if (CollectionUtils.isEmpty(reasonCountMap)) {
            return new ArrayList<>();
        }
        return prepareRatingReasonDistributionResponse(reasonCountMap, globalCount);

    }

    private List<RatingReasonDistributionResponse> prepareRatingReasonDistributionResponse(Map<String, Integer> reasonCountMap, Integer globalCount) {
        List<RatingReasonDistributionResponse> list = new ArrayList<>();
        for (Map.Entry<String,Integer> entry : reasonCountMap.entrySet()) {
            RatingReasonDistributionResponse ratingReasonDistributionResponse = new RatingReasonDistributionResponse();
            ratingReasonDistributionResponse.setReason(entry.getKey());
            ratingReasonDistributionResponse.setCount(entry.getValue());
            ratingReasonDistributionResponse.setShare((double) Math.round((Double.valueOf(entry.getValue())/Double.valueOf(globalCount))*100*100)/100.0);
            list.add(ratingReasonDistributionResponse);
        }
        return list;
    }

    private static List<RatingDistributionResponse> getRatings(List<Map<String, Object>> result) {

        Map<RatingStar,Integer> ratingStarMap = new HashMap<>();

        for (Map<String, Object> resultMap : result) {
            for (RatingStar ratingStar : RatingStar.values()) {
                String key = RatingStar.getValue(ratingStar);
                if (resultMap.containsKey(key)) {
                    Integer value = (Integer) resultMap.get(key);
                    RatingStar ratingStarKey = RatingStar.getRatingStar(key);
                    if (ratingStarMap.containsKey(ratingStarKey)) {
                        ratingStarMap.put(ratingStarKey, ratingStarMap.get(ratingStarKey) + value);
                    } else {
                        ratingStarMap.put(ratingStarKey, value);
                    }
                }
            }
        }
        return prepareRatingDistributionResponse(ratingStarMap);
    }

    private static List<RatingDistributionResponse> prepareRatingDistributionResponse(Map<RatingStar, Integer> ratingStarMap) {
        List<RatingDistributionResponse> list = new ArrayList<>();
        for (Map.Entry<RatingStar, Integer> entry : ratingStarMap.entrySet()) {
            RatingDistributionResponse ratingDistributionResponse = new RatingDistributionResponse();
            ratingDistributionResponse.setRating(RatingStar.getValue(entry.getKey()));
            ratingDistributionResponse.setCount(entry.getValue());
            list.add(ratingDistributionResponse);
        }
        return list;
    }

}
