package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

import static com.swiggy.vendor_insights.constants.MfrConstants.*;

@Component
public class MFRMetricsTransformation {
    @Autowired
    private AggregationService aggregationService;

    public Map<String, Object> transform(List<Template> keys, RequestObj requestObj) {

        AggregationBo orderAggregationBo = aggregationService.aggregate(keys, requestObj);
        return convertToTransformedObject(orderAggregationBo);
    }

    private static Map<String, Object> convertToTransformedObject(AggregationBo orderAggregationBo) {
        Optional<AggregationBo> orderAggregationBoOptional = Optional.ofNullable(orderAggregationBo);

        Optional<List<Map<String, Object>>> queryResultDataMap = orderAggregationBoOptional
                .flatMap(orderAggregation -> Optional.ofNullable(orderAggregation.getData()))
                .flatMap(orderAggregationBoMap -> Optional.ofNullable((QueryResult) orderAggregationBoMap.get(Template.MFR_METRICS_KEY)))
                .flatMap(queryResult -> Optional.ofNullable(queryResult.getResult()));

        Map<String, Object> result = new HashMap<String, Object>();

        if(queryResultDataMap.isPresent() && queryResultDataMap.get().size() > 0 ){
            result.put(MFR_PRESSED_CORRECTLY, (Integer)queryResultDataMap.get().get(0).get(MFR_PRESSED_CORRECTLY));
            result.put(MFR_PRESSED_EARLY, (Integer)queryResultDataMap.get().get(0).get(MFR_PRESSED_EARLY));
            result.put(MFR_NOT_PRESSED, (Integer)queryResultDataMap.get().get(0).get(MFR_NOT_PRESSED));
        }
        return result;
    }
}
