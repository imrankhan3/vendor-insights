package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ArrayList;

import static com.swiggy.vendor_insights.constants.MfrConstants.MFR_REPORT_KEY;
import static com.swiggy.vendor_insights.constants.MfrConstants.ORDER_PREP_TIME;
import static com.swiggy.vendor_insights.constants.MfrConstants.MFR_REPORT_VALUE;


@Component
public class MFRStatusTransformation {

    @Autowired
    private AggregationService aggregationService;


    public Map<Long, List<Object>> transform(List<Template> keys, RequestObj requestObj) {

        AggregationBo orderAggregationBo = aggregationService.aggregate(keys, requestObj);
        return convertToTransformedObject(orderAggregationBo);
    }

    private static Map<Long, List<Object>> convertToTransformedObject(AggregationBo orderAggregationBo) {
        Optional<AggregationBo> orderAggregationBoOptional = Optional.ofNullable(orderAggregationBo);

        Optional<List<Map<String, Object>>> queryResultDataMap = orderAggregationBoOptional
                .flatMap(orderAggregation -> Optional.ofNullable(orderAggregation.getData()))
                .flatMap(orderAggregationBoMap -> Optional.ofNullable((QueryResult) orderAggregationBoMap.get(Template.MFR_STATUS_KEY)))
                .flatMap(queryResult -> Optional.ofNullable(queryResult.getResult()));

        Map<Long, List<Object>> result = new HashMap<>();

        queryResultDataMap.get().forEach(map -> {
            Long key = (Long) map.get(MFR_REPORT_KEY);
            List values = new ArrayList();
            values.add(map.get(MFR_REPORT_VALUE));
            values.add(map.get(ORDER_PREP_TIME));
            result.put(key, values);
        });

        return result;
    }

}
