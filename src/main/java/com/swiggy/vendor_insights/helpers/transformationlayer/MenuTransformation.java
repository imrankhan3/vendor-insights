package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.ResponseType;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.ItemResponse;
import com.swiggy.vendor_insights.pojos.responseobj.MenuResponse;
import com.swiggy.vendor_insights.pojos.responseobj.OrderMetricResponse;
import com.swiggy.vendor_insights.utils.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by sandeepjindal1 on 5/23/17.
 */
@Component
public class MenuTransformation implements TransformationService<List<InsightsBaseResponse>> {

    @Autowired
    private AggregationService aggregationService;


    public List<InsightsBaseResponse> transform(List<Template> keys, RequestObj requestObj) {

        AggregationBo menuAggregationBo = aggregationService.aggregate(keys, requestObj);
        return convertToTransformedObject(menuAggregationBo);
    }

    public static List<InsightsBaseResponse> convertToTransformedObject(AggregationBo menuAggregationBo) {
        QueryResult queryResult = null;
        QueryResult rawOrderQueryResult = null;
        QueryResult oosResult = null;

        if (ObjectUtils.isEmpty(menuAggregationBo)) {
            return new ArrayList<>();
        }

        List<InsightsBaseResponse> data = new ArrayList<>();

        for (Template key : menuAggregationBo.getData().keySet()) {
            if(key.equals(Template.ITEMS) || key.equals(Template.ITEMSCATEGORYBASIS)) {
                rawOrderQueryResult = (QueryResult) menuAggregationBo.getData().get(Template.RAW_ORDER);
            }
            if(key.equals(Template.ITEMS)){
                oosResult = (QueryResult) menuAggregationBo.getData().get(Template.OOS_ITEMS);
            }
            if (key.equals(Template.ITEMS)) {
                queryResult = (QueryResult) menuAggregationBo.getData().get(Template.ITEMS);
                data = getInsightsBaseResponses(ResponseType.getValue(ResponseType.ITEM_WITH_OOS), queryResult, rawOrderQueryResult, oosResult);
            } else if(key.equals(Template.CATEGORY)) {
                queryResult = (QueryResult) menuAggregationBo.getData().get(Template.CATEGORY);
                data = getInsightsBaseResponses(ResponseType.getValue(ResponseType.MENU), queryResult, rawOrderQueryResult, oosResult);
            } else if(key.equals(Template.SUBCATEGORY)) {
                queryResult = (QueryResult) menuAggregationBo.getData().get(Template.SUBCATEGORY);
                data = getInsightsBaseResponses(ResponseType.getValue(ResponseType.MENU), queryResult, rawOrderQueryResult, oosResult);
            } else if(key.equals(Template.ITEMSCATEGORYBASIS)) {
                queryResult = (QueryResult) menuAggregationBo.getData().get(Template.ITEMSCATEGORYBASIS);
                data = getInsightsBaseResponses(ResponseType.getValue(ResponseType.ITEM), queryResult, rawOrderQueryResult, oosResult);
            }
        }

        return data;
    }

    /**
     *
     * @param key this defines build the object for item or not item - 0
     * @param queryResult output from the data query interface
     * @return
     */

    private static List<InsightsBaseResponse> getInsightsBaseResponses(Integer key, QueryResult queryResult, QueryResult rawResult, QueryResult oosResult) {
        if (ObjectUtils.isEmpty(queryResult)) {
            return new ArrayList<>();
        }

        Map<String, Object> rawTuple = new HashMap<>();
        List<InsightsBaseResponse> data = new ArrayList<>();

        if(key.equals(ResponseType.getValue(ResponseType.ITEM)) || key.equals(ResponseType.getValue(ResponseType.ITEM_WITH_OOS)))
            if(ObjectUtils.isEmpty(rawResult)){
                return null;
            } else {
               rawTuple = rawResult.getResult().get(0);
            }

        if(key.equals(ResponseType.getValue(ResponseType.ITEM_WITH_OOS))){
            List<ItemResponse> deliveredData = new ArrayList<>();
            for (Map<String, Object> itemTuple : cleanItems(queryResult.getResult())) {
                deliveredData.add(ItemResponse.builder()
                             .id(getId(itemTuple))
                             .name(getName(itemTuple))
                             .orderCount(getOrderCount(itemTuple))
                             .revenue(getRevenue(itemTuple))
                             .totalRevenue(getTotalRevenue(rawTuple))
                             .totalOrderCount(getOrderCount(rawTuple))
                             .isVeg(getIsVeg(itemTuple))
                             .category(getCategory(itemTuple))
                             .subCategory(getSubCategory(itemTuple))
                             .price(getPrice(itemTuple))
                             .build());
            }
            List<ItemResponse> oosData = new ArrayList<>();
            for (Map<String, Object> itemTuple : oosResult.getResult()) {
                oosData.add(ItemResponse.builder()
                        .id(getId(itemTuple))
                        .name(getName(itemTuple))
                        .orderCount(getOrderCount(itemTuple))
                        .isVeg(getIsVeg(itemTuple))
                        .category(getCategory(itemTuple))
                        .subCategory(getSubCategory(itemTuple))
                        .price(getPrice(itemTuple))
                        .build());
            }
            data.add(OrderMetricResponse.builder()
                .topDeliveredOrders(deliveredData)
                .topOosOrders(oosData)
                .build());
        } else if(key.equals(ResponseType.getValue(ResponseType.MENU))){
            for (Map<String, Object> menuTuple : queryResult.getResult()) {
                data.add(MenuResponse.builder()
                        .id(getId(menuTuple))
                        .name(getName(menuTuple))
                        .orderCount(getOrderCount(menuTuple))
                        .totalOrderCount(getTotalOrderCount(menuTuple))
                        .isVeg(getIsVeg(menuTuple))
                        .build());
            }
        } else {
            for (Map<String, Object> itemTuple : queryResult.getResult()) {
                data.add(ItemResponse.builder()
                        .id(getId(itemTuple))
                        .name(getName(itemTuple))
                        .orderCount(getOrderCount(itemTuple))
                        .totalOrderCount(getOrderCount(rawTuple))
                        .isVeg(getIsVeg(itemTuple))
                        .totalRevenue(getTotalRevenue(rawTuple))
                        .revenue(getRevenue(itemTuple))
                        .price(getPrice(itemTuple))
                        .build());
            }

        }
        return data;
    }

    private static List<Map<String, Object>> cleanItems(List<Map<String, Object>> results) {

        return results.stream().filter(result -> (Integer) result.get(Settings.ORDER_COUNT) > 0).collect(Collectors.toList());
    }

    private static Integer getOrderCount(Map<String, Object> result) {

        return (Integer) result.get(Settings.ORDER_COUNT);
    }

    private static Integer getRevenue(Map<String, Object> result) {

        return ((Double) result.get(Settings.REVENUE)).intValue();
    }

    private static Integer getTotalRevenue(Map<String, Object> result) {

        return ((Double) result.get(Settings.REVENUE)).intValue();
    }

    private static Integer getTotalOrderCount(Map<String, Object> result) {

        return (Integer) result.get(Settings.TOTAL_ORDER_COUNT);
    }

    private static Integer getId(Map<String, Object> result) {

        return (Integer) result.get(Settings.ID);
    }

    private static String getName(Map<String, Object> result) {

        return (String) result.get(Settings.NAME);
    }

    private static Boolean getIsVeg(Map<String, Object> result) {

        return (Boolean) result.get(Settings.IS_VEG);
    }

    private static String getCategory(Map<String, Object> result) {

        return (String) result.get(Settings.CATEGORY);
    }

    private static String getSubCategory(Map<String, Object> result) {

        return (String) result.get(Settings.SUB_CATEGORY);
    }

    private static Double getPrice(Map<String, Object> result){

        return (double) result.get(Settings.PRICE);
    }
}
