package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.ConfirmationTimeResponse;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.utils.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by sandeepjindal1 on 5/23/17.
 */
@Component
public class ConfirmationTimeTransformation implements TransformationService<List<InsightsBaseResponse>> {

    @Autowired
    private AggregationService aggregationService;


    public List<InsightsBaseResponse> transform(List<Template> keys, RequestObj requestObj) {

        AggregationBo orderAggregationBo = aggregationService.aggregate(keys, requestObj);
        return convertToTransformedObject(orderAggregationBo);
    }

    public static List<InsightsBaseResponse> convertToTransformedObject(AggregationBo confirmationTimeAggregationBo) {
        QueryResult queryResult = null;
//        RestaurantServiceResponse restaurantServiceResponse = null;

        if (ObjectUtils.isEmpty(confirmationTimeAggregationBo)) {
            return null;
        }

        for (Template key : confirmationTimeAggregationBo.getData().keySet()) {
            if (key.equals(Template.CONFIRMATION_TIME)) {
                queryResult = (QueryResult) confirmationTimeAggregationBo.getData().get(Template.CONFIRMATION_TIME);
                break;
            }
        }

        if (ObjectUtils.isEmpty(queryResult)) {
            return null;
        }

        ConfirmationTimeResponse confirmationTimeResponse = null;
        for (Map<String, Object> confirmationTimeTuple : queryResult.getResult()) {
            confirmationTimeResponse = ConfirmationTimeResponse.builder()
                    .avgConfirmationTime(getAvgConfirmationTime(confirmationTimeTuple))
                    .maxConfirmationTime(getMaxConfirmationTime(confirmationTimeTuple))
                    .minConfirmationTime(getMinConfirmationTime(confirmationTimeTuple))
                    .zeroThreeMin(getZeroThreeMinCount(confirmationTimeTuple))
                    .threeSixMin(getThreeSixCount(confirmationTimeTuple))
                    .sixNineMin(getSixNineCount(confirmationTimeTuple))
                    .ninePlus(getNinePlusCount(confirmationTimeTuple))
                    .confirmedByDe(getConfirmedByDeCount(confirmationTimeTuple))
                    .notAccepted(getnotAcceptedCount(confirmationTimeTuple))
                    .noOfOrders(getNoOfOrders(confirmationTimeTuple))
                    .acceptedOrders(getAcceptedOrders(confirmationTimeTuple))
                    .build();
        }

        List<InsightsBaseResponse> data = new ArrayList<>();
        data.add(confirmationTimeResponse);
        return data;
    }

    private static Double getMinConfirmationTime(Map<String, Object> confirmationTimeTuple) {
        return (double) Math.round((((Integer) confirmationTimeTuple.get(Settings.MIN_CONFIRMATION_TIME)).doubleValue()) * 100 / 60) / 100;
    }

    private static Double getMaxConfirmationTime(Map<String, Object> confirmationTimeTuple) {
        return (double) Math.round((((Integer) confirmationTimeTuple.get(Settings.MAX_CONFIRMATION_TIME)).doubleValue() * 100) / 60) / 100;
    }

    //Returns in minutes
    private static Double getAvgConfirmationTime(Map<String, Object> confirmationTimeTuple) {
        return (double) Math.round((Double) confirmationTimeTuple.get(Settings.AVG_CONFIRMATION_TIME) * 100 / 60) / 100;
    }

    private static Integer getZeroThreeMinCount(Map<String, Object> confirmationTimeTuple){
        return (Integer) confirmationTimeTuple.get(Settings.ZERO_THREE_MIN);
    }

    private static Integer getThreeSixCount(Map<String, Object> confirmationTimeTuple){
        return (Integer) confirmationTimeTuple.get(Settings.THREE_SIX_MIN);
    }

    private static Integer getSixNineCount(Map<String, Object> confirmationTimeTuple){
        return (Integer) confirmationTimeTuple.get(Settings.SIX_NINE_MIN);
    }

    private static Integer getConfirmedByDeCount(Map<String, Object> confirmationTimeTuple){
        return (Integer) confirmationTimeTuple.get(Settings.CONFIRMED_BY_DE);
    }

    private static Integer getnotAcceptedCount(Map<String, Object> confirmationTimeTuple){
        return (Integer) confirmationTimeTuple.get(Settings.NOT_ACCEPTED);
    }

    private static Integer getNoOfOrders(Map<String, Object> confirmationTimeTuple){
        return (Integer) confirmationTimeTuple.get(Settings.NO_OF_ORDERS);
    }

    private static Integer getNinePlusCount(Map<String, Object> confirmationTimeTuple){
        return (Integer) confirmationTimeTuple.get(Settings.NINE_PLUS);
    }

    private static Integer getAcceptedOrders(Map<String, Object> confirmationTimeTuple){
        return (Integer) confirmationTimeTuple.get(Settings.ACCEPTED_ORDERS);
    }

}
