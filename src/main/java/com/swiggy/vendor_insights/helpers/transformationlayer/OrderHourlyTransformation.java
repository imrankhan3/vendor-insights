package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.vendor_insights.enums.Peaks;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.ConfirmationTimePeakResponse;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.orders.OrderHourlyResponse;
import com.swiggy.vendor_insights.pojos.responseobj.orders.PeakOrderResponse;
import com.swiggy.vendor_insights.utils.Settings;
import com.swiggy.vendor_insights.utils.Tuple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import com.swiggy.vendor_insights.helpers.PeakHelper;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by sandeepjindal1 on 5/30/17.
 */
@Component
public class OrderHourlyTransformation implements TransformationService<List<InsightsBaseResponse>> {

    @Autowired
    private AggregationService aggregationService;


    public List<InsightsBaseResponse> transform(List<Template> keys, RequestObj requestObj) {

        AggregationBo orderAggregationBo = aggregationService.aggregate(keys, requestObj);
        return convertToTransformedObject(orderAggregationBo);
    }

    public static List<InsightsBaseResponse> convertToTransformedObject(AggregationBo orderAggregationBo) {
        QueryResult queryResult = null;

        if (ObjectUtils.isEmpty(orderAggregationBo)) {
            return null;
        }

        for (Template key : orderAggregationBo.getData().keySet()) {
            if (key.equals(Template.ORDERS_HOURLY)) {
                queryResult = (QueryResult) orderAggregationBo.getData().get(Template.ORDERS_HOURLY);
                break;
            }
        }

        if (ObjectUtils.isEmpty(queryResult)) {
            return null;
        }

        List<OrderHourlyResponse> data = new ArrayList<>();
        for (Map<String, Object> orderTuple : queryResult.getResult()) {
            data.add(OrderHourlyResponse.builder()
                    .hour(getHour(orderTuple))
                    .revenue(getRevenue(orderTuple))
                    .numberOfOrders(getNumberOfOrders(orderTuple))
                    .numberOfCancelledOrders(getCancelledOrders(orderTuple))
                    .numberOfDeliveredOrders(getDeliveredOrders(orderTuple))
                    .losses(getLosses(orderTuple))
                    .build());
        }
        return peakWiseData(data);
    }

    /**
     *
     * @param hourList list of orders metrics based on hours
     * @return data transformed day wise
     */
    private static List<InsightsBaseResponse> peakWiseData(List<OrderHourlyResponse> hourList){
        List<InsightsBaseResponse> data = new ArrayList<>();
        hourList.stream().collect(Collectors.groupingBy(k -> PeakHelper.getPeak(k.getHour()), Collectors.toList()))
                .entrySet()
                .stream()
                .map(peaksListEntry -> new Tuple<>(peaksListEntry.getKey(), peaksListEntry.getValue().stream().map(ohr -> {
                    PeakOrderResponse peakOrderResponse = new PeakOrderResponse();
                    peakOrderResponse.setNumberOfOrders(ohr.getNumberOfOrders());
                    peakOrderResponse.setRevenue(ohr.getRevenue());
                    peakOrderResponse.setLosses(ohr.getLosses());
                    peakOrderResponse.setNumberOfCancelledOrders(ohr.getNumberOfCancelledOrders());
                    peakOrderResponse.setNumberOfDeliveredOrders(ohr.getNumberOfDeliveredOrders());
                    peakOrderResponse.setOrderHourlyResponse(Collections.singletonList(ohr));
                    peakOrderResponse.setPeak(peaksListEntry.getKey());
                    return peakOrderResponse;
                }).collect(Collectors.toList())))
                .map(peaksListEntry -> new Tuple<>(peaksListEntry.getLeft(), peaksListEntry.getRight()
                        .stream().reduce((a, b) -> {
                            a.setRevenue(a.getRevenue() + b.getRevenue());
                            a.setNumberOfOrders(a.getNumberOfOrders() + b.getNumberOfOrders());
                            a.setNumberOfDeliveredOrders(a.getNumberOfDeliveredOrders() + b.getNumberOfDeliveredOrders());
                            a.setNumberOfCancelledOrders(a.getNumberOfCancelledOrders() + b.getNumberOfCancelledOrders());
                            a.setLosses(a.getLosses() + b.getLosses());
                            List<OrderHourlyResponse> m = new ArrayList<>();
                            m.addAll(a.getOrderHourlyResponse());
                            m.addAll(b.getOrderHourlyResponse());
                            a.setOrderHourlyResponse(m);
                            return a;
                        })))
                .filter(k -> k.getRight().isPresent())
                .map(k -> new Tuple<>(k.getLeft(),k.getRight().get())).forEach(k -> {
                    PeakOrderResponse a = k.getRight();
                    a.setPeak(k.getLeft());
                    data.add(a);
                });
        return handleEmptyPeaks(data);

    }

    private static List<InsightsBaseResponse> handleEmptyPeaks(List<InsightsBaseResponse> data){
        List<Peaks> existingPeaks = new ArrayList<>();
        for (InsightsBaseResponse entry: data){
            existingPeaks.add(((PeakOrderResponse) entry).getPeak());
        }
        for (Peaks peak: Peaks.values()){
            if(!existingPeaks.contains(peak)){
                data.add(PeakOrderResponse.builder()
                                    .revenue(0.0)
                                    .losses(0.0)
                                    .numberOfOrders(0)
                                    .numberOfDeliveredOrders(0)
                                    .numberOfCancelledOrders(0)
                                    .peak(peak)
                                    .build());
            }
        }
        return data;
    }

    private static Integer getNumberOfOrders(Map<String, Object> result) {

        return (Integer) result.get(Settings.NO_OF_ORDERS);
    }

    private static Double getRevenue(Map<String, Object> result) {

        return (Double) result.get(Settings.REVENUE);
    }

    private static Integer getHour(Map<String, Object> result) {

        return (Integer) result.get(Settings.HOUR);
    }

    private static Integer getCancelledOrders(Map<String, Object> result) {

        return (Integer) result.get(Settings.NO_OF_CANCELLED_ORDERS);
    }

    private static Integer getDeliveredOrders(Map<String, Object> result) {

        return (Integer) result.get(Settings.NO_OF_DELIVERED_ORDERS);
    }

    private static Double getLosses(Map<String, Object> result) {

        return (Double) result.get(Settings.LOSSES);
    }

}
