package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.GrowthPackCampaign;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.GrowthPackResponse;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.utils.Settings;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.*;

@Component
public class GrowthPackTransformation implements TransformationService<List<InsightsBaseResponse>> {

    private final static String CANCELLED_STATUS = "cancel";

    @Autowired
    private AggregationService aggregationService;

    @Override
    public List<InsightsBaseResponse> transform(@NonNull List<Template> keys, @NonNull RequestObj requestObj) {
        AggregationBo orderAggregationBo = aggregationService.aggregate(keys, requestObj);
        return convertToTransformedObject(orderAggregationBo, requestObj);
    }

    private List<InsightsBaseResponse> convertToTransformedObject(AggregationBo growthPackAggregationBo, RequestObj requestObj) {
        QueryResult queryResult = null;

        if (ObjectUtils.isEmpty(growthPackAggregationBo)) {
            return new ArrayList<>();
        }

        for (Template key : growthPackAggregationBo.getData().keySet()) {
            if (key.equals(Template.GROWTH_PACK_METRICS)) {
                queryResult = (QueryResult) growthPackAggregationBo.getData().get(Template.GROWTH_PACK_METRICS);
                return getMetrics(queryResult, requestObj);
            }
            if (key.equals(Template.GROWTH_PACK_HISTORY_METRICS)) {
                queryResult = (QueryResult) growthPackAggregationBo.getData().get(Template.GROWTH_PACK_HISTORY_METRICS);
                return getHistoryMetrics(queryResult, requestObj);
            }
        }
        return new ArrayList<>();
    }

    private List<InsightsBaseResponse> getMetrics(QueryResult queryResult, RequestObj requestObj) {

        if (ObjectUtils.isEmpty(queryResult)) {
            return new ArrayList<>();
        }

        List<InsightsBaseResponse> result = new ArrayList<>();
        for (Map<String, Object> opsTuple : queryResult.getResult()) {
            result.add(GrowthPackResponse.builder()
                    .totalOrders(getTotalOrders(opsTuple))
                    .newCustomers(getNewCustomers(opsTuple))
                    .oldCustomers(getOldCustomers(opsTuple))
                    .restaurantId(getRestaurantIds(opsTuple))
                    .GMV(getGMV(opsTuple))
                    .status(requestObj.getStatus())
                    .build());
        }

        return result;
    }


    private List<InsightsBaseResponse> getHistoryMetrics(QueryResult queryResult, RequestObj requestObj) {


        Integer totalOrders;
        Integer newCustomers;
        Integer oldCustomers;
        Integer restaurantId = 0;
        Double gmv;

        if (ObjectUtils.isEmpty(queryResult)) {
            return new ArrayList<>();
        }

        List<InsightsBaseResponse> result = new ArrayList<>();

        for (GrowthPackCampaign campaignIds : requestObj.getListOfGrowthPackCampaign()) {

            gmv = 0.0;

            Set<Integer> newCustomersSet = new HashSet<>();
            Set<Integer> totalCustomersSet = new HashSet<>();
            Set<Long> totalOrdersSet = new HashSet<>();
            List<Double> gmvList = new ArrayList<>();

            for (Map<String, Object> opsTuple : queryResult.getResult()) {
                if (campaignIds.getCampaignIds().contains(getCampaignId(opsTuple))) {

                    restaurantId = getRestaurantIds(opsTuple);

                    if ((getRevenue(opsTuple) > 1 && getIsReconciled(opsTuple))
                            || (getCancelledStatus(opsTuple).equals(CANCELLED_STATUS) && (getResponsibleId(opsTuple) == 2))) {

                        totalCustomersSet.add(getCustomerId(opsTuple));
                        totalOrdersSet.add(getOrderId(opsTuple));
                        gmvList.add(getRevenue(opsTuple));

                    }

                    if ((getRevenue(opsTuple) > 1 && getIsReconciled(opsTuple) && getRestaurantFirstOrder(opsTuple))
                            || (getCancelledStatus(opsTuple).equals(CANCELLED_STATUS) && getResponsibleId(opsTuple) == 2
                            && getRestaurantFirstOrder(opsTuple))) {

                        newCustomersSet.add(getCustomerId(opsTuple));
                    }
                }
            }
            totalOrders = totalOrdersSet.size();
            newCustomers = newCustomersSet.size();
            oldCustomers = totalCustomersSet.size() - newCustomersSet.size();
            for (Double revenue : gmvList) {
                gmv += revenue;

            }

            result.add(GrowthPackResponse.builder()
                    .totalOrders(totalOrders)
                    .newCustomers(newCustomers)
                    .oldCustomers(oldCustomers)
                    .restaurantId(restaurantId)
                    .campaignId(campaignIds.getCampaignIds())
                    .GMV(gmv)
                    .build());
        }
        return result;
    }

    private Integer getRestaurantIds(Map<String, Object> result) {
        return result.get(Settings.REST_ID) == null ? -1 : (Integer) result.get(Settings.REST_ID);
    }

    private Integer getTotalOrders(Map<String, Object> result) {
        return result.get(Settings.TOTAL_ORDERS) == null ? 0 : (Integer) result.get(Settings.TOTAL_ORDERS);
    }

    private Integer getNewCustomers(Map<String, Object> result) {
        return result.get(Settings.NEW_CUSTOMERS) == null ? 0 : (Integer) result.get(Settings.NEW_CUSTOMERS);
    }

    private Integer getOldCustomers(Map<String, Object> result) {
        return result.get(Settings.OLD_CUSTOMERS) == null ? 0 : (Integer) result.get(Settings.OLD_CUSTOMERS);
    }

    private Integer getCampaignId(Map<String, Object> result) {
        return result.get(Settings.CAMPAIGN_ID) == null ? -1 : (Integer) result.get(Settings.CAMPAIGN_ID);
    }

    private Double getGMV(Map<String, Object> result) {
        return result.get(Settings.GMV) == null ? 0 : (Double) result.get(Settings.GMV);
    }

    private Integer getCustomerId(Map<String, Object> result) {
        return result.get(Settings.CUSTOMER_ID) == null ? -1 : (Integer) result.get(Settings.CUSTOMER_ID);
    }

    private Boolean getIsReconciled(Map<String, Object> result) {
        return result.get(Settings.IS_RECONCILED) != null
                && ("1".equals(result.get(Settings.IS_RECONCILED).toString())
                || Boolean.TRUE.toString().equals(result.get(Settings.IS_RECONCILED).toString()));
    }

    private String getCancelledStatus(Map<String, Object> result) {
        return result.get(Settings.CANCELLED_STATUS) == null ? "0" : (String) result.get(Settings.CANCELLED_STATUS);
    }

    private Boolean getRestaurantFirstOrder(Map<String, Object> result) {

        return result.get(Settings.RESTAURANT_FIRST_ORDER) != null
                && ("1".equals(result.get(Settings.RESTAURANT_FIRST_ORDER).toString())
                || Boolean.TRUE.toString().equals(result.get(Settings.RESTAURANT_FIRST_ORDER).toString()));
    }

    private Double getRevenue(Map<String, Object> result) {
        return result.get(Settings.REVENUE) == null ? 0 : (Double) result.get(Settings.REVENUE);
    }

    private Integer getResponsibleId(Map<String, Object> result) {
        return result.get(Settings.RESPONSIBLE_ID) == null ? 0 : (Integer) result.get(Settings.RESPONSIBLE_ID);
    }

    private Long getOrderId(Map<String, Object> result) {
        return result.get(Settings.ORDER_ID) == null ? -1L : (Long) result.get(Settings.ORDER_ID);
    }

}
