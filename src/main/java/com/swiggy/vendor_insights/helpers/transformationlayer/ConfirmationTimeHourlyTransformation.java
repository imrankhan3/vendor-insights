package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.Peaks;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.PeakHelper;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.ConfirmationTimeHourlyResponse;
import com.swiggy.vendor_insights.pojos.responseobj.ConfirmationTimePeakResponse;
import com.swiggy.vendor_insights.pojos.responseobj.ConfirmationTimeResponse;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.utils.Settings;
import com.swiggy.vendor_insights.utils.Tuple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import java.lang.Math;

/**
 * Created by sandeepjindal1 on 5/30/17.
 */
@Component
public class ConfirmationTimeHourlyTransformation implements TransformationService<List<InsightsBaseResponse>> {

    @Autowired
    private AggregationService aggregationService;


    public List<InsightsBaseResponse> transform(List<Template> keys, RequestObj requestObj) {

        AggregationBo orderAggregationBo = aggregationService.aggregate(keys, requestObj);
        return convertToTransformedObject(orderAggregationBo);
    }

    public static List<InsightsBaseResponse> convertToTransformedObject(AggregationBo orderAggregationBo) {
        QueryResult queryResult = null;

        if (ObjectUtils.isEmpty(orderAggregationBo)) {
            return null;
        }

        for (Template key : orderAggregationBo.getData().keySet()) {
            if (key.equals(Template.CONFIRMATION_TIME_HOURLY)) {
                queryResult = (QueryResult) orderAggregationBo.getData().get(Template.CONFIRMATION_TIME_HOURLY);
                break;
            }
        }

        if (ObjectUtils.isEmpty(queryResult)) {
            return null;
        }

        List<ConfirmationTimeHourlyResponse> data = new ArrayList<>();
        for (Map<String, Object> confirmationTimeTuple : queryResult.getResult()) {
            data.add(ConfirmationTimeHourlyResponse.builder()
                    .avgConfirmationTime(getAvgConfirmationTime(confirmationTimeTuple))
                    .maxConfirmationTime(getMaxConfirmationTime(confirmationTimeTuple))
                    .minConfirmationTime(getMinConfirmationTime(confirmationTimeTuple))
                    .zeroThreeMin(getZeroThreeMinCount(confirmationTimeTuple))
                    .threeSixMin(getThreeSixCount(confirmationTimeTuple))
                    .sixNineMin(getSixNineCount(confirmationTimeTuple))
                    .ninePLus(getNinePlusCount(confirmationTimeTuple))
                    .confirmedByDe(getConfirmedByDeCount(confirmationTimeTuple))
                    .notAccepted(getnotAcceptedCount(confirmationTimeTuple))
                    .noOfOrders(getNoOfOrders(confirmationTimeTuple))
                    .hour(getHour(confirmationTimeTuple))
                    .acceptedOrders(getAcceptedOrders(confirmationTimeTuple))
                    .build());
        }
        return peakWiseData(data);
    }

    /**
     *
     * @param hourList list of orders metrics based on hours
     * @return data transformed day wise
     */
    private static List<InsightsBaseResponse> peakWiseData(List<ConfirmationTimeHourlyResponse> hourList){
        List<InsightsBaseResponse> data = new ArrayList<>();
        hourList.stream().collect(Collectors.groupingBy(k -> PeakHelper.getPeak(k.getHour()), Collectors.toList()))
                .entrySet()
                .stream()
                .map(peaksListEntry -> new Tuple<>(peaksListEntry.getKey(), peaksListEntry.getValue().stream().map(ohr -> {
                            ConfirmationTimePeakResponse peakResponse = new ConfirmationTimePeakResponse();
                            peakResponse.setAvgConfirmationTime(ohr.getAvgConfirmationTime());
                            peakResponse.setMaxConfirmationTime(ohr.getMaxConfirmationTime());
                            peakResponse.setMinConfirmationTime(ohr.getMinConfirmationTime());
                            peakResponse.setZeroThreeMin(ohr.getZeroThreeMin());
                            peakResponse.setThreeSixMin(ohr.getThreeSixMin());
                            peakResponse.setSixNineMin(ohr.getSixNineMin());
                            peakResponse.setNinePlus(ohr.getNinePLus());
                            peakResponse.setConfirmedByDe(ohr.getConfirmedByDe());
                            peakResponse.setNotAccepted(ohr.getNotAccepted());
                            peakResponse.setNoOfOrders(ohr.getNoOfOrders());
                            peakResponse.setAcceptedOrders(ohr.getAcceptedOrders());
                            peakResponse.setConfirmationHourlyResponse(Collections.singletonList(ohr));
                            peakResponse.setPeak(peaksListEntry.getKey());
                            return peakResponse;
                        }).collect(Collectors.toList())))
                        .map(peaksListEntry -> new Tuple<>(peaksListEntry.getLeft(), peaksListEntry.getRight()
                                .stream().reduce((a,b) -> {
                                            a.setZeroThreeMin(a.getZeroThreeMin() + b.getZeroThreeMin());
                                            a.setThreeSixMin(a.getThreeSixMin() + b.getThreeSixMin());
                                            a.setSixNineMin(a.getSixNineMin() + b.getSixNineMin());
                                            a.setNinePlus(a.getNinePlus() + b.getNinePlus());
                                            a.setConfirmedByDe(a.getConfirmedByDe() + b.getConfirmedByDe());
                                            a.setNotAccepted(a.getNotAccepted() + b.getNotAccepted());
                                            a.setNoOfOrders(a.getNoOfOrders() + b.getNoOfOrders());
                                            a.setAcceptedOrders(a.getAcceptedOrders() + b.getAcceptedOrders());
                                            List<ConfirmationTimeHourlyResponse> m = new ArrayList<>();
                                            m.addAll(a.getConfirmationHourlyResponse());
                                            m.addAll(b.getConfirmationHourlyResponse());
                                            a.setConfirmationHourlyResponse(m);
                                            a.setMaxConfirmationTime(Math.max(a.getMaxConfirmationTime(), b.getMaxConfirmationTime()));
                                            a.setMinConfirmationTime(Math.min(a.getMinConfirmationTime(), b.getMinConfirmationTime()));
                                            a.setAvgConfirmationTime((double) Math.round(((a.getAvgConfirmationTime() * a.getNoOfOrders() + b.getAvgConfirmationTime() * b.getNoOfOrders()) * 100) / (a.getNoOfOrders() + b.getNoOfOrders())) / 100);
                                            return a;
                                        })))
                        .filter(k -> k.getRight().isPresent())
                        .map(k -> new Tuple<>(k.getLeft(),k.getRight().get())).forEach(k -> {
                             ConfirmationTimePeakResponse a = k.getRight();
                             a.setPeak(k.getLeft());
                             data.add(a);
                        });
        return handleEmptyPeaks(data);
    }

    private static List<InsightsBaseResponse> handleEmptyPeaks(List<InsightsBaseResponse> data){
        List<Peaks> existingPeaks = new ArrayList<>();
        for (InsightsBaseResponse entry: data){
            existingPeaks.add(((ConfirmationTimePeakResponse) entry).getPeak());
        }
        for (Peaks peak: Peaks.values()){
            if (!existingPeaks.contains(peak)){
                data.add(ConfirmationTimePeakResponse.builder()
                        .avgConfirmationTime(0.0)
                        .maxConfirmationTime(0.0)
                        .minConfirmationTime(0.0)
                        .zeroThreeMin(0)
                        .threeSixMin(0)
                        .sixNineMin(0)
                        .ninePlus(0)
                        .confirmedByDe(0)
                        .notAccepted(0)
                        .noOfOrders(0)
                        .acceptedOrders(0)
                        .peak(peak)
                        .build());
            }
        }
        return data;
    }

    private static Double getMinConfirmationTime(Map<String, Object> confirmationTimeTuple) {
        return (double) Math.round((((Integer) confirmationTimeTuple.get(Settings.MIN_CONFIRMATION_TIME)).doubleValue()) * 100 / 60) / 100;
    }

    private static Double getMaxConfirmationTime(Map<String, Object> confirmationTimeTuple) {
        return (double) Math.round((((Integer) confirmationTimeTuple.get(Settings.MAX_CONFIRMATION_TIME)).doubleValue() * 100) / 60) / 100;
    }

    private static Double getAvgConfirmationTime(Map<String, Object> confirmationTimeTuple) {
        return (double) Math.round((Double) confirmationTimeTuple.get(Settings.AVG_CONFIRMATION_TIME) * 100 / 60) / 100;
    }

    private static Integer getZeroThreeMinCount(Map<String, Object> confirmationTimeTuple){
        return (Integer) confirmationTimeTuple.get(Settings.ZERO_THREE_MIN);
    }

    private static Integer getThreeSixCount(Map<String, Object> confirmationTimeTuple){
        return (Integer) confirmationTimeTuple.get(Settings.THREE_SIX_MIN);
    }

    private static Integer getSixNineCount(Map<String, Object> confirmationTimeTuple){
        return (Integer) confirmationTimeTuple.get(Settings.SIX_NINE_MIN);
    }

    private static Integer getNinePlusCount(Map<String, Object> confirmationTimeTuple){
        return (Integer) confirmationTimeTuple.get(Settings.NINE_PLUS);
    }

    private static Integer getConfirmedByDeCount(Map<String, Object> confirmationTimeTuple){
        return (Integer) confirmationTimeTuple.get(Settings.CONFIRMED_BY_DE);
    }

    private static Integer getnotAcceptedCount(Map<String, Object> confirmationTimeTuple){
        return (Integer) confirmationTimeTuple.get(Settings.NOT_ACCEPTED);
    }

    private static Integer getNoOfOrders(Map<String, Object> confirmationTimeTuple){
        return (Integer) confirmationTimeTuple.get(Settings.NO_OF_ORDERS);
    }

    private static Integer getHour(Map<String, Object> confirmationTimeTuple){
        return (Integer) confirmationTimeTuple.get(Settings.HOUR);
    }

    private static Integer getAcceptedOrders(Map<String, Object> confirmationTimeTuple){
        return (Integer) confirmationTimeTuple.get(Settings.ACCEPTED_ORDERS);
    }

}
