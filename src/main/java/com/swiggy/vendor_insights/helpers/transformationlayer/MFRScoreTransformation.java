package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.swiggy.vendor_insights.constants.MfrConstants.MFR_ACCURATE_ORDERS;
import static com.swiggy.vendor_insights.constants.MfrConstants.TOTAL_DELIVERED_ORDERS;

@Component
public class MFRScoreTransformation {

    @Autowired
    private AggregationService aggregationService;

    public List<Long> transform(List<Template> keys, RequestObj requestObj) {

        AggregationBo orderAggregationBo = aggregationService.aggregate(keys, requestObj);
        return convertToTransformedObject(orderAggregationBo);
    }

    public static List<Long> convertToTransformedObject(AggregationBo orderAggregationBo) {
        Optional<AggregationBo> orderAggregationBoOptional = Optional.ofNullable(orderAggregationBo);

        Optional<Map<String, Object>> queryResultDataMap = orderAggregationBoOptional
                .flatMap(orderAggregation -> Optional.ofNullable(orderAggregation.getData()))
                .flatMap(orderAggregationBoMap -> Optional.ofNullable((QueryResult) orderAggregationBoMap.get(Template.MFR_SCORE_KEY)))
                .flatMap(queryResult -> Optional.ofNullable(queryResult.getResult()))
                .flatMap(queryResultData -> Optional.ofNullable(queryResultData.get(0)));

        Optional<Integer> totalOrdersOptional = queryResultDataMap.flatMap(result -> Optional.ofNullable((Integer) result.get(TOTAL_DELIVERED_ORDERS)));
        Optional<Integer> mfrAccurateOrdersOptional = queryResultDataMap.flatMap(result -> Optional.ofNullable((Integer) result.get(MFR_ACCURATE_ORDERS)));
        int totalOrders = totalOrdersOptional.orElse(0);

        int mfrAccurateOrders = mfrAccurateOrdersOptional.orElse(0);

        List<Long> data = new ArrayList<>();
        data.add((long) totalOrders);
        data.add((long) mfrAccurateOrders);

        return data;
    }
}
