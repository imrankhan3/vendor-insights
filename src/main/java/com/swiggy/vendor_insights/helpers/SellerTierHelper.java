package com.swiggy.vendor_insights.helpers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.TierInfo;
import com.swiggy.vendor_insights.utils.Settings;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;


@Component
public class SellerTierHelper {

    private static String unTieredMessage = "Currently no tier has been assigned to this restaurant. Tier will be assigned in next business cycle.";
    private static String negativeMessage = "This doesn’t look good! Improve metrics in red to keep enjoying %s tier benefits.";
    private static String negativeMessage2 = "Go for %s! Improve your metrics in red to unlock %s tier benefits from next month.";
    private static String positiveMessage = "Good work! Maintain metrics in green to keep enjoying %s tier benefits.";
    private static String positiveMessage2 = "Great work! You are close to unlocking %s tier benefits from next month.";


    private static String currentTierKey = "currentTier";
    private static String nextTierKey = "nextTier";

    private static String platinum = "Platinum";
    private static String gold = "Gold";
    private static String silver = "Silver";
    private static String bronze = "Bronze";
    private static String untiered = "Untiered";

    ObjectMapper objectMapper = new ObjectMapper();

    public Pair<String, Boolean> getMessage(Map<String, Object> data){
        String message = null;
        Boolean isMessagePositive = null;
        if(data == null){
            return new Pair(message, isMessagePositive);
        }
        TierInfo currentTierInfo = objectMapper.convertValue(data.get(currentTierKey), TierInfo.class);
        TierInfo nextTierInfo = objectMapper.convertValue(data.get(nextTierKey), TierInfo.class);

        if (currentTierInfo.getTier() != null) {
            if (untiered.equals(currentTierInfo.getTier())) {
                message = unTieredMessage;
            } else {
                if (currentTierInfo.getAcceptanceRate() &&
                        currentTierInfo.getCancellationRate() &&
                        currentTierInfo.getEditRate() &&
                        currentTierInfo.getRating() &&
                        currentTierInfo.getTotalSales()) {
                    isMessagePositive = true;
                    if (bronze.equals(currentTierInfo.getTier()) || silver.equals(currentTierInfo.getTier())) {
                        message = String.format(positiveMessage2, nextTierInfo.getTier());
                    } else if (platinum.equals(currentTierInfo.getTier())) {
                        message = String.format(positiveMessage, currentTierInfo.getTier());
                    } else if (gold.equals(currentTierInfo.getTier())) {
                        message = String.format(positiveMessage, currentTierInfo.getTier());
                        if (nextTierInfo.getAcceptanceRate() ||
                                nextTierInfo.getCancellationRate() ||
                                nextTierInfo.getEditRate() ||
                                nextTierInfo.getRating() ||
                                nextTierInfo.getTotalSales()) {
                            message = String.format(positiveMessage2, nextTierInfo.getTier());
                        }
                    }
                } else {
                    isMessagePositive = false;
                    if (platinum.equals(currentTierInfo.getTier()) || gold.equals(currentTierInfo.getTier())) {
                        message = String.format(negativeMessage, currentTierInfo.getTier());
                    } else if (bronze.equals(currentTierInfo.getTier()) || silver.equals(currentTierInfo.getTier())) {
                        message = String.format(negativeMessage2, nextTierInfo.getTier(), nextTierInfo.getTier());
                    }
                }
            }
        }
        return new Pair(message, isMessagePositive);
    }
}
