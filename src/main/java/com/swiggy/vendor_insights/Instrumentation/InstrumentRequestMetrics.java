package com.swiggy.vendor_insights.Instrumentation;

import java.lang.annotation.*;

@Target(value = {ElementType.METHOD})
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
public @interface InstrumentRequestMetrics {

    String type() default "default";
}
