package com.swiggy.vendor_insights.Instrumentation;

import io.prometheus.client.Counter;
import io.prometheus.client.Gauge;
import io.prometheus.client.Histogram;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;


@Aspect
@Component
@Slf4j
public class InstrumentRequestMetricsAspect {

    private static final String VENDOR_INSIGHTS_REQUEST_COUNTER = "vendor_insight_request_counter";
    private static final String VENDOR_INSIGHTS_REQUEST_COUNTER_HELPER = "vendor_insight_request_counter_helper";
    private static final String VENDOR_INSIGHTS_REQUEST_PROGRESS = "vendor_insights_request_progress";
    private static final String VENDOR_INSIGHTS_REQUEST_PROGRESS_HELPER = "vendor_insights_request_progress_helper";
    private static final String VENDOR_INSIGHTS_REQUEST_HISTOGRAM = "vendor_insight_request_histogram";
    private static final String VENDOR_INSIGHTS_REQUEST_HISTOGRAM_HELPER = "vendor_insight_request_histogram_helper";
    private static final String VENDOR_INSIGHTS_REQUEST_STATUS = "vendor_insights_request_status";
    private static final String VENDOR_INSIGHTS_API_NAME = "vendor_insights_api_name";
    private static final String SUCCESS = "success";
    private static final String TOTAL_COUNT = "total_requests";
    private static final String DEFAULT = "default";

    private Counter requestCounter;

    private Gauge requestProgress;

    private Histogram histogram;

    public InstrumentRequestMetricsAspect() {
        this.requestCounter = Counter.build()
                .name(VENDOR_INSIGHTS_REQUEST_COUNTER)
                .labelNames(VENDOR_INSIGHTS_API_NAME, VENDOR_INSIGHTS_REQUEST_STATUS)
                .help(VENDOR_INSIGHTS_REQUEST_COUNTER_HELPER)
                .register();

        this.requestProgress = Gauge.build()
                .name(VENDOR_INSIGHTS_REQUEST_PROGRESS)
                .labelNames(VENDOR_INSIGHTS_API_NAME)
                .help(VENDOR_INSIGHTS_REQUEST_PROGRESS_HELPER)
                .register();

        this.histogram = Histogram.build()
                .name(VENDOR_INSIGHTS_REQUEST_HISTOGRAM)
                .labelNames(VENDOR_INSIGHTS_API_NAME)
                .help(VENDOR_INSIGHTS_REQUEST_HISTOGRAM_HELPER)
                .register();
    }

    @Pointcut("@annotation(instrumentRequestMetrics)")
    public void InstrumentRequestMetricsDefinition(InstrumentRequestMetrics instrumentRequestMetrics) {
    }

    @Around(value = "InstrumentRequestMetricsDefinition(instrumentRequestMetrics)")
    public Object metricsAroundExecution(ProceedingJoinPoint proceedingJoinPoint, InstrumentRequestMetrics instrumentRequestMetrics) throws Throwable {

        Object object = null;
        String type = DEFAULT.equals(instrumentRequestMetrics.type()) ? proceedingJoinPoint.getSignature().toShortString() : instrumentRequestMetrics.type();
        this.requestCounter.labels(type, TOTAL_COUNT).inc();
        this.requestProgress.labels(type).inc();
        Histogram.Timer timer = this.histogram.labels(type).startTimer();
        try {
            object = proceedingJoinPoint.proceed();
            this.requestCounter.labels(type, SUCCESS).inc();

        } catch (Throwable ex) {
            this.requestCounter.labels(type, ex.getClass().getName()).inc();
            throw ex;
        } finally {
            this.requestProgress.labels(type).dec();
            timer.observeDuration();
        }
        return object;
    }
}
