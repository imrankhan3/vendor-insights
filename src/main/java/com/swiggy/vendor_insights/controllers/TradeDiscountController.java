package com.swiggy.vendor_insights.controllers;

import com.swiggy.commons.response.Response;
import com.swiggy.vendor_insights.Instrumentation.InstrumentRequestMetrics;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.transformationlayer.TradeDiscountMetricsTransformation;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/td/metrics", produces = "application/json")
public class TradeDiscountController extends BaseController<List<InsightsBaseResponse>> {

    @Autowired
    private TradeDiscountMetricsTransformation tradeDiscountMetricsTransformation;

    @InstrumentRequestMetrics
    @RequestMapping(method = RequestMethod.POST)
    public Response getTDMetrics(@RequestBody RequestObj requestObj) throws Exception {
        return new Response(1,
                getInsightsBaseResponse(requestObj,
                        Collections.singletonList(Template.TD_METRICS),
                        tradeDiscountMetricsTransformation),
                "TD metrics data fetched successfully");
    }
}
