package com.swiggy.vendor_insights.controllers;

import com.swiggy.commons.response.Response;
import com.swiggy.vendor_insights.Instrumentation.InstrumentRequestMetrics;
import com.swiggy.vendor_insights.helpers.HelpContextHelper;
import com.swiggy.vendor_insights.helpers.transformationlayer.HelpTransformation;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.HelpContextResponse;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.utils.Settings;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

/**
 * Created by sandeepjindal1 on 5/29/17.
 */
@RestController
@RequestMapping(value = "/v2/", produces = "application/json")
public class HelpController extends BaseController<InsightsBaseResponse>{

    @Autowired
    private HelpTransformation helpTransformation;

    @Autowired
    private HelpContextHelper helpContextHelper;

    @InstrumentRequestMetrics
    @RequestMapping(method = RequestMethod.POST, value = "/help/")
    public Response getHelpContext() throws Exception{
        return new Response(1, helpContextHelper.getMessage(Settings.HELP, "help_guide"), "fetched data successfully");
    }

    @InstrumentRequestMetrics
    @RequestMapping(path = "/help/refresh")
    public Response refreshCache() throws Exception {

        helpContextHelper.refresh();

        return new Response(1,"refreshed cached successfully" , "refreshed cached successfully");
    }
}
