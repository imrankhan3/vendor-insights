package com.swiggy.vendor_insights.controllers;


import com.swiggy.commons.response.Response;
import com.swiggy.vendor_insights.Instrumentation.InstrumentRequestMetrics;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.transformationlayer.OOSOrdersAndReasonsMonthlyTransformation;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by sandeep.jindal on 6/11/17.
 */

@RestController
@RequestMapping(value = "/v1/oos_metrics", produces = "application/json")
public class OOSMetricsController extends BaseController<List<InsightsBaseResponse>>{

    @Autowired
    private OOSOrdersAndReasonsMonthlyTransformation oosOrdersAndReasonsMonthlyTransformation;

    @InstrumentRequestMetrics
    @RequestMapping(method = RequestMethod.POST)
    public Response getOOSMetricsForRestaurant(@RequestBody RequestObj requestObj) throws Exception {
        return new Response(1, getInsightsBaseResponse(requestObj,
                                    Collections.singletonList(Template.ORDERS_OOS), oosOrdersAndReasonsMonthlyTransformation), "order data fetched successfully");
    }
}
