package com.swiggy.vendor_insights.controllers;


import com.swiggy.commons.response.Response;
import com.swiggy.vendor_insights.Instrumentation.InstrumentRequestMetrics;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.transformationlayer.MenuTransformation;
import com.swiggy.vendor_insights.helpers.transformationlayer.TransformationService;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by sandeepjindal on 5/17/17.
 */
@RestController
@RequestMapping(value = "/v1/menu/", produces = "application/json")
public class MenuController extends BaseController<List<InsightsBaseResponse>>{

    @Autowired
    private MenuTransformation menuTransformation;

    @InstrumentRequestMetrics
    @RequestMapping(method = RequestMethod.POST, path = "/items")
    public Response getItemsMeasures(@Valid @RequestBody RequestObj requestObj) throws Exception {

        List<Template> keys = new ArrayList<>();
        keys.add(Template.ITEMS);
        keys.add(Template.RAW_ORDER);
        keys.add(Template.OOS_ITEMS);
        List<InsightsBaseResponse> data = getInsightsBaseResponse(requestObj,
                                keys, menuTransformation);
        return new Response(1,data,"fetched data successfully");
    }

    @InstrumentRequestMetrics
    @RequestMapping(method = RequestMethod.POST, path = "/items_cat")
    public Response getItemsBasedOnCategoryAndSubcategoryMeasures(@Valid @RequestBody RequestObj requestObj) throws Exception {

        List<Template> keys = new ArrayList<>();
        keys.add(Template.ITEMSCATEGORYBASIS);
        keys.add(Template.RAW_ORDER);
        List<InsightsBaseResponse> data = getInsightsBaseResponse(requestObj,
                            keys, menuTransformation);
        return new Response(1,data,"fetched data successfully");
    }

    @InstrumentRequestMetrics
    @RequestMapping(method = RequestMethod.POST, path = "/categories")
    public Response getCategoryMeasures(@Valid @RequestBody RequestObj requestObj) throws Exception {

        List<InsightsBaseResponse> data = getInsightsBaseResponse(requestObj,
                            Collections.singletonList(Template.CATEGORY), menuTransformation);
        return new Response(1,data,"fetched data successfully");
    }

    @InstrumentRequestMetrics
    @RequestMapping(method = RequestMethod.POST, path = "/subcategories")
    public Response getSubCategoryMeasures(@Valid @RequestBody RequestObj requestObj) throws Exception {

        List<InsightsBaseResponse> data = getInsightsBaseResponse(requestObj,
                            Collections.singletonList(Template.SUBCATEGORY), menuTransformation);
        return new Response(1,data,"fetched data successfully");
    }

}