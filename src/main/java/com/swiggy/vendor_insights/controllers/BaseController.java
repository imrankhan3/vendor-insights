package com.swiggy.vendor_insights.controllers;

import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.transformationlayer.TransformationService;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * Created by lekhaj.v on 7/31/17.
 */
public class BaseController<T> {

    T getInsightsBaseResponse(@RequestBody RequestObj requestObj, List<Template> keys, TransformationService<T> transformationService){
        return transformationService.transform(keys, requestObj);
    }
}
