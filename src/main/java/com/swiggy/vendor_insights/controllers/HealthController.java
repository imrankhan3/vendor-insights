package com.swiggy.vendor_insights.controllers;

import com.swiggy.vendor_insights.Instrumentation.InstrumentRequestMetrics;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.swiggy.commons.response.Response;


import java.util.List;

/**
 * Created by sandeep.jindal on 9/25/17.
 */
@RestController
@RequestMapping(value = "/", produces = "application/json")
public class HealthController extends BaseController<List<InsightsBaseResponse>> {

    @InstrumentRequestMetrics
    @RequestMapping(method = {RequestMethod.GET}, path = "insights-new/ping")
    public Response healthCheck() throws Exception {
        return new Response(1, "All is well");
    }

}
