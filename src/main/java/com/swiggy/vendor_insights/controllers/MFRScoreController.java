package com.swiggy.vendor_insights.controllers;


import com.swiggy.vendor_insights.Instrumentation.InstrumentRequestMetrics;
import com.swiggy.vendor_insights.exceptions.exceptions.BadRequestException;
import com.swiggy.vendor_insights.exceptions.exceptions.MfrException;
import com.swiggy.vendor_insights.exceptions.pojos.MfrGenericResponse;
import com.swiggy.vendor_insights.services.MFRScoreService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

import static com.swiggy.vendor_insights.constants.MfrConstants.SUCCESS_CODE;
import static com.swiggy.vendor_insights.constants.MfrConstants.SUCCESS_MESSAGE;

@Slf4j
@RestController
@RequestMapping(value = "/vendor/v1/mfrscore/", produces = "application/json")
public class MFRScoreController {

    @Autowired
    private MFRScoreService mfrScoreService;

    @InstrumentRequestMetrics
    @RequestMapping(method = RequestMethod.GET, path = "/restaurants")
    public MfrGenericResponse getRestaurantDetails(
            @NotNull @RequestParam("restaurantIds") List<String> restaurantIds,
            @Valid @RequestParam(value = "fromDate", required = false) String fromDate,
            @Valid @RequestParam(value = "toDate", required = false) String  toDate,
            @RequestParam(value = "modelName", required = false) String modelName
    ) throws MfrException, BadRequestException {

        if( restaurantIds.size() == 0 )
            throw new BadRequestException("invalid restaurant id");

        try{
            return MfrGenericResponse.builder()
                    .code(SUCCESS_CODE)
                    .message(SUCCESS_MESSAGE)
                    .data(mfrScoreService.getMfrScore(restaurantIds, fromDate, toDate, modelName))
                    .build();
        }
        catch(BadRequestException e){
            log.info("BadRequestException occured");
            throw new BadRequestException(e.getMessage());
        } catch(Exception e){
            throw new MfrException(e.getMessage());
        }
    }
}
