package com.swiggy.vendor_insights.controllers;


import com.swiggy.commons.response.Response;
import com.swiggy.vendor_insights.Instrumentation.InstrumentRequestMetrics;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.transformationlayer.MetricTransformationService;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shahbaz.khalid on 5/22/17.
 */

@RestController
@RequestMapping(value = "/v1/metrics", produces = "application/json")
public class RestaurantMetricsController {

    @Autowired
    private MetricTransformationService metricsTransformation;

    @InstrumentRequestMetrics
    @RequestMapping(method = RequestMethod.POST,path = "/")
    public Response getMetricsForRestaurant (@RequestBody RequestObj requestObj) throws Exception {
        return new Response(1,getInsightsBaseResponses(requestObj),"order data fetched successfully");
    }

    /**
     *
     * @param requestObj
     * inculcate templates and request obj
     * @return metrics information
     */
    private List<InsightsBaseResponse> getInsightsBaseResponses(@RequestBody RequestObj requestObj) {
        List<Template> keys = new ArrayList<>();
        keys.add(Template.METRICS);
        return metricsTransformation.transform(keys, requestObj);
    }
}
