package com.swiggy.vendor_insights.controllers;


import com.swiggy.commons.response.Response;
import com.swiggy.vendor_insights.Instrumentation.InstrumentRequestMetrics;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.transformationlayer.CancellationMetricTransformation;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shahbaz.khalid on 5/22/17.
 */

@RestController
@RequestMapping(value = "/v1/cancellation_metrics", produces = "application/json")
public class CancellationMetricsController extends BaseController<List<InsightsBaseResponse>>{

    @Autowired
    private CancellationMetricTransformation cancellationMetricTransformation;

    @InstrumentRequestMetrics
    @RequestMapping(method = RequestMethod.POST)
    public Response getMetricsForRestaurant(@RequestBody RequestObj requestObj) throws Exception {
        List<Template> keys = new ArrayList<>();
        keys.add(Template.CANCELLATION);
        return new Response(1, getInsightsBaseResponse(requestObj, keys, cancellationMetricTransformation), "order data fetched successfully");
    }

}
