package com.swiggy.vendor_insights.controllers;

import com.swiggy.commons.response.Response;
import com.swiggy.vendor_insights.Instrumentation.InstrumentRequestMetrics;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.transformationlayer.SellerTieringTransformation;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by lekhaj.v on 8/3/17.
 */

@RestController
@RequestMapping(value = "/v1/seller_tiering", produces = "application/json")
public class SellerTieringController extends BaseController<InsightsBaseResponse>{

    @Autowired
    private SellerTieringTransformation sellerTieringTransformation;

    @InstrumentRequestMetrics
    @RequestMapping(method = RequestMethod.POST)
    public Response getSellerTierData(@RequestBody RequestObj requestObj) throws Exception {
        List<Template> keys = new ArrayList<>();
        keys.add(Template.SELLER_TIER_STATS);
        keys.add(Template.SRS);
        return new Response(1, getInsightsBaseResponse(requestObj, keys, sellerTieringTransformation), "Seller Tiering data fetched successfully");
    }
}
