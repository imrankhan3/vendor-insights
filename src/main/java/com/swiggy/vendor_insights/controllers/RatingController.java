package com.swiggy.vendor_insights.controllers;

import com.swiggy.commons.response.Response;
import com.swiggy.vendor_insights.Instrumentation.InstrumentRequestMetrics;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.transformationlayer.RatingForOrderTransformation;
import com.swiggy.vendor_insights.helpers.transformationlayer.RatingTransformation;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by sandeepjindal1 on 5/29/17.
 */
@Log4j2
@RestController
@RequestMapping(value = "/v2/", produces = "application/json")
public class RatingController extends BaseController<InsightsBaseResponse>{

    @Autowired
    private RatingTransformation ratingTransformation;

    @Autowired
    private RatingForOrderTransformation ratingforOrderTransformation;

    @InstrumentRequestMetrics
    @RequestMapping(method = RequestMethod.POST, path = "/ratings")
    public Response getRatingMeasures(@Valid @RequestBody RequestObj requestObj) throws Exception {
        log.info("fetched data successfully -{}", requestObj);

        List<Template> keys = new ArrayList<>();
        keys.add(Template.RATINGS);
        keys.add(Template.SRS);
        return new Response(1,getInsightsBaseResponse(requestObj, keys, ratingTransformation),"fetched data successfully");

    }

    @InstrumentRequestMetrics
    @RequestMapping(method = RequestMethod.POST, path = "/ratings_for_order")
    public Response getRatingData(@Valid @RequestBody RequestObj requestObj) throws Exception {

        return new Response(1,getInsightsBaseResponse(requestObj, Collections.singletonList(Template.RATING_FOR_ORDER), ratingforOrderTransformation),"fetched data successfully");

    }

}
