package com.swiggy.vendor_insights.controllers;

import com.swiggy.vendor_insights.Instrumentation.InstrumentRequestMetrics;
import com.swiggy.vendor_insights.exceptions.exceptions.BadRequestException;
import com.swiggy.vendor_insights.exceptions.exceptions.MfrReportException;
import com.swiggy.vendor_insights.exceptions.pojos.MfrGenericResponse;
import com.swiggy.vendor_insights.services.AveragePrepTimeService;
import com.swiggy.vendor_insights.services.MFRStatusReportService;
import com.swiggy.vendor_insights.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

import static com.swiggy.vendor_insights.constants.MfrConstants.SUCCESS_CODE;
import static com.swiggy.vendor_insights.constants.MfrConstants.SUCCESS_MESSAGE;

@RestController
@Slf4j
@RequestMapping(value = "/mfr/reporting/", produces = "application/json")
public class MFRReportingController {

    @Autowired
    private MFRStatusReportService mfrStatusService;

    @Autowired
    private AveragePrepTimeService averagePrepTimeService;

    @InstrumentRequestMetrics
    @RequestMapping(method = RequestMethod.GET, path = "/pastorders")
    public MfrGenericResponse getMfrStatusPastOrders(
            @RequestParam(value = "orderIds", required = false) List<Long> orderIds
    ) throws MfrReportException, BadRequestException {

        if(orderIds == null || orderIds.size() == 0) {
            throw new BadRequestException("invalid orderIds");
        }

        try {
            return MfrGenericResponse.builder()
                    .code(SUCCESS_CODE)
                    .message(SUCCESS_MESSAGE)
                    .data(mfrStatusService.getMfrStatus(orderIds))
                    .build();
        } catch (Exception e) {
            log.info("Exception occured, fetching mfr status for past orders, {}", e.getMessage());
            throw new MfrReportException(e.getMessage());
        }

    }

    @InstrumentRequestMetrics
    @RequestMapping(method = RequestMethod.GET, path = "/metrics")
    public MfrGenericResponse getMfrMetrics(
            @RequestParam(value = "restaurantId", required = false) Long restaurantId,
            @RequestParam(value = "fromDate", required = false) String fromDate,
            @RequestParam(value = "toDate", required = false) String toDate
    ) throws MfrReportException, BadRequestException {

        if (restaurantId == null || fromDate == null || toDate == null || restaurantId < 0) {
            throw new BadRequestException("invalid params");
        }

        try {
            return MfrGenericResponse.builder()
                    .code(SUCCESS_CODE)
                    .message(SUCCESS_MESSAGE)
                    .data(mfrStatusService.getMfrMetrics(restaurantId, DateUtils.convertToLocalDateTime(fromDate),
                            DateUtils.convertToLocalDateTime(toDate)))
                    .build();

        } catch (BadRequestException ex) {
            throw new BadRequestException("invalid date");
        } catch (Exception e) {
            log.info("Exception occured, fetching mfr metrics,{}", e.getMessage());
            throw new MfrReportException("Exception occured, fetching mfr metrics, " + e.getMessage());
        }

    }

    @InstrumentRequestMetrics
    @RequestMapping(method = RequestMethod.GET, path = "/preptime")
    public MfrGenericResponse getAveragePrepTime (@NotNull @RequestParam("restaurantId") String restaurantId,
                                                  @Valid
                                                  @RequestParam(value = "fromDate", required = false) String fromDate,
                                                  @Valid @RequestParam(value = "toDate", required = false) String  toDate) {
        if (restaurantId == null || fromDate == null || toDate == null || Long.parseLong(restaurantId) < 0 ) {
            throw new BadRequestException("invalid params");
        }
        return MfrGenericResponse.builder()
                .code(SUCCESS_CODE)
                .message(SUCCESS_MESSAGE)
                .data(averagePrepTimeService.getAveragePrepTime(restaurantId, DateUtils.convertToLocalDateTime(fromDate), DateUtils.convertToLocalDateTime(toDate)))
                .build();

    }
}
