package com.swiggy.vendor_insights.controllers;

import com.swiggy.commons.response.Response;
import com.swiggy.vendor_insights.Instrumentation.InstrumentRequestMetrics;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.exceptions.exceptions.InvalidFieldException;
import com.swiggy.vendor_insights.helpers.transformationlayer.ConfirmationTimeHourlyTransformation;
import com.swiggy.vendor_insights.helpers.transformationlayer.ConfirmationTimeTransformation;
import com.swiggy.vendor_insights.helpers.transformationlayer.TransformationService;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.utils.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by sandeepjindal1 on 5/29/17.
 */
@RestController
@RequestMapping(value = "/v1/", produces = "application/json")
public class ConfirmationTimeController extends BaseController<List<InsightsBaseResponse>>{

    @Autowired
    private ConfirmationTimeTransformation confirmationTimeTransformation;

    @Autowired
    private ConfirmationTimeHourlyTransformation confirmationTimeHourlyTransformation;

    @InstrumentRequestMetrics
    @RequestMapping(method = RequestMethod.POST, path = "/confirmation_time")
    public Response getConfirmationTimeMeasures(@Valid @RequestBody RequestObj requestObj) throws Exception {

        List<Template> keys = new ArrayList<>();
        TransformationService transformationService;

        if(Settings.PEAK_WISE.equals(requestObj.getGroup_by())){
            keys.add(Template.CONFIRMATION_TIME_HOURLY);
            transformationService = confirmationTimeHourlyTransformation;
        }else if (requestObj.getGroup_by() == null) {
            keys.add(Template.CONFIRMATION_TIME);
            transformationService = confirmationTimeTransformation;
        }else {
            throw new InvalidFieldException("group_by", "Valid values are : peak-wise");
        }

        return new Response(1, getInsightsBaseResponse(requestObj, keys, transformationService),"fetched data successfully");
    }

}
