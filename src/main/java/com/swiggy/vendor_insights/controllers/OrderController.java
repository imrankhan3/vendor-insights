package com.swiggy.vendor_insights.controllers;


import com.swiggy.commons.response.Response;
import com.swiggy.vendor_insights.Instrumentation.InstrumentRequestMetrics;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.exceptions.exceptions.InvalidFieldException;
import com.swiggy.vendor_insights.helpers.transformationlayer.OrderHourlyTransformation;
import com.swiggy.vendor_insights.helpers.transformationlayer.OrderMonthlyTransformation;
import com.swiggy.vendor_insights.helpers.transformationlayer.OrderTransformation;
import com.swiggy.vendor_insights.helpers.transformationlayer.OrderWeeklyTransformation;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.utils.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sandeepjindal on 5/17/17.
 */
@RestController
@RequestMapping(value = "/v1/", produces = "application/json")
public class OrderController {

    @Autowired
    private OrderTransformation orderTransformation;

    @Autowired
    private OrderMonthlyTransformation orderMonthlyTransformation;

    @Autowired
    private OrderWeeklyTransformation orderWeeklyTransformation;

    @Autowired
    private OrderHourlyTransformation orderHourlyTransformation;


    @InstrumentRequestMetrics
    @RequestMapping(method = RequestMethod.POST, path = "/orders")
    public Response getOrderMeasures(@Valid @RequestBody RequestObj requestObj) {
         return new Response(1,getInsightsBaseResponses(requestObj),"fetched data successfully");
    }

    /**
     *
     * @param requestObj
     * inculcate templates and request obj
     * @return order information
     */
    private List<InsightsBaseResponse> getInsightsBaseResponses(@Valid @RequestBody RequestObj requestObj) {
        List<Template> keys = new ArrayList<>();
        keys.add(Template.ORDERS);
        List<InsightsBaseResponse> data = new ArrayList<>();
        if(Settings.DAILY.equals(requestObj.getGroup_by())) {
            data = orderTransformation.transform(keys, requestObj);
        } else if(Settings.WEEKLY.equals(requestObj.getGroup_by())){
            data = orderWeeklyTransformation.transform(keys, requestObj);
        } else if(Settings.MONTHLY.equals(requestObj.getGroup_by())){
            List<Template> monthlyKeys = new ArrayList<>();
            monthlyKeys.add(Template.ORDERS_MONTHLY);
            data = orderMonthlyTransformation.transform(monthlyKeys, requestObj);
        } else if(Settings.PEAK_WISE.equals(requestObj.getGroup_by())){
            List<Template> hourlyKeys = new ArrayList<>();
            hourlyKeys.add(Template.ORDERS_HOURLY);
            data = orderHourlyTransformation.transform(hourlyKeys, requestObj);
        }else {
            throw new InvalidFieldException("group_by", "Valid values are : daily, weekly, monthly, peak-wise");
        }
        return data;
    }

}