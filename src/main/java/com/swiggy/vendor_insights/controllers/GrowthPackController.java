package com.swiggy.vendor_insights.controllers;

import com.swiggy.commons.response.Response;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.GrowthPackHelper;
import com.swiggy.vendor_insights.helpers.transformationlayer.GrowthPackTransformation;
import com.swiggy.vendor_insights.pojos.requestobj.*;
import com.swiggy.vendor_insights.utils.Settings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.*;

@RestController
@Slf4j
@RequestMapping(value = "/v1/growth-pack", produces = "application/json")
public class GrowthPackController extends BaseController<List<InsightsBaseResponse>> {

    @Autowired
    private GrowthPackTransformation growthPackTransformation;

    @Autowired
    private GrowthPackHelper growthPackHelper;

    @RequestMapping(method = {RequestMethod.POST}, path = "/metrics")
    public Response getGrowthPackMetrics(@Valid @RequestBody GrowthPackRequest growthPackRequest) {

        List<InsightsBaseResponse> insightsBaseResponse = new ArrayList<>();
        RequestObj pastRequestObject = growthPackHelper.getGrowthPackMetrics(growthPackRequest, Settings.PAST_STATUS);
        RequestObj currentRequestObject = growthPackHelper.getGrowthPackMetrics(growthPackRequest, Settings.CURRENT_STATUS);

        if (!pastRequestObject.getCampaign_id().isEmpty()) {
            insightsBaseResponse.addAll(getInsightsBaseResponse(pastRequestObject,
                    Collections.singletonList(Template.GROWTH_PACK_METRICS),
                    growthPackTransformation));
        }
        if (!currentRequestObject.getCampaign_id().isEmpty()) {
            insightsBaseResponse.addAll(getInsightsBaseResponse(currentRequestObject,
                    Collections.singletonList(Template.GROWTH_PACK_METRICS),
                    growthPackTransformation));
        }

        return new Response(1, insightsBaseResponse,
                "Growth Pack metrics data fetched successfully");
    }


    @RequestMapping(method = {RequestMethod.POST}, path = "/metrics/history")
    public Response getGrowthPackMetricsHistory(@Valid @RequestBody GrowthPackHistoryRequest growthPackHistoryMap) {

        List<InsightsBaseResponse> insightsBaseResponses = new ArrayList<>();
        for (Map.Entry<String, List<GrowthPackCampaign>> entry : growthPackHistoryMap.getGrowthPackHistoryMap().entrySet()) {
            RequestObj requestObj = growthPackHelper.getGrowthPackMetricsHistory(Long.valueOf(entry.getKey()), entry.getValue());
            if (!requestObj.getCampaign_id().isEmpty()) {
                insightsBaseResponses.addAll(getInsightsBaseResponse(requestObj,
                        Collections.singletonList(Template.GROWTH_PACK_HISTORY_METRICS),
                        growthPackTransformation));
            }
        }
        return new Response(1, insightsBaseResponses,
                "Growth Pack history metrics fetched successfully");
    }
}
