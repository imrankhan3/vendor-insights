package com.swiggy.vendor_insights.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by sandeepjindal1 on 5/25/17.
 */
@RestController
@RequestMapping(value = "/test", produces = "application/json")
public class TestController {

//    @Autowired
//    RedisTest redisTest;
//
//    @RequestMapping(method = RequestMethod.GET, path = "/")
//    public Response getKeys(){
//
//        List<OrderHourlyResponse> hourList = new ArrayList<>();
//        hourList.add(new OrderHourlyResponse(2,200d,8));
//        hourList.add(new OrderHourlyResponse(2,200d,9));
//        hourList.add(new OrderHourlyResponse(2,200d,11));
//        hourList.add(new OrderHourlyResponse(2,200d,16));
//        hourList.add(new OrderHourlyResponse(2,200d,15));
//
////        hourList.stream().collect(Collectors.groupingBy(f -> getPeak(f),
////                Collectors.collectingAndThen(Collectors.
////                        reducing((a,b)-> new PeakOrderResponse(a.getNumberOfOrders()+b.getNumberOfOrders(),
////                                a.getRevenue()+b.getRevenue(),"breakfast"), Optional.get))))
//
//        hourList.stream().collect(Collectors.groupingBy(k -> getPeak(k.getHour()), Collectors.toList()));
//
//        System.out.println("hey");
//
//
//        List<Map<Peaks, PeakOrderResponse>> collect = hourList.stream().collect(Collectors.groupingBy(k -> getPeak(k.getHour()), Collectors.toList()))
//                .entrySet()
//                .stream()
//                .map(peaksListEntry -> new Tuple<>(peaksListEntry.getKey(), peaksListEntry.getValue()
//                        .stream().reduce((a, b) -> {
//                            a.setRevenue(a.getRevenue() + b.getRevenue());
//                            a.setNumberOfOrders(a.getNumberOfOrders() + b.getNumberOfOrders());
//                            return a;
//                        })))
//                .map(k -> new Tuple<>(k.getLeft(), k.getRight()))
//                .map(k -> {
//                    Map<Peaks, PeakOrderResponse> data = new HashMap<>();
//                    OrderHourlyResponse orderHourlyResponse = k.getRight().get();
//                    PeakOrderResponse peakOrderResponse = new PeakOrderResponse();
//                    peakOrderResponse.setNumberOfOrders(orderHourlyResponse.getNumberOfOrders());
//                    peakOrderResponse.setRevenue(orderHourlyResponse.getRevenue());
//                    peakOrderResponse.setPeak(k.getLeft());
//                    data.put(k.getLeft(), peakOrderResponse);
//                    return data;
//                })
//                .collect(Collectors.toList());
//
//
////        List<String> data = redisTest.getKeys();
//        return new Response(1,collect,"tested");
//    }
//
//    private static Peaks getPeak(Integer h) {
//        if(h >= 7 && h <=12){
//            return Peaks.BREAKFAST;
//        }
//        else {
//            return Peaks.DINNER;
//        }
//    }
}
