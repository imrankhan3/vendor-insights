package com.swiggy.vendor_insights;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication()
@Import(value = {VendorInsightsApplicationConfig.class,
		RedisConfig.class})
public class VendorInsightsApplication {

	public static void main(String[] args) {
		SpringApplication.run(VendorInsightsApplication.class, args);
	}

}
