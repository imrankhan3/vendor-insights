package com.swiggy.vendor_insights.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static com.swiggy.vendor_insights.constants.MfrConstants.MFR_REQUEST_PREFIX;

@Slf4j
@Service
public class MfrScoreCacheImpl implements MfrScoreCache {

    @Autowired
    private ValueOperations<String, String> valueOperations;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public Optional<Double> getMFRScore(Long restaurantId, String modelName) {
        Optional<Double> mfrScore = Optional.empty();
        String mfrScoreStr = valueOperations.get(getCacheKey(restaurantId + "_" + modelName));
        if(!StringUtils.isEmpty(mfrScoreStr)) {
            log.info("Successfully got MfrScore from cache. RestaurantId: {}", restaurantId);
            mfrScore = Optional.ofNullable(Double.valueOf(mfrScoreStr));
        }
        return mfrScore;
    }

    /**
     * Set mfrScore into cache
     *
     * @param mfrScore
     */
    @Override
    public void setMFRScore(Long restaurantId, Double mfrScore,
                            String modelName, Long ttl) {
        if(mfrScore != null && ttl > 0) {
            valueOperations.set(
                    getCacheKey(restaurantId + "_" + modelName), Double.toString(mfrScore),
                    ttl, TimeUnit.SECONDS
            );
            log.info("Successfully set mfrScore into cache. mfrScore: {}", mfrScore);
        } else {
            log.error("Failed to set mfrScore into cache. MFRScoreData.MfrScore: {}", mfrScore);
        }
    }

    /**
     * generate cache key for mfrScore
     *
     * @param restaurantId
     * @return
     */
    private String getCacheKey(String restaurantId) {
        return MFR_REQUEST_PREFIX + restaurantId;
    }
}
