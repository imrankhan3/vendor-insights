package com.swiggy.vendor_insights.cache;

import java.util.Optional;

public interface MfrScoreCache {
    Optional<Double> getMFRScore(Long restaurantId, String modelName);

    void setMFRScore(Long restaurantId, Double mfr, String modelName, Long ttl);
}
