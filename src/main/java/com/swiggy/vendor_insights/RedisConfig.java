package com.swiggy.vendor_insights;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.ValueOperations;

/**
 * Created by sandeepjindal1 on 5/25/17.
 */
@Configuration
public class RedisConfig {
    @Autowired
    private RedisTemplate redisTemplate;

    @Bean
    RedisTemplate redisTemplate() {
        return redisTemplate;
    }

    @Bean
    public ValueOperations valueOperations() {
        return redisTemplate.opsForValue();
    }

    @Bean
    public SetOperations setOperations() {
        return redisTemplate.opsForSet();
    }

    @Bean
    public HashOperations hashOperations() {
        return redisTemplate.opsForHash();
    }
}
