package com.swiggy.vendor_insights.exceptions.exceptions;

public class MfrException extends RuntimeException {
    public MfrException(String message) {
        super(message);
    }
}
