package com.swiggy.vendor_insights.exceptions.exceptions;

public class MfrReportException extends RuntimeException{
    public MfrReportException(String message) {
        super(message);
    }
}
