package com.swiggy.vendor_insights.exceptions.exceptions;

import org.springframework.validation.BindingResult;

/**
 * Created by sandeep.jindal on 6/08/17.
 */
public class InvalidFieldException extends RuntimeException{

    public String message;

    public String field;

    private BindingResult bindingResult;


    public InvalidFieldException(String field, String message){
        super(message);

        this.field=field;
        this.message=message;
    }
}
