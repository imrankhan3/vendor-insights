package com.swiggy.vendor_insights.exceptions.exceptions;

/**
 * Created by sandeepjindal1 on 5/18/17.
 */
public class RestaurantNotFoundException extends Exception {

    public RestaurantNotFoundException(String message){

        super(message);
    }

}
