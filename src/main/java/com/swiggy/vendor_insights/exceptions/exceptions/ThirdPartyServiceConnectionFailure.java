package com.swiggy.vendor_insights.exceptions.exceptions;

/**
 * Created by sandeepjindal1 on 6/8/17.
 */
public class ThirdPartyServiceConnectionFailure extends RuntimeException {

    public ThirdPartyServiceConnectionFailure(String message){
        super(message);
    }
}
