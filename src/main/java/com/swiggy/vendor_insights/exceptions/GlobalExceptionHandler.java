package com.swiggy.vendor_insights.exceptions;

import com.swiggy.commons.response.Response;
import com.swiggy.vendor_insights.exceptions.exceptions.*;
import com.swiggy.vendor_insights.exceptions.pojos.FieldValidationError;
import com.swiggy.vendor_insights.exceptions.pojos.MfrGenericResponse;
import com.swiggy.vendor_insights.exceptions.pojos.ValidationError;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sandeep.jindal on 6/08/17.
 */

@Log4j2
@ControllerAdvice
public class GlobalExceptionHandler {
    private final static int FAILURE_CODE = 1;
    private final static String STATUS_MESSAGE = "error processing request";
    private final static String UNEXPECTED_ERROR_MESSAGE = "unexpected error occurred";

    @Autowired
    MessageSource messageSource;

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
    public Response handleMethodArgumentNotValidException(MethodArgumentNotValidException ex){
        ValidationError validationError = this.processValidationErrors(ex.getBindingResult().getFieldErrors());
        return new Response(0, validationError, "Validation Failed");
    }

    @ExceptionHandler(InvalidFieldException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
    public Response handleInvalidFieldException(InvalidFieldException ex){
        HashMap<String, Object> fieldValidationErrors = new HashMap<>();
        List<FieldValidationError> fieldValidationErrorList = new ArrayList<>();
        fieldValidationErrors.put("field_validation_errors", fieldValidationErrorList);
        fieldValidationErrorList.add(new FieldValidationError(ex.field, ex.message));
        return new Response(0, fieldValidationErrors,"Validation Error");
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public Response handleException(RuntimeException ex){
        log.info("FAILED {}", ex.getStackTrace());
        ex.printStackTrace();
        return new Response(0, ex.getMessage(), "There was some error while processing this request.");
    }

    @ExceptionHandler(ThirdPartyServiceConnectionFailure.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public Response handleException(ThirdPartyServiceConnectionFailure ex){
        log.info("FAILED {}", ex.getStackTrace());
        ex.printStackTrace();
        return new Response(0,  ex.getMessage(), "Error while connecting to Third Party Service.");
    }


    @ExceptionHandler(ThirdPartyServiceFailureException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public Response handleException(ThirdPartyServiceFailureException ex){
        log.error("FAILED {}", ex.getStackTrace());
        ex.printStackTrace();
        return new Response(0,  ex.getMessage(), "Third Party Service Failure");
    }


    @ExceptionHandler(Exception.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public Response handleException(Exception ex){
        log.error("FAILED {}", ex.getStackTrace());
        ex.printStackTrace();
        return new Response(0,  ex.getMessage(), "There was some error while processing this request.");
    }


    private ValidationError processValidationErrors(List<FieldError> fieldErrors){
        ValidationError validationError = new ValidationError();

        if(!fieldErrors.isEmpty()){
            for(FieldError fieldError : fieldErrors){
                String localisedMessage = "";
                localisedMessage = messageSource.getMessage(fieldError, LocaleContextHolder.getLocale());
                validationError.addFieldValidationError(fieldError.getField(), localisedMessage);
            }
        }

        return validationError;
    }

    @ExceptionHandler(MfrException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public MfrGenericResponse handleMfrException(MfrException ex) {
        log.info("FAILED, {}", ex.getMessage());
        MfrGenericResponse.Error err = new MfrGenericResponse.Error( StringUtils.isEmpty(ex.getMessage()) ? UNEXPECTED_ERROR_MESSAGE : ex.getMessage() );
        return MfrGenericResponse.builder()
                .error(err)
                .code(FAILURE_CODE)
                .message(STATUS_MESSAGE)
                .build();
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public MfrGenericResponse handleBadRequestException(BadRequestException ex) {
        log.info("FAILED, {}", ex.getMessage());
        MfrGenericResponse.Error err = new MfrGenericResponse.Error( StringUtils.isEmpty(ex.getMessage()) ? UNEXPECTED_ERROR_MESSAGE : ex.getMessage() );
        return MfrGenericResponse.builder()
                .error(err)
                .code(FAILURE_CODE)
                .message(STATUS_MESSAGE)
                .build();
    }

    @ExceptionHandler(MfrReportException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public MfrGenericResponse handleMfrReportException(MfrReportException ex) {
        log.info("FAILED, {}", ex.getMessage());
        MfrGenericResponse.Error err = new MfrGenericResponse.Error( StringUtils.isEmpty(ex.getMessage()) ? UNEXPECTED_ERROR_MESSAGE : ex.getMessage());
        return MfrGenericResponse.builder()
                .error(err)
                .code(FAILURE_CODE)
                .message(STATUS_MESSAGE)
                .build();
    }
}
