package com.swiggy.vendor_insights.exceptions.pojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * Created by sandeep.jindal on 6/08/17.
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class FieldValidationError {

    @JsonProperty(value = "field")
    String field;

    @JsonProperty(value = "value")
    String message;
}
