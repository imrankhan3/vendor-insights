package com.swiggy.vendor_insights.exceptions.pojos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sandeep.jindal on 6/08/17.
 */

public class ValidationError {

    @JsonProperty(value = "field_validation_errors")
    List<FieldValidationError> fieldValidationErrors = new ArrayList<FieldValidationError>();

    public ValidationError(){

    }

    public void addFieldValidationError(String path, String message) {
        FieldValidationError fieldValidationError = new FieldValidationError(path, message);
        fieldValidationErrors.add(fieldValidationError);
    }

    public List<FieldValidationError> getFieldValidationErrors(){
        return this.fieldValidationErrors;
    }
}
