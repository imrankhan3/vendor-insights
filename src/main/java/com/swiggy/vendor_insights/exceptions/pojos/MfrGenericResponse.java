package com.swiggy.vendor_insights.exceptions.pojos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.vendor_insights.pojos.api.MFRScoreData;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Builder;


import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
public class MfrGenericResponse<T> {

    @NotNull
    @JsonProperty("statusCode")
    private Integer code;

    @NotNull
    @JsonProperty("statusMessage")
    private String message;

    @Nullable
    @JsonProperty("data")
    private T data;

    @Nullable
    @JsonProperty("error")
    private Error error;

    @Getter
    @Setter
    @AllArgsConstructor
    public static class Error {
        @JsonProperty("errorMessage")
        private String errorMessage;
    }


}
