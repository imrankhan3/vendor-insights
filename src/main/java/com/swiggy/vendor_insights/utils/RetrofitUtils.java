package com.swiggy.vendor_insights.utils;

import com.swiggy.vendor_insights.exceptions.exceptions.ThirdPartyServiceConnectionFailure;
import lombok.extern.slf4j.Slf4j;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

/**
 * Created by sandeepjindal on 5/15/17.
 */

@Slf4j
public class RetrofitUtils {
    public static <T> T execute(Call<T> call, T defaultVal) {
        Response<T> response = null;
        try {
            response = call.execute();
        } catch (IOException e) {
            log.error("error executing the request {}", defaultVal, e);
            throw new ThirdPartyServiceConnectionFailure(e.getMessage());
        }
        if (response.errorBody() != null) {
            CommonUtils.handleError(response);
        }
        return response.body() != null ? response.body() : defaultVal;
    }
}


