package com.swiggy.vendor_insights.utils;

import com.google.common.collect.ImmutableMap;

import java.util.Map;
/**
 * Created by sandeepjindal1 on 5/25/17.
 */
public class Settings {

    /**
     * Redis keys config
     */
    public static final String TEST_KEY = "TEST";
    public static final String KEY_LIST = "key_list";
    public static final String TEMPLATE_MAP = "template_map";
    public static final String RATING_REASONS = "rating_reasons";

    public static final String HELP = "help_context";
    public static final String CONFIG = "cache_config";
    public static final String MESSAGE = "messages";

    public static final String PEAK_WISE= "peak-wise";
    public static final String DAILY= "daily";
    public static final String WEEKLY= "weekly";
    public static final String MONTHLY= "monthly";

    public static final Map<Integer, String> CANCELLATION_DISPOSITION_MAP = ImmutableMap.of(
            25, "Restaurant Closed",
            26, "Restaurant Closed",
            27, "Restaurant not accepting orders",
            42, "Items Not Available"
    );

    /**
     * Object mapper variables
     */

    public static final String REST_ID = "rest_id";
    public static final String CAMPAIGN_ID = "campaign_id";
    public static final String MODEL_BASED_RATING = "modelBasedRating";
    public static final String RATED_ORDERS = "rated_orders";
    public static final String REVENUE = "revenue";
    public static final String DATE = "date";
    public static final String NO_OF_ORDERS = "no_of_orders";
    public static final String ORDER_COUNT = "order_count";
    public static final String REASON = "reason";
    public static final String INITIATED_BY = "initiated_by";
    public static final String WEEK = "week";
    public static final String END_DATE = "end_date";
    public static final String MONTH = "month";
    public static final String HOUR = "hour";
    public static final String ALTER_NOT_ACCEPTED_DELIVERED = "alter_not_accepted_delivered";
    public static final String ALTER_NOT_ACCEPTED_CANCELLED = "alter_not_accepted_cancelled";
    public static final String ALTER_ACCEPTED = "alter_accepted";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String MIN_CONFIRMATION_TIME = "min_confirmation_time";
    public static final String MAX_CONFIRMATION_TIME = "max_confirmation_time";
    public static final String AVG_CONFIRMATION_TIME = "avg_confirmation_time";
    public static final String TOTAL_ORDERS = "total_orders";
    public static final String CANCELLED_ORDERS = "cancelled_orders";
    public static final String EDITED_ORDERS = "edited_orders";
    public static final String ALTER_PROVIDED_DELIVERED = "alter_provided_delivered";
    public static final String CONFIRMED_IN_6MIN = "confirmed_in_6min";
    public static final String ZERO_THREE_MIN = "zero_three_min";
    public static final String THREE_SIX_MIN = "three_six_min";
    public static final String SIX_NINE_MIN = "six_nine_min";
    public static final String CONFIRMED_BY_DE = "confirmed_by_de";
    public static final String NOT_ACCEPTED = "not_accepted";
    public static final String NO_OF_DELIVERED_ORDERS = "no_of_delivered_orders";
    public static final String NO_OF_CANCELLED_ORDERS = "no_of_cancelled_orders";
    public static final String LOSSES = "losses";
    public static final String YEAR = "year";
    public static final String TOTAL_ORDER_COUNT = "total_order_count";
    public static final String TOTAL_CANCELLED_ORDERS = "total_cancelled_orders";
    public static final String LAST_UPDATED_TIME = "last_updated_time";
    public static final String IS_VEG = "is_veg";
    public static final String DELIVERED_ORDERS = "delivered_orders";
    public static final String CATEGORY = "category";
    public static final String SUB_CATEGORY = "sub_category";
    public static final String REASON_ID = "reason_id";
    public static final String ISSUES = "issues";
    public static final String COMMENTS = "comments";
    public static final String RATING = "rating";
    public static final String ORDER_ID = "order_id";
    public static final String PRICE = "price";
    public static final String NINE_PLUS = "nine_plus";
    public static final String ACCEPTED_ORDERS = "accepted_orders";
    public static final String SPEND = "spend";
    public static final String DISCOUNTED_ORDERS = "discounted_orders";
    public static final String NEW_CUSTOMERS = "new_customers";
    public static final String OLD_CUSTOMERS = "old_customers";
    public static final String DAYS = "days";
    public static final String SLOTS = "slots";
    public static final String ORDER_IDS = "order_ids";
    public static final String GMV = "gmv";
    public static final String PAST_STATUS = "past";
    public static final String CURRENT_STATUS = "current";
    public static final String CUSTOMER_ID = "customer_id";
    public static final String IS_RECONCILED = "is_reconciled";
    public static final String RESPONSIBLE_ID = "responsible_id";
    public static final String CANCELLED_STATUS = "cancelled_status";
    public static final String RESTAURANT_FIRST_ORDER = "restaurant_first_order";
    public static final String AVERAGE_PREP_TIME = "average_prep_time";

}
