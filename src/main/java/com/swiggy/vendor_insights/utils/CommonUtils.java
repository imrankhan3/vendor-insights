package com.swiggy.vendor_insights.utils;


import com.swiggy.vendor_insights.exceptions.exceptions.BadRequestException;
import com.swiggy.vendor_insights.exceptions.exceptions.ThirdPartyServiceFailureException;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import retrofit2.Response;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by sandeepjindal on 5/16/17.
 */
@Component
@Log4j2
public class CommonUtils {
    
    public static <T> void handleError(Response<T> response) {
        try {
            log.error("Error body {} with response {}", response.errorBody().string(), response.raw().body().string());
            throw new ThirdPartyServiceFailureException(response.errorBody().string());
        } catch (Exception e) {
            log.error("Error logging exception ", e.getMessage());
        }
    }

    public static Long parseRestaurantId(String restId) {
        Long restaurantId;
        try {
            if (StringUtils.isEmpty(restId)) {
                log.error("Invalid restaurantId {}", restId);
                throw new BadRequestException("Invalid restaurant id");
            }
            restaurantId = Long.parseLong(restId);

        } catch(Exception e) {
            log.error("restaurantId parsing failed with exception {}", e.getMessage());
            throw new BadRequestException("Invalid restaurant id");
        }
        return restaurantId;
    }

    public static RequestObj buildRequestObj (List<Long> restaurantIds, LocalDateTime startDate, LocalDateTime endDate) {
        return RequestObj.builder()
                .rest_id(restaurantIds)
                .start_date(DateUtils.convertToUTC(startDate))
                .end_date(DateUtils.convertToUTC(endDate))
                .build();
    }

}
