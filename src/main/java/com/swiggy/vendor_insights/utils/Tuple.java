package com.swiggy.vendor_insights.utils;

import lombok.Getter;

/**
 * Created by sandeepjindal1 on 5/30/17.
 */
@Getter
public class Tuple<T,R> {
    private final T left;
    private final R right;

    public Tuple(T t, R r){
        this.left = t;
        this.right = r;
    }
}
