package com.swiggy.vendor_insights.utils;

import com.swiggy.vendor_insights.exceptions.exceptions.BadRequestException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.swiggy.vendor_insights.constants.MfrConstants.HOUR_TO_UTC;
import static com.swiggy.vendor_insights.constants.MfrConstants.ISO_DATE_FORMAT;
import static com.swiggy.vendor_insights.constants.MfrConstants.MINUTES_TO_UTC;


public class DateUtils {

    public static String convertToUTC(LocalDateTime localDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ISO_DATE_FORMAT);
        return localDateTime.minusHours(HOUR_TO_UTC).minusMinutes(MINUTES_TO_UTC).format(formatter);
    }

    public static LocalDateTime convertToLocalDateTime(String time) {
        LocalDateTime date;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ISO_DATE_FORMAT);
        try {
            date = LocalDateTime.parse(time, formatter);
        } catch (Exception ex) {
            throw new BadRequestException(ex.getMessage());
        }
        return date;
    }
}
