package com.swiggy.vendor_insights.constants;


public class MfrConstants {
    public static final String SUCCESS_MESSAGE = "success";
    public static final int SUCCESS_CODE = 0;
    public static final String MFR_REQUEST_PREFIX = "mfr_restaurant_id_";
    public static final String WEEK_ON_WEEK = "wow";
    public static final String MONTH_ON_MONTH = "mom";
    public static final String MFR_ACCURATE_ORDERS = "mfr_accurate_orders";
    public static final String TOTAL_DELIVERED_ORDERS = "total_delivered_orders";
    public static final String ISO_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final Long HOUR_TO_UTC = 5L;
    public static final Long MINUTES_TO_UTC = 30L;
    public static final String MFR_REPORT_KEY = "order_id";
    public static final String MFR_REPORT_VALUE = "mfr_accuracy";
    public static final String ORDER_PREP_TIME = "prep_time";
    public static final String MFR_PRESSED_CORRECTLY = "mfr_pressed_correctly";
    public static final String MFR_PRESSED_EARLY = "mfr_pressed_early";
    public static final String MFR_NOT_PRESSED = "mfr_not_pressed";
}
