package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.vendor_insights.AbstractUnitTest;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.BaseBo;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.ItemResponse;
import com.swiggy.vendor_insights.pojos.responseobj.MenuResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

/**
 * Created by sandeepjindal1 on 6/22/17.
 */
public class MenuTransformationTest extends AbstractUnitTest {

    @Mock
    private AggregationService aggregationService;

    @InjectMocks
    private MenuTransformation menuTransformation;

    private AggregationBo aggregationBo = new AggregationBo();

    private AggregationBo aggregationBoCat = new AggregationBo();

    private AggregationBo aggregationBoItem = new AggregationBo();

    private AggregationBo aggregationBoBare = new AggregationBo();

    private RequestObj requestObj = new RequestObj();

    private QueryResult queryResult = new QueryResult();

    private QueryResult rawResult = new QueryResult();

    private QueryResult itemQueryResult = new QueryResult();

    @Before
    public void setUp() throws Exception {
        Map<String,Object> metaInfo = new HashMap<>();

        metaInfo.put("time_granularity","1 day");

        ObjectMapper objectMapper = new ObjectMapper();

        InputStream menuInputStream = this.getClass().getClassLoader().getResourceAsStream("json/menu.json");

        InputStream itemInputStream = this.getClass().getClassLoader().getResourceAsStream("json/item.json");

        MenuResponse menuResponse = objectMapper.
                readValue(menuInputStream, MenuResponse.class);

        ItemResponse itemResponse = objectMapper.
                readValue(itemInputStream, ItemResponse.class);

        Map<String, Object> dataTuple = objectMapper.convertValue(menuResponse, Map.class);

        Map<String, Object> itemDataTuple = objectMapper.convertValue(itemResponse, Map.class);

        Map<String, Object> rawDataTuple = new HashMap<>();

        rawDataTuple.put("revenue",200);
        rawDataTuple.put("order_count",2);

        List<Map<String,Object>> data= new ArrayList<>();

        List<Map<String,Object>> rawData= new ArrayList<>();

        List<Map<String,Object>> itemData= new ArrayList<>();

        rawData.add(rawDataTuple);

        data.add(dataTuple);

        itemData.add(itemDataTuple);

        queryResult.setMetaInfo(metaInfo);

        queryResult.setResult(data);

        rawResult.setMetaInfo(metaInfo);

        rawResult.setResult(rawData);

        itemQueryResult.setMetaInfo(metaInfo);

        itemQueryResult.setResult(itemData);

        Map<Template,BaseBo> aggregationDataCategory = new HashMap<>();

        aggregationDataCategory.put(Template.RAW_ORDER,(BaseBo) rawResult);

        aggregationDataCategory.put(Template.CATEGORY,(BaseBo) queryResult);


        Map<Template,BaseBo> aggregationDataItem = new HashMap<>();

        aggregationDataItem.put(Template.RAW_ORDER,(BaseBo) rawResult);

        aggregationDataItem.put(Template.ITEMS,(BaseBo) itemQueryResult);


        Map<Template,BaseBo> aggregationDataBare = new HashMap<>();

        aggregationDataBare.put(Template.CATEGORY,null);

        aggregationBoBare.setData(aggregationDataBare);

        aggregationBoCat.setData(aggregationDataCategory);

        aggregationBoItem.setData(aggregationDataItem);

        mock_methods();

    }

    private void mock_methods() {

        when(aggregationService.aggregate(anyList(), any(RequestObj.class))).thenReturn(aggregationBoCat);

    }

    @Test
    public void transform() throws Exception {

        List<Template> keys = new ArrayList<>();

        List<InsightsBaseResponse> response = menuTransformation.transform(keys,requestObj);

        MenuResponse menuResponse = new MenuResponse();

        menuResponse.setId(1);
        menuResponse.setName("PBM");
        menuResponse.setOrderCount(1);

        Assert.assertEquals(menuResponse,(response.get(0)));

    }

    @Test
    public void convertToTransformedObjectItem() throws Exception {

        List<InsightsBaseResponse> response = menuTransformation.convertToTransformedObject(null);

        Assert.assertEquals(0,response.size());

        List<InsightsBaseResponse> responseQr = menuTransformation.convertToTransformedObject(aggregationBoBare);

        Assert.assertEquals(0,responseQr.size());

        List<InsightsBaseResponse> responseItem = menuTransformation.convertToTransformedObject(aggregationBoItem);

        ItemResponse itemResponse = new ItemResponse();

        itemResponse.setId(1);
        itemResponse.setOrderCount(1);
        itemResponse.setName("PBM");
        itemResponse.setTotalRevenue(200);
        itemResponse.setTotalOrderCount(2);
        itemResponse.setRevenue(100);

        Assert.assertEquals(itemResponse, (responseItem.get(0)));

    }

    @Test
    public void convertToTransformedObjectCategory() throws Exception {

        List<InsightsBaseResponse> response = menuTransformation.convertToTransformedObject(null);

        Assert.assertEquals(0,response.size());

        List<InsightsBaseResponse> responseQr = menuTransformation.convertToTransformedObject(aggregationBoBare);

        Assert.assertEquals(0,responseQr.size());

        List<InsightsBaseResponse> responseCat = menuTransformation.convertToTransformedObject(aggregationBoCat);

        MenuResponse menuResponse = new MenuResponse();

        menuResponse.setId(1);
        menuResponse.setName("PBM");
        menuResponse.setOrderCount(1);

        Assert.assertEquals(menuResponse, (responseCat.get(0)));

    }

}