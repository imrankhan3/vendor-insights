package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.vendor_insights.AbstractUnitTest;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.BaseBo;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.orders.OrderMonthlyResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

/**
 * Created by sandeepjindal1 on 6/23/17.
 */
public class OrderMonthlyTransformationTest extends AbstractUnitTest {

    @Mock
    private AggregationService aggregationService;

    @InjectMocks
    private OrderMonthlyTransformation orderMonthlyTransformation;

    private AggregationBo aggregationBo = new AggregationBo();

    private AggregationBo aggregationBoQR = new AggregationBo();

    private RequestObj requestObj = new RequestObj();

    private QueryResult queryResult = new QueryResult();

    @Before
    public void setUp() throws Exception {

        Map<String,Object> meta_info = new HashMap<>();

        meta_info.put("time_granularity","1 day");

        ObjectMapper objectMapper = new ObjectMapper();

        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("json/ordersMonthly.json");

        OrderMonthlyResponse orderMonthlyResponse1 = objectMapper.
                readValue(inputStream, OrderMonthlyResponse.class);

        Map<String, Object> data_tuple = objectMapper.convertValue(orderMonthlyResponse1, Map.class);

//      TypeReference<Map<String,Object>> typeRef = new TypeReference<Map<String,Object>>() {};

//      Map<String, Object> data_tuple1 = objectMapper.readValue(inputStream,typeRef);

        List<Map<String,Object>> data= new ArrayList<>();

        data.add(data_tuple);

        queryResult.setMetaInfo(meta_info);

        queryResult.setResult(data);

        Map<Template,BaseBo> aggregationData = new HashMap<>();

        aggregationData.put(Template.ORDERS_MONTHLY,(BaseBo) queryResult);

        Map<Template,BaseBo> aggregationDataQR = new HashMap<>();

        aggregationDataQR.put(Template.ORDERS_MONTHLY,null);

        aggregationBoQR.setData(aggregationDataQR);

        aggregationBo.setData(aggregationData);

        mock_methods();

    }

    private void mock_methods() {

        when(aggregationService.aggregate(anyList(), any(RequestObj.class))).thenReturn(aggregationBo);

    }

    @Test
    public void transform() throws Exception {

        List<Template> keys = new ArrayList<>();

        List<InsightsBaseResponse> response = orderMonthlyTransformation.transform(keys,requestObj);

        OrderMonthlyResponse orderMonthlyResponse = new OrderMonthlyResponse();

        orderMonthlyResponse.setNumberOfOrders(12);
        orderMonthlyResponse.setRevenue(120d);
        orderMonthlyResponse.setMonth(1);

        Assert.assertEquals(orderMonthlyResponse,((OrderMonthlyResponse) response.get(0)));

    }

    @Test
    public void convertToTransformedObject() throws Exception {

        List<InsightsBaseResponse> response = orderMonthlyTransformation.convertToTransformedObject(null);

        Assert.assertEquals(0,response.size());

        List<InsightsBaseResponse> responseQr = orderMonthlyTransformation.convertToTransformedObject(aggregationBoQR);

        Assert.assertEquals(0,responseQr.size());

        List<InsightsBaseResponse> responseFull = orderMonthlyTransformation.convertToTransformedObject(aggregationBo);

        OrderMonthlyResponse orderMonthlyResponse = new OrderMonthlyResponse();

        orderMonthlyResponse.setNumberOfOrders(12);
        orderMonthlyResponse.setRevenue(120d);
        orderMonthlyResponse.setMonth(1);

        Assert.assertEquals(orderMonthlyResponse, ((OrderMonthlyResponse) responseFull.get(0)));

    }

}