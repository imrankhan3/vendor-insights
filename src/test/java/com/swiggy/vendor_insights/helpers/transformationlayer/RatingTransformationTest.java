package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.vendor_insights.AbstractUnitTest;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.BaseBo;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.RestaurantServiceResponse;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.rating.RatingsResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by sandeepjindal1 on 6/22/17.
 */
public class RatingTransformationTest extends AbstractUnitTest {

    @InjectMocks
    private RatingTransformation ratingTransformation;

    @Mock
    private AggregationService aggregationService;

    private AggregationBo aggregationBo = new AggregationBo();

    private AggregationBo aggregationBoQR = new AggregationBo();

    private AggregationBo aggregationBoSRS = new AggregationBo();

    private RequestObj requestObj = new RequestObj();

    private QueryResult queryResult = new QueryResult();

    private RestaurantServiceResponse restaurantServiceResponse = new RestaurantServiceResponse();

    @Spy
    private RedisTemplate redisTemplate = new RedisTemplate();

    Map<Object,Object> map = new HashMap<>();


    @Before
    public void setUp() throws Exception {



        Map<String,Object> meta_info = new HashMap<>();

        meta_info.put("time_granularity","1 day");

        ObjectMapper objectMapper = new ObjectMapper();

        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("json/ratings.json");

        TypeReference<Map<String,Object>> typeRef = new TypeReference<Map<String,Object>>() {};

        Map<String, Object> data_tuple = objectMapper.readValue(inputStream, typeRef);

        List<Map<String,Object>> data= new ArrayList<>();

        data.add(data_tuple);

        queryResult.setMetaInfo(meta_info);

        queryResult.setResult(data);

        List<Map<String,Object>> data_srs= new ArrayList<>();

        Map<String,Object> srs_data= new HashMap<>();

        srs_data.put("modelBasedRating",3.2d);

        data_srs.add(srs_data);

        restaurantServiceResponse.setData(data_srs);

        Map<Template,BaseBo> aggregationData = new HashMap<>();

        aggregationData.put(Template.RATINGS,(BaseBo) queryResult);

        Map<Template,BaseBo> aggregationDataQR = new HashMap<>();

        Map<Template,BaseBo> aggregationDataSRS = new HashMap<>();

        aggregationDataQR.put(Template.RATINGS,null);

        aggregationDataSRS.put(Template.SRS,restaurantServiceResponse);

        aggregationDataSRS.put(Template.RATINGS,(BaseBo) queryResult);


        aggregationBoQR.setData(aggregationDataQR);

        aggregationBo.setData(aggregationData);

        aggregationBoSRS.setData(aggregationDataSRS);

        map.put("r_one_count","FOOD_RELATED");
        map.put("r_two_count","PACKAGING");
        map.put("r_three_count","ITEM_MISSING");
        map.put("r_four_count","food_quality");
        map.put("r_five_count","food_quantity");

        mock_methods();

    }

    private void mock_methods() {

//        when(aggregationService.aggregate(anyList(), anyObject())).thenReturn(aggregationBo);

        HashOperations<Serializable, Object, Object> hashOperations =mock(HashOperations.class);

        when(redisTemplate.opsForHash()).thenReturn(hashOperations);
        when(hashOperations.entries("rating_reasons")).thenReturn(map);

    }

    @Test
    public void transform() {

        InsightsBaseResponse response = ratingTransformation.convertToTransformedObject(null);

        Assert.assertNull(response);

        InsightsBaseResponse responseQr = ratingTransformation.convertToTransformedObject(aggregationBoQR);

        Assert.assertNull(responseQr);

        InsightsBaseResponse responseFull = ratingTransformation.convertToTransformedObject(aggregationBo);

        InsightsBaseResponse responseSRS = ratingTransformation.convertToTransformedObject(aggregationBoSRS);

        ObjectMapper objectMapper = new ObjectMapper();

        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("json/ratingExpected.json");

        RatingsResponse ratingsResponse1 = null;
        try {
             ratingsResponse1 = objectMapper.
                    readValue(inputStream, RatingsResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(ratingsResponse1.getRatedOrders(),((RatingsResponse)responseFull).getRatedOrders());

        Assert.assertEquals(ratingsResponse1.getModelBasedRating(),((RatingsResponse)responseSRS).getModelBasedRating());

    }

    @Test
    public void convertToTransformedObject() throws Exception {

    }

}