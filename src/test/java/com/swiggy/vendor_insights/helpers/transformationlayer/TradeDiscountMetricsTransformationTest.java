package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.vendor_insights.AbstractUnitTest;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.BaseBo;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.TradeDiscountMetricsResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

public class TradeDiscountMetricsTransformationTest extends AbstractUnitTest {

    @InjectMocks
    TradeDiscountMetricsTransformation tradeDiscountMetricsTransformation;

    @Mock
    private AggregationService aggregationService;

    private AggregationBo aggregationBo = new AggregationBo();

    private AggregationBo aggregationBoQR = new AggregationBo();

    private RequestObj requestObj = new RequestObj();

    private QueryResult queryResult = new QueryResult();
    @Before
    public void setup() throws Exception {
        Map<String,Object> meta_info = new HashMap<>();
        meta_info.put("time_granularity","1 day");
        ObjectMapper objectMapper = new ObjectMapper();

        TradeDiscountMetricsResponse tradeDiscountMetricsResponse = TradeDiscountMetricsResponse.builder()
                .revenue(123D)
                .restaurantId(212)
                .spend(321D)
                .build();
        Map<String, Object> data_tuple = objectMapper.convertValue(tradeDiscountMetricsResponse, Map.class);

        List<Map<String,Object>> data= new ArrayList<>();

        data.add(data_tuple);

        queryResult.setMetaInfo(meta_info);

        queryResult.setResult(data);
        Map<Template,BaseBo> aggregationData = new HashMap<>();

        aggregationData.put(Template.TD_METRCIS,(BaseBo) queryResult);

        Map<Template,BaseBo> aggregationDataQR = new HashMap<>();

        aggregationDataQR.put(Template.ORDERS,null);

        aggregationBoQR.setData(aggregationDataQR);

        aggregationBo.setData(aggregationData);

    }

    @Test
    public void testTransform() {
        when(aggregationService.aggregate(anyList(), any(RequestObj.class))).thenReturn(aggregationBo);
        List<Template> templates = Arrays.asList(Template.TD_METRCIS);
        List<InsightsBaseResponse> response = tradeDiscountMetricsTransformation.transform(templates, requestObj);
        Assert.assertEquals(1, response.size());

    }

    @Test
    public void testTransformWhenAggrregationBOIsNull() {
        when(aggregationService.aggregate(anyList(), any(RequestObj.class))).thenReturn(null);
        List<Template> templates = Arrays.asList(Template.TD_METRCIS);
        List<InsightsBaseResponse> response = tradeDiscountMetricsTransformation.transform(templates, requestObj);
        Assert.assertEquals(0, response.size());

    }

    @Test
    public void testTransformWhenBaseBOIsNull() {
        when(aggregationService.aggregate(anyList(), any(RequestObj.class))).thenReturn(aggregationBoQR);
        List<Template> templates = Arrays.asList(Template.TD_METRCIS);
        List<InsightsBaseResponse> response = tradeDiscountMetricsTransformation.transform(templates, requestObj);
        Assert.assertEquals(0, response.size());

    }
}
