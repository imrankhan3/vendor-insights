package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.vendor_insights.AbstractUnitTest;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.BaseBo;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.orders.OrderResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

/**
 * Created by sandeepjindal1 on 6/27/17.
 */
public class OrderTransformationTest extends AbstractUnitTest {

    @Mock
    private AggregationService aggregationService;

    @InjectMocks
    private OrderTransformation orderTransformation;

    private AggregationBo aggregationBo = new AggregationBo();

    private AggregationBo aggregationBoQR = new AggregationBo();

    private RequestObj requestObj = new RequestObj();

    private QueryResult queryResult = new QueryResult();

    @Before
    public void setUp() throws Exception {
        Map<String,Object> meta_info = new HashMap<>();

        meta_info.put("time_granularity","1 day");

        ObjectMapper objectMapper = new ObjectMapper();

        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("json/orders.json");

        OrderResponse orderResponse = objectMapper.
                readValue(inputStream, OrderResponse.class);

        Map<String, Object> data_tuple = objectMapper.convertValue(orderResponse, Map.class);

//      TypeReference<Map<String,Object>> typeRef = new TypeReference<Map<String,Object>>() {};

//      Map<String, Object> data_tuple1 = objectMapper.readValue(inputStream,typeRef);

        List<Map<String,Object>> data= new ArrayList<>();

        data.add(data_tuple);

        queryResult.setMetaInfo(meta_info);

        queryResult.setResult(data);

        Map<Template,BaseBo> aggregationData = new HashMap<>();

        aggregationData.put(Template.ORDERS,(BaseBo) queryResult);

        Map<Template,BaseBo> aggregationDataQR = new HashMap<>();

        aggregationDataQR.put(Template.ORDERS,null);

        aggregationBoQR.setData(aggregationDataQR);

        aggregationBo.setData(aggregationData);

        mock_methods();

    }

    private void mock_methods() {

        when(aggregationService.aggregate(anyList(), any(RequestObj.class))).thenReturn(aggregationBo);

    }

    @Test
    public void transform() throws Exception {

        List<Template> keys = new ArrayList<>();

        List<InsightsBaseResponse> response = orderTransformation.transform(keys,requestObj);

        OrderResponse orderResponse = new OrderResponse();

        orderResponse.setNumberOfOrders(12);
        orderResponse.setRevenue(120d);
        orderResponse.setDate("2017-02-01");

        Assert.assertEquals(orderResponse,(response.get(0)));

    }

    @Test
    public void convertToTransformedObject() throws Exception {

        List<InsightsBaseResponse> response = orderTransformation.convertToTransformedObject(null);

        Assert.assertEquals(0,response.size());

        List<InsightsBaseResponse> responseQr = orderTransformation.convertToTransformedObject(aggregationBoQR);

        Assert.assertEquals(0,responseQr.size());

        List<InsightsBaseResponse> responseFull = orderTransformation.convertToTransformedObject(aggregationBo);

        OrderResponse orderResponse = new OrderResponse();

        orderResponse.setNumberOfOrders(12);
        orderResponse.setRevenue(120d);
        orderResponse.setDate("2017-02-01");

        Assert.assertEquals(orderResponse, ((OrderResponse) responseFull.get(0)));

    }



}