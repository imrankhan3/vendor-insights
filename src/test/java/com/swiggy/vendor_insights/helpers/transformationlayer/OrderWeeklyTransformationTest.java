package com.swiggy.vendor_insights.helpers.transformationlayer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.vendor_insights.AbstractUnitTest;
import com.swiggy.vendor_insights.enums.Template;
import com.swiggy.vendor_insights.helpers.aggregationlayer.AggregationService;
import com.swiggy.vendor_insights.pojos.bo.BaseBo;
import com.swiggy.vendor_insights.pojos.bo.QueryResult;
import com.swiggy.vendor_insights.pojos.bo.aggregationbo.AggregationBo;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.orders.OrderResponse;
import com.swiggy.vendor_insights.pojos.responseobj.orders.OrderWeeklyResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

/**
 * Created by sandeepjindal1 on 6/27/17.
 */
public class OrderWeeklyTransformationTest extends AbstractUnitTest {
    @Mock
    private AggregationService aggregationService;

    @InjectMocks
    private OrderWeeklyTransformation orderWeeklyTransformation;

    private AggregationBo aggregationBo = new AggregationBo();

    private AggregationBo aggregationBoQR = new AggregationBo();

    private RequestObj requestObj = new RequestObj();

    private QueryResult queryResult = new QueryResult();

    @Before
    public void setUp() throws Exception {

        Map<String,Object> meta_info = new HashMap<>();

        meta_info.put("time_granularity","1 day");

        ObjectMapper objectMapper = new ObjectMapper();

        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("json/orders.json");

        OrderResponse orderResponseweekly = objectMapper.
                readValue(inputStream, OrderResponse.class);

        Map<String, Object> data_tuple = objectMapper.convertValue(orderResponseweekly, Map.class);

        List<Map<String,Object>> data= new ArrayList<>();

        data.add(data_tuple);

        queryResult.setMetaInfo(meta_info);

        queryResult.setResult(data);

        Map<Template,BaseBo> aggregationData = new HashMap<>();

        aggregationData.put(Template.ORDERS,(BaseBo) queryResult);

        Map<Template,BaseBo> aggregationDataQR = new HashMap<>();

        aggregationDataQR.put(Template.ORDERS,null);

        aggregationBoQR.setData(aggregationDataQR);

        aggregationBo.setData(aggregationData);

        mock_methods();

    }

    private void mock_methods() {

        when(aggregationService.aggregate(anyList(), any(RequestObj.class))).thenReturn(aggregationBo);

    }

    @Test
    public void transform() throws Exception {

        List<Template> keys = new ArrayList<>();

        List<InsightsBaseResponse> response = orderWeeklyTransformation.transform(keys,requestObj);

        OrderWeeklyResponse orderWeeklyResponse = new OrderWeeklyResponse();

        orderWeeklyResponse.setNumberOfOrders(12);
        orderWeeklyResponse.setRevenue(120d);
        orderWeeklyResponse.setStartDate("2017-02-01");
        orderWeeklyResponse.setEndDate("2017-02-01");
        orderWeeklyResponse.setWeek(6);

        Assert.assertEquals(orderWeeklyResponse,((OrderWeeklyResponse) response.get(0)));

    }

    @Test
    public void convertToTransformedObject() throws Exception {

        List<InsightsBaseResponse> response = orderWeeklyTransformation.convertToTransformedObject(null);

        Assert.assertEquals(0,response.size());

        List<InsightsBaseResponse> responseQr = orderWeeklyTransformation.convertToTransformedObject(aggregationBoQR);

        Assert.assertEquals(0,responseQr.size());

        List<InsightsBaseResponse> responseFull = orderWeeklyTransformation.convertToTransformedObject(aggregationBo);

        OrderWeeklyResponse orderWeeklyResponse = new OrderWeeklyResponse();

        orderWeeklyResponse.setNumberOfOrders(12);
        orderWeeklyResponse.setRevenue(120d);
        orderWeeklyResponse.setStartDate("2017-02-01");
        orderWeeklyResponse.setEndDate("2017-02-01");
        orderWeeklyResponse.setWeek(6);


        Assert.assertEquals(orderWeeklyResponse, ((OrderWeeklyResponse) responseFull.get(0)));

    }

}