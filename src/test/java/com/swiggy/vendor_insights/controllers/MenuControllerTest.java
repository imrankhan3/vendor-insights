package com.swiggy.vendor_insights.controllers;

import com.swiggy.commons.response.Response;
import com.swiggy.vendor_insights.AbstractUnitTest;
import com.swiggy.vendor_insights.helpers.transformationlayer.MenuTransformation;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.ItemResponse;
import com.swiggy.vendor_insights.pojos.responseobj.MenuResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.Mockito.when;

/**
 * Created by sandeepjindal1 on 6/22/17.
 */
public class MenuControllerTest extends AbstractUnitTest {

    @Mock
    private MenuTransformation menuTransformation;

    @InjectMocks
    private MenuController menuController;

    List<InsightsBaseResponse> menuListResponse = new ArrayList<>();

    List<InsightsBaseResponse> itemListResponse = new ArrayList<>();

    @Before
    public void setUp() throws Exception {

    MenuResponse menuResponse = new MenuResponse();

    menuResponse.setId(1);
    menuResponse.setName("PBM");
    menuResponse.setOrderCount(1);

    menuListResponse.add(menuResponse);

    ItemResponse itemResponse = new ItemResponse();

    itemResponse.setId(1);
    itemResponse.setName("PBM");
    itemResponse.setOrderCount(1);
    itemResponse.setRevenue(100);
    itemResponse.setTotalOrderCount(2);
    itemResponse.setTotalRevenue(200);

    itemListResponse.add(itemResponse);

    mock_methods();
    }

    private void mock_methods() {

        when(menuTransformation.transform(anyObject(), any(RequestObj.class))).thenReturn(menuListResponse);

    }

    @Test
    public void getCategoryMeasures() throws Exception {

        RequestObj requestObj = new RequestObj();

        Response response = menuController.getCategoryMeasures(requestObj);

        Assert.assertEquals(menuListResponse, response.getData());

    }

    @Test
    public void getItemsMeasures() throws Exception {


        RequestObj requestObj = new RequestObj();

        when(menuTransformation.transform(anyObject(), any(RequestObj.class))).thenReturn(itemListResponse);

        Response response = menuController.getItemsMeasures(requestObj);

        Assert.assertEquals(itemListResponse, response.getData());

    }

    @Test
    public void getItemsBasedOnCategoryAndSubcategoryMeasures() throws Exception {


        RequestObj requestObj = new RequestObj();

        when(menuTransformation.transform(anyObject(), any(RequestObj.class))).thenReturn(itemListResponse);

        Response response = menuController.getItemsBasedOnCategoryAndSubcategoryMeasures(requestObj);

        Assert.assertEquals(itemListResponse, response.getData());

    }


    @Test
    public void getSubCategoryMeasures() throws Exception {

        RequestObj requestObj = new RequestObj();

        Response response = menuController.getSubCategoryMeasures(requestObj);

        Assert.assertEquals(menuListResponse, response.getData());

    }


}