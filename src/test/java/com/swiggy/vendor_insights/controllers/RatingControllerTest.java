package com.swiggy.vendor_insights.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.commons.response.Response;
import com.swiggy.vendor_insights.AbstractUnitTest;
import com.swiggy.vendor_insights.helpers.transformationlayer.RatingTransformation;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.rating.RatingsResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.io.InputStream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.Mockito.when;

/**
 * Created by sandeepjindal1 on 6/22/17.
 */
public class RatingControllerTest extends AbstractUnitTest {

    @Mock
    private RatingTransformation ratingTransformation;

    @InjectMocks
    private RatingController ratingController;

    InsightsBaseResponse ratingResponse = null;

    @Before
    public void setUp() throws Exception {

        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("json/ratingExpected.json");

        ObjectMapper objectMapper = new ObjectMapper();

        ratingResponse = objectMapper.
                readValue(inputStream, RatingsResponse.class);

        mock_methods();
    }

    private void mock_methods() {

        when(ratingTransformation.transform(anyObject(), any(RequestObj.class))).thenReturn(ratingResponse);

    }
    @Test
    public void getRatingMeasures() throws Exception {

        RequestObj requestObj = new RequestObj();

        Response response = ratingController.getRatingMeasures(requestObj);

        Assert.assertEquals(ratingResponse, response.getData());

    }

}