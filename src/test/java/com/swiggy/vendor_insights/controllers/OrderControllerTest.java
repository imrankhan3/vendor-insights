package com.swiggy.vendor_insights.controllers;

import com.swiggy.commons.response.Response;
import com.swiggy.vendor_insights.AbstractUnitTest;
import com.swiggy.vendor_insights.helpers.transformationlayer.OrderHourlyTransformation;
import com.swiggy.vendor_insights.helpers.transformationlayer.OrderMonthlyTransformation;
import com.swiggy.vendor_insights.helpers.transformationlayer.OrderTransformation;
import com.swiggy.vendor_insights.pojos.requestobj.RequestObj;
import com.swiggy.vendor_insights.pojos.responseobj.InsightsBaseResponse;
import com.swiggy.vendor_insights.pojos.responseobj.orders.OrderResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.Mockito.when;

/**
 * Created by sandeepjindal1 on 6/27/17.
 */
public class OrderControllerTest extends AbstractUnitTest {
    @Mock
    private OrderTransformation orderTransformation;

    @Mock
    private OrderMonthlyTransformation orderMonthlyTransformation;

    @Mock
    private OrderHourlyTransformation orderHourlyTransformation;

    @InjectMocks
    private OrderController orderController;

    List<InsightsBaseResponse> orderListResponse = new ArrayList<>();

    List<InsightsBaseResponse> orderHourlyListResponse = new ArrayList<>();

    @Before
    public void setUp() throws Exception {

        OrderResponse orderResponse = new OrderResponse();

        orderResponse.setNumberOfOrders(12);
        orderResponse.setRevenue(120d);
        orderResponse.setDate("2017-02-01");

        orderListResponse.add(orderResponse);

        mock_methods();
    }

    private void mock_methods() {

        when(orderTransformation.transform(anyObject(), any(RequestObj.class))).thenReturn(orderListResponse);

    }

    @Test
    public void getOrderMeasures() throws Exception {

        RequestObj requestObj = new RequestObj();

        requestObj.setGroup_by("daily");

        Response response = orderController.getOrderMeasures(requestObj);

        Assert.assertEquals(orderListResponse, response.getData());

    }


}