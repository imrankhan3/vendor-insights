package com.swiggy.vendor_insights;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Created by sandeepjindal1 on 6/22/17.
 */
@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class VendorInsightsApplicationTest {

    @Test
    public void contextLoads() {
    }

}