package com.swiggy;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by sandeepjindal1 on 6/22/17.
 */
@Configuration
@EntityScan(basePackages = { "com.swiggy" })
@EnableJpaRepositories(basePackages = "com.swiggy")
@EnableTransactionManagement
public class TestConfig {

}
